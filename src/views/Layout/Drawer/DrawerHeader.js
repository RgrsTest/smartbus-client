import { styled } from '@mui/material/styles';


const DrawerHeader = styled('div')(({ theme }) => ({
   
    display: 'inline',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  }));

export {DrawerHeader}