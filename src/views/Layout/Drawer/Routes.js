import React from 'react';
import { useTheme } from '@mui/material/styles';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { DrawerHeader } from './DrawerHeader';
import { DrawerBase } from './DrawerBase';
import { ItemRoute } from './ItemRoute';
import { Context } from '../../../Context';


function Routes(props){
    const routes = props.routes
    const theme = useTheme();
    const { drawer, setDrawer } = React.useContext(Context)

    return (
      <DrawerBase variant="permanent" open={drawer}>
          <DrawerHeader>
          <IconButton onClick={() => setDrawer(!drawer)}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
          </DrawerHeader>
          <Divider />
          
          <List>
                {routes.map((item, index) => (
                    <ItemRoute text={item.text} link={item.link} key={index}>
                        <item.icon></item.icon>
                    </ItemRoute>
                ))}
            </List>
          <Divider />
      </DrawerBase>
    )
}

export {Routes}
