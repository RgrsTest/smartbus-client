import React from 'react';
import {Routes} from './Routes'



function Drawer({ routes }){
    
    return (
        <Routes routes={ routes }>
        </Routes>
    )
}

export {Drawer}
