import React from "react";

import { ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import { Link, useMatch } from "react-router-dom";


function ItemRoute(props){
    const isExact = useMatch(props.link)
    return(
        <Link to={props.link} style={{textDecoration: 'none', color: 'ButtonText'}}>
            <ListItemButton selected={isExact? true : false} >
                <ListItemIcon>
                    { props.children }
                </ListItemIcon>
                
                <ListItemText primary={props.text} />
            </ListItemButton>
        </Link>
    )
}

export {ItemRoute}