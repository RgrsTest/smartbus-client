import React from "react";
import { Box } from "@mui/material";
import { AppBar } from "./AppBar";
import { Drawer } from "./Drawer";
import { DrawerHeader } from "./Drawer/DrawerHeader";


function Layout(props) {
    return (
        <Box sx={{ display: 'flex' }}>
 

            <AppBar />
            
            
            <Box component="main" sx={{ flexGrow: 1, p: 0 }}>
            {props.children}
               
              
              
            </Box>
        </Box>
    )
}

export {Layout}

//<Drawer routes={props.routes} />