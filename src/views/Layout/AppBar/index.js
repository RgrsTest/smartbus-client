import React from 'react';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { AppBarBase } from './AppBarBase';
import AccountIcon from '@mui/icons-material/AccountCircle';
import { Context } from '../../../Context';
import { Box } from '@mui/system';
import { Tooltip } from '@mui/material';
import {Avatar,Divider,ListItemIcon} from '@mui/material';
import PersonAdd from '@mui/icons-material/PersonAdd';
import Settings from '@mui/icons-material/Settings';
import Logout from '@mui/icons-material/Logout';

import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import { Link, useMatch } from "react-router-dom";
function AppBar(){

    const { drawer, setDrawer, handleSetJWT, setAuth } = React.useContext(Context)
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [openCatalogo, setOpenCatalogo] = React.useState(null);
    const open = Boolean(anchorEl);

    const openCat = Boolean(openCatalogo);


    const handleOpenCatalogo = (event) =>{
        setOpenCatalogo(event.currentTarget);
    }
    const handleCloseCatalogo = () => {
        setOpenCatalogo(null);
    };


    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleCloseSession = () => {
        handleSetJWT('')
        setAuth(null)
    }
    return (
        <React.Fragment>

        <AppBarBase   color="inherit" position="fixed" open={ drawer } sx={{height:'60px'}}>

        <Toolbar  sx={{ display: "flex", justifyContent: "space-between",height:'60px' }}>
            <Box sx={{height:'40px',flexGrow: 0, display: {xs: 'none', md: 'flex'}}}>
            
            
            <Link to={"/"} style={{textDecoration: 'none', color: 'ButtonText'}}>
            <img style={{ width: 160, height:30, marginTop:4}} src={'/assets/smartbus.png'} />
                
                </Link>


                </Box>


                <Box  sx={{height:'60px'}} display="flex" justifyContent="flex-end">
                <Button
                id="basic-button"
                color="primary"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
                sx={{marginRight:'40px'}}
                >
                Documentos
                </Button>


                <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                'aria-labelledby': 'basic-button',
                }}
                PaperProps={{
                    elevation: 0,
                    sx: {
                      overflow: 'visible',
                      filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                      mt: 1,
                      '& .MuiAvatar-root': {
                        width: 60,
                        height:60,
                        ml: -0.5,
                        mr: 1,
                      },
                      '&:before': {
                        content: '""',
                        display: 'block',
                        position: 'absolute',
                        top: 0,
                        right: 45,
                        width: 10,
                        height: 10,
                        bgcolor: 'background.paper',
                        transform: 'translateY(-50%) rotate(45deg)',
                        zIndex: 0,
                      },
                    },
                  }}
                >
                <Link to={"/Validaciones"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Tarjetas</MenuItem>
                </Link>
                <Link to={"/Recargas"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Recargas</MenuItem>
                </Link>
                <Link to={"/Ventas"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Efectivo</MenuItem>
                </Link>


                </Menu>


                <Button
                id="basic-button"
                color="primary"
                aria-controls={openCat ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={openCat ? 'true' : undefined}
                onClick={handleOpenCatalogo}
                sx={{marginRight:'40px'}}

                >
                Catalogos
                </Button>


                <Menu
                id="basic-menu"
                anchorEl={openCatalogo}
                open={openCat}
                onClose={handleCloseCatalogo}
                MenuListProps={{
                'aria-labelledby': 'basic-button',
                
                }}
                PaperProps={{
                    elevation: 0,
                    sx: {
                      overflow: 'visible',
                      filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                      mt: 1,
                      '& .MuiAvatar-root': {
                        width: 60,
                        height:60,
                        ml: -0.5,
                        mr: 1,
                      },
                      '&:before': {
                        content: '""',
                        display: 'block',
                        position: 'absolute',
                        top: 0,
                        right: 55,
                        width: 10,
                        height: 10,
                        bgcolor: 'background.paper',
                        transform: 'translateY(-50%) rotate(45deg)',
                        zIndex: 0,
                      },
                    },
                  }}
                >
                <Link to={"/vehiculos"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Vehiculos</MenuItem>
                </Link>
                <Link to={"/operadores"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Operadores</MenuItem>
                </Link>
                <Link to={"/dispositivos"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Dispositivos</MenuItem>
                </Link>
                <Link to={"/rutas"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Rutas</MenuItem>
                </Link>
                <Link to={"/perfiles"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Perfiles</MenuItem>
                </Link>
                <Link to={"/productos"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Productos</MenuItem>
                </Link>
                <Link to={"/transbordos"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Transbordos</MenuItem>
                </Link>
                <Link to={"/lams"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Lista LAM</MenuItem>
                </Link>
                <Link to={"/lapv"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Lista LAPV</MenuItem>
                </Link>
                <Link to={"/laprs"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Lista LAPR</MenuItem>
                </Link>
                <Link to={"/fimpe"} style={{textDecoration: 'none', color: 'ButtonText'}}>
                <MenuItem onClick={handleClose}>Fimpe</MenuItem>
                </Link>


                </Menu>








                <Tooltip title='Cerrar sesion'>
                <IconButton
                onClick={() => handleCloseSession()}
                size='large'

                >
                    <AccountIcon />
                </IconButton>
                </Tooltip>



            </Box>
        </Toolbar>
           
        </AppBarBase>
        </React.Fragment> 
    )
}

export { AppBar }

/*
<Toolbar  sx={{ display: "flex", justifyContent: "space-between",height:'10px' }}>
<Box sx={{height:'10px',flexGrow: 0, display: {xs: 'none', md: 'flex'}}}>
 
  




</Box>
</Toolbar>*/





/*
<Link to={"/"} style={{textDecoration: 'none', color: 'ButtonText'}}>
<Typography variant="h6" noWrap component="div" >
    Smartbus
</Typography>
</Link>


</Box>


<Box  sx={{height:'10px'}} display="flex" justifyContent="flex-end">
<Button
id="basic-button"
color="primary"
aria-controls={open ? 'basic-menu' : undefined}
aria-haspopup="true"
aria-expanded={open ? 'true' : undefined}
onClick={handleClick}
sx={{marginRight:'40px'}}
>
Documentos
</Button>


<Menu
id="basic-menu"
anchorEl={anchorEl}
open={open}
onClose={handleClose}
MenuListProps={{
'aria-labelledby': 'basic-button',
}}
>
<Link to={"/Validaciones"} style={{textDecoration: 'none', color: 'ButtonText'}}>
<MenuItem onClick={handleClose}>Tarjetas</MenuItem>
</Link>
<Link to={"/Recargas"} style={{textDecoration: 'none', color: 'ButtonText'}}>
<MenuItem onClick={handleClose}>Recargas</MenuItem>
</Link>
<Link to={"/Ventas"} style={{textDecoration: 'none', color: 'ButtonText'}}>
<MenuItem onClick={handleClose}>Efectivo</MenuItem>
</Link>


</Menu>


<Button
id="basic-button"
color="primary"
aria-controls={open ? 'basic-menu' : undefined}
aria-haspopup="true"
aria-expanded={open ? 'true' : undefined}
onClick={handleClick}
sx={{marginRight:'40px'}}

>
Catalogos
</Button>










<Tooltip title='Cerrar sesion'>
<IconButton
onClick={() => handleCloseSession()}
size='large'

>
    <AccountIcon />
</IconButton>
</Tooltip>*/