import React from "react";
import { Documentos } from "../../Api";
import { Context } from "../../Context";


function useVentas(fromDate,toDate){
    const { jwt } = React.useContext(Context)
    const [ loading, setLoading ] = React.useState(false)
    const [ ventas, setVentas ] = React.useState([])
    const ventasDocumentos = Documentos(jwt)
    const payload = {
        schema: 'ventas',
        filter: {

            where:[
                {
                    field:"payload->encabezado->fecha_hora",
                    operator: ">=",
                    value : fromDate
                },

                {
                    field:"payload->encabezado->fecha_hora",
                    operator: "<=",
                    value : toDate
                }

        

            ]

        }
    }

    async function getVentas(){
        setLoading(true)
        payload['with-storage'] = ['*']
        const response = await ventasDocumentos.consultar(payload)
        if (response.status === 'success'){
            setVentas(response.data)
        }
        setLoading(false)
    }

    React.useEffect(getVentas, [fromDate,toDate])

    return {
        loading,
        ventas
    }
}

export { useVentas }