import React from "react";
import { Paper, Table, TableBody,TextField, Box,TableCell, TableContainer, TableHead, TableRow,Stack,CardHeader ,Container,IconButton } from "@mui/material";
import { useVentas } from "./useVentas";
import { Loading } from "../components/Loading";
import { CustomRow } from "../components/CustomRow";
import { DatePickerRow } from "../components/DatePickerRow";
import { useDownloadExcel,downloadExcel } from 'react-export-table-to-excel';
import { DatePicker, LocalizationProvider,TimePicker} from "@mui/lab";
import DateAdapter from '@mui/lab/AdapterDateFns';
import { SlRefresh } from "react-icons/sl";
import { RiFileExcel2Fill ,RiSearchLine } from "react-icons/ri";



const columns = [
  { id: 'filtro', label:'Vehiculo', marginL:11, minWidth: 150 },
  { 
    id: 'completas', 
    label: 'Ventas Completas', 
    minWidth: 150,
    marginL:10,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX')
  },
  { 
    id: 'completas_recaudo', 
    label: 'Ventas Completas Recaudo', 
    minWidth: 150,
    marginL:11,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },
  {
    id: 'incompletas',
    label: 'Ventas Incompletas',
    minWidth: 150,
    align: 'right',
    marginL:9,
    format: (value) => value.toLocaleString('es-MX')
  },
  {
    id: 'incompletas_recaudo',
    label: 'Ventas Incompletas Recaudo',
    minWidth: 150,
    marginL:10,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },
  {
    id: 'total',
    label: 'Total Movimientos',
    minWidth: 150,
    marginL:7,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX')
  },
  {
    id: 'total_recaudo',
    label: 'Total Recaudado',
    minWidth: 150,
    marginL:15,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },

]

const detailsColumns = [
  { id: 'fecha_hora_servidor', label: 'Fecha Hora Servidor'},
  { id: 'fecha_hora', label: 'Fecha y Hora' },
  { id: 'folio_transaccion', label: 'Folio de Transaccion' },
  { id: 'tipo', label: 'Tipo' },
  { 
    id: 'monto', 
    label: 'Monto',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },
  { id: 'subidas', label: 'Subidas' },
  { id: 'bajadas', label: 'Bajadas' }
]

function uniqueValuesArray(data){
  let result = []
  data.forEach((item) => {
    if (result.findIndex((item) => item) === -1 ){
      result.push(item)}
  })
  return result
}

function arraySum(data){
  let result = 0
  data.forEach((item) =>{
    result += item
  })
  return result
}

function createData(data, filter){
  let result = []
  if(data.length > 0){
    const valores = data.map( (item) => item.payload )
    let filtro = uniqueValuesArray(valores.map((item) => item.encabezado._relaciones[filter].payload.label))
    
    filtro.forEach( (value) => {
      var completas = valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value && item.encabezado.tipo === 'VENTA'))
      var incompletas = valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value && item.encabezado.tipo === 'VENTA_INCOMPLETA'))
      var completas_recaudo = arraySum(completas.map((item) => item.encabezado.monto))
      var incompletas_recaudo = arraySum(incompletas.map((item) => item.encabezado.monto))
      
      let localValue = {
        filtro: value,
        completas: completas.length,
        completas_recaudo: completas_recaudo,
        incompletas: incompletas.length,
        incompletas_recaudo: arraySum(incompletas.map((item) => item.encabezado.monto)),
        total: completas.length + incompletas.length,
        total_recaudo: completas_recaudo + incompletas_recaudo,
        details: valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value))
      }
      result.push(localValue)
    })
  }
  return result
}

function Ventas(){

  const [ fromDate, setFromDate] = React.useState(new Date())
  const [ toDate, setToDate] = React.useState(new Date())

  const [ startDate, setStartDate] = React.useState(new Date())
  const [ finalDate, setFinalDate] = React.useState(new Date())

  const { loading, ventas } = useVentas(fromDate,toDate)

  

  const [ docExcel, setDocExcel] = React.useState(null)

  const header = [];
  const excel = [];
  header.push("Unidad");

  detailsColumns.forEach((item) => {


    header.push(item.label);


   })

   console.log("header");
   console.log(header)

function handleDownloadExcel() {
  downloadExcel({
    fileName: "ReporteValidacionEfectivo",
    sheet: "Transacciones",
    tablePayload: {
      header,

      body: excel,
    },
  });
}

const handleDate = () => {
  setFromDate(startDate);
  setToDate(finalDate);
}

return (
  <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
   
   
   <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Ventas en Efectivo</Box>
    
   
   <Box>

    <TableCell sx={{ marginLeft:0,marginTop:10,width: '100vw'}}>
   

    <LocalizationProvider dateAdapter={DateAdapter}>
      <DatePicker label='Fecha Inicial' value={startDate} 
      inputFormat="dd/MM/yy"
      onChange={(newValue) => setStartDate(newValue)}
      renderInput={(params) => <TextField variant="standard" 
      sx={{width: 220,marginLeft:3,justifyContent: 'left'}} 
      error={false} {...params} />} />

   </LocalizationProvider>

   <LocalizationProvider dateAdapter={DateAdapter}>
      <DatePicker label='Fecha Final'  value={finalDate} 
      inputFormat="dd/MM/yy"
      onChange={(newValue) => setFinalDate(newValue)}
      renderInput={(params) => <TextField variant="standard" 
      sx={{width: 220,marginLeft:4}} 
      error={false} {...params} />} />

   </LocalizationProvider>

   
    <IconButton   sx={{marginLeft:7, marginTop:1.6, ":hover": {
        bgcolor: "#F2F4F4",
        color: "#1661c9"
      }}}  variant="solid" onClick={handleDate}>
                <RiSearchLine />
                                      
    </IconButton>

    <IconButton  sx={{marginLeft:3, marginTop:1.5 , ":hover": {
        bgcolor: "#F2F4F4",
        color: "#1661c9"
      }}}  variant="solid"  onClick={handleDownloadExcel}>
        <RiFileExcel2Fill />
                                                        
    </IconButton>


    </TableCell>
  
    </Box>
    <Box sx={{display: 'flex' ,justifyContent: 'flex-start'}}>
              {columns.map((column) => (
                  < Box
                  key={column.id}
                  sx={{marginLeft:column.marginL, minWidth:column.minWidth}}
                  
                  >
                    {column.label}
                  </Box>
                ))}
                          
     </Box>                 


  <TableBody>
  {createData(ventas, 'vehiculo').map(( row, index ) => (
    <CustomRow key={index} row={row} fields={columns} detailFields={detailsColumns} excel={excel} ></CustomRow>
  ))}
</TableBody>




    
    
    <Loading status={loading}></Loading>
  </Paper>
)
}

export {Ventas}