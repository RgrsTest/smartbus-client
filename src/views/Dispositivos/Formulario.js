import { Box, Button, Card, CardActions, CardContent, CardHeader, Modal, TextField } from "@mui/material";
import { Loading } from '../components/Loading'
import React from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };


function Formulario( { status, setStatus, success, setSuccess, action, item = null } ){
    const [ externalId,  setExternalId] = React.useState('')
    const [ label, setLabel ] = React.useState('')
    const [ numeroTelefono, setNumeroTelefono ] = React.useState('')
    const [ errorExternalId, setErrorExternalId ] = React.useState(false)
    const [ errorLegendExternalId, setErrorLegendExternalId ] = React.useState('')
    const [ errorLabel, setErrorLabel ] = React.useState(false)
    const [ errorLegendLabel, setErrorLegendLabel ] = React.useState('')
    const [ errorNumeroTelefono, setErrorNumeroTelefono ] = React.useState(false)
    const [ errorLegendNumeroTelefono, setErrorLegendNumeroTelefono ] = React.useState('')
    const [ buttonAction, setButtonAction ] = React.useState('Agregar')
    const [ title, setTitle ] = React.useState('Crear un nuevo dispositivo')

    const handlePayload = () => {
        if (item){
            setButtonAction('Guardar')
            item.payload.external_id && setExternalId(item.payload.external_id)
            item.payload.label && setLabel(item.payload.label)
            item.payload.numero_telefono && setNumeroTelefono(item.payload.numero_telefono)
            setTitle('Editar un dispositivo')
        }
        else {
            setExternalId('')
            setLabel('')
            setNumeroTelefono('')
            setTitle('Crear un nuevo dispositivo')
            setButtonAction('Agregar')
        }
    }

    React.useEffect(() => handlePayload(), [item])

    const handleNumeroTelefonico = (event) => {
        if (event.charCode < 48 || event.charCode > 57 ){
            event.preventDefault()
        }
        else if ( !(event.charCode < 48 || event.charCode > 57 ) && numeroTelefono.length >= 10 ){
            event.preventDefault()
        }
        else {
            if ( errorNumeroTelefono ) {
                setErrorNumeroTelefono(false)
                setErrorLegendNumeroTelefono('')
            }
        }
    }

    const handleLabel = () => {
        if ( errorLabel ) {
            setErrorLabel(false)
            setErrorLegendLabel('')
        }
    }

    const handleExternalId = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && externalId.length >= 4 ){
            event.preventDefault()
        }
        else {
            if ( errorExternalId ) {
                setErrorExternalId(false)
                setErrorLegendExternalId('')
            }
        }
    }

    const handleExternalIdBlur = (event) => {
        if (!label) setLabel(event.target.value.toUpperCase())
    } 

    const handleOnClose = () => {
        setExternalId('')
        setLabel('')
        setNumeroTelefono('')
        setErrorExternalId(false)
        setErrorLabel(false)
        setErrorNumeroTelefono(false)
        setErrorLegendExternalId('')
        setErrorLegendLabel('')
        setErrorLegendNumeroTelefono('')
        setStatus(false)
    }
    const handleUpdate = async () => {
        let ok = true
        if ( !externalId ) {
            setErrorExternalId(true)
            setErrorLegendExternalId('Este campo es obligatorio')
            ok = false
        }
        else if ( externalId.length < 4 ) {
            setErrorExternalId(true)
            setErrorLegendExternalId('External ID debe ser un numero hexadecimal de 4 digitos')
            ok = false
        }
        if ( !label ) {
            setErrorLabel(true)
            setErrorLegendLabel('Este campo es obligatorio')
            ok = false
        }
        if ( !numeroTelefono ){
            setErrorNumeroTelefono(true)
            setErrorLegendNumeroTelefono('Este campo es obligatorio')
            ok = false
        }
        else if ( numeroTelefono.length < 10  ) {
            setErrorNumeroTelefono(true)
            setErrorLegendNumeroTelefono('El numero de telefono es invalido')
            ok = false
        }

        if (ok) {
            let send = null
            if (item) {
                send =  await action({ external_id: externalId.toLowerCase(), label: label.toUpperCase(), numero_telefono: numeroTelefono}, item.id)
            }
            else {
                send =  await action({ external_id: externalId.toLowerCase(), label: label.toUpperCase(), numero_telefono: numeroTelefono})
            }
            if ( send ) {
                setExternalId('')
                setLabel('')
                setNumeroTelefono('')
                setSuccess(!success)
                setStatus(false)                
            }
        }
    }

    return (
        <Modal open={ status }>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                            <TextField sx={{ m: 2, width: 400 }} error={ errorExternalId } helperText={ errorLegendExternalId } label='External ID' value={ externalId } onKeyPress={(e) => handleExternalId(e)} onChange={ (e) => setExternalId(e.target.value) } onBlur={(e) => handleExternalIdBlur(e)} />
                            <TextField sx={{ m: 2, width: 400 }} error={ errorLabel } helperText={ errorLegendLabel } label='Label' value={ label } onKeyPress={() => handleLabel()} onChange={ (e) => setLabel(e.target.value) } />
                            <TextField sx={{ m: 2, width: 400 }} error={ errorNumeroTelefono } helperText={ errorLegendNumeroTelefono } label='Numero telefono' value={ numeroTelefono } onKeyPress={(e) => handleNumeroTelefonico(e)} onChange={(e) => setNumeroTelefono(e.target.value)} />
                    </CardContent>
                    <CardActions>
                        <Button onClick={()  => handleUpdate() }>
                            { buttonAction }
                        </Button>
                        <Button onClick={ () => handleOnClose() }>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Loading></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}