import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useDispositivos() {
    const { jwt } = React.useContext(Context)
    const [ dispositivos, setDispositivos ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const dispositivosCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'dispositivos',
    }
    
    async function getDispositivos(busqueda = null){
      if (busqueda) {
        
      }
      setLoading(true)
      try{
        const response = await dispositivosCatalogo.consultarCatalogo(payload)
        setDispositivos(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }

    
    async function updateDispositivos(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await dispositivosCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteDispositivos(id){
      try{
        payload.id = id
        const response = await dispositivosCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          return true
        }
      }
      catch (e){
        setError(true)
      }
      return false
    }

    React.useEffect(getDispositivos, [])

    return {
      dispositivos,
      loading,
      error,
      setLoading,
      setError,
      getDispositivos,
      updateDispositivos,
      deleteDispositivos
    }
}

export {useDispositivos}