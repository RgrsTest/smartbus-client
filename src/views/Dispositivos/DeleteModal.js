import { Box, Button, Card, CardActions, CardContent, CardHeader, Modal, Typography } from "@mui/material";
import React from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function DeleteModal( { status, setStatus, success, setSucess, action, item=null}) {

    const hanldeDeleteDispositivos = async () => {
        setStatus(false)
        const response = await action(item.id)
        if( response ){
            setSucess(!success)
        }
    }

    return (
        <Modal open={ status } >
            <Box style={ style }>
                <Card>
                    <CardHeader title="Eliminar dispositivo" />
                    <CardContent>
                        <Typography>
                            ¿Esta seguro que desea eliminar el dispositivo { item && item.payload.label }?
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => hanldeDeleteDispositivos()} >
                            Si
                        </Button>
                        <Button onClick={() => setStatus(false)}>
                            No
                        </Button>
                    </CardActions>
                </Card>
            </Box>
        </Modal>
    )
}

export { DeleteModal }