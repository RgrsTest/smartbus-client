import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import { Avatar, Box, IconButton, List, ListItem, ListItemAvatar, ListItemText, TextField, Button, Switch, Modal,Paper } from "@mui/material";
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import { useDispositivos } from "./useDispositivos";
import { DeleteModal } from "./DeleteModal";
import { Loading } from "../components/Loading";
import { ErrorModal } from "./ErrorModal";

const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Dispositivos(){
  const [ change, setChange ] = React.useState(false)
  const { loading, dispositivos, error, getDispositivos, updateDispositivos, deleteDispositivos, setError } = useDispositivos()
  const [ modal, setModal ] = React.useState(false)
  const [ search, setSearch ] = React.useState('')
  const [ payload, setPayload ] = React.useState(null)
  const [ localDispositivos, setLocalDispositivos ] = React.useState([])
  const [ deleteModal, setDeleteModal ] = React.useState(false)

  React.useEffect(() =>{
    if( search.length > 0 ){
      let searching = search.toLocaleLowerCase()
      let valores = dispositivos.filter((item) => {
        const itemText = item.payload.label.toLowerCase()
        return itemText.includes(searching)
      })
      setLocalDispositivos(valores)
    }else{
      setLocalDispositivos(dispositivos)
    }
  },[dispositivos, search])

  React.useEffect(() => { getDispositivos()}, [change])

  return (
    <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Dispositivos</Box>
    <Box  sx={{width:"95%", marginTop:1, marginLeft:4}}>
      <Container >
        <TextField sx={{width: '50%', mr: '10%'}}
        placeholder="Escriba para buscar"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        />
        <Button sx={{width: '40%', height: 50}} onClick={() => { setModal(true); setPayload(null)}} variant='contained' >
          Agregar
        </Button>
      
      </Container> 
      <List >
        {localDispositivos.map((item, index) => (
          <ListItem
          key={index}
            secondaryAction={
                <Box>
                    <IconButton edge="end" aria-label="edit" onClick={ () => {setModal(true); setPayload(item)}}>
                        <EditIcon />
                    </IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setDeleteModal(true); setPayload(item)}}>
                        <DeleteIcon />
                    </IconButton>
                </Box>
            }
          >
            <ListItemAvatar>
              <Avatar>
                <PointOfSaleIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={item.payload.label}
              secondary={item.payload.numero_telefono}
            />
          </ListItem>
        ))}
      </List>
      <ErrorModal status={ error } setStatus={setError} />
      <Loading status={ loading } />
      <Formulario status={ modal } setStatus={ setModal } success={ change } setSuccess={ setChange } item={ payload } action={ updateDispositivos } />
      <DeleteModal status={ deleteModal } setStatus={ setDeleteModal } success={ change } setSucess={ setChange } item={ payload } action={ deleteDispositivos } />
    </Box> </Paper>
  )
}

export {Dispositivos}