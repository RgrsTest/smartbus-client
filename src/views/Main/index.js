import React from "react"
import { Layout } from "../Layout"
import { Rutas } from "../Rutas"
import { Dispositivos } from "../Dispositivos"
import { Vehiculos } from "../Vehiculos"
import { Dashboard } from "../Dashboard"
import DashboardIcon from '@mui/icons-material/Dashboard';
import BusIcon from '@mui/icons-material/DirectionsBus';
import PointOfSaleIcon from '@mui/icons-material/PointOfSale';
import RouteIcon from '@mui/icons-material/Route';
import VentasIcon from '@mui/icons-material/AttachMoney';
import RecargasIcon from '@mui/icons-material/LocalAtm';
import ValidacionesIcon from '@mui/icons-material/Contactless';
import LocalizacionIcon from '@mui/icons-material/MyLocation';
import TrackingIcon from '@mui/icons-material/Directions';
import OperadorIcon from '@mui/icons-material/Face';
import PerfilIcon from '@mui/icons-material/PeopleAlt';
import ProductosIcon from '@mui/icons-material/ShoppingCart';
import TransbordoIcon from '@mui/icons-material/Transform';
import LamsIcon from '@mui/icons-material/FactCheck';
import LaprsIcon from '@mui/icons-material/List';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';

import { Context} from "../../Context"
import { Login } from "../Login"
import { BrowserRouter as Router, Routes as  Switch, Route, Navigate, useLocation  } from "react-router-dom"
import { Ventas } from "../Ventas"
import { Validaciones } from "../Validaciones"
import { Recargas } from "../Recargas"
import { Localizacion } from "../Localizacion"
import { Tracking } from "../Tracking"
import { Operadores } from "../Operadores"
import { Perfiles } from "../Perfiles"
import { Productos } from "../Productos"
import { Transbordos } from "../Transbordos"
import { Lams } from "../Lams"
import { Laprs } from "../Lapsr"
import { Lapv } from "../Lapv"
import { Fimpe } from "../Fimpe"


function RequireAuth(props){
    const { jwt, auth } = React.useContext(Context)
    const location = useLocation()

    if (!auth && jwt === ''){
        return <Navigate to='/login' state={{from: location}} replace />
    }
    return props.children
}


function Main() {
    const routes = [
        { text: 'Dashboard', icon: DashboardIcon, link: '/', component: Dashboard }, 
        { text: 'Ventas', icon: VentasIcon, link: '/ventas', component: Ventas },
        { text: 'Validaciones', icon: ValidacionesIcon, link: '/validaciones', component: Validaciones },
        { text: 'Recargas', icon: RecargasIcon, link: '/recargas', component: Recargas },
       // { text: 'Localizacion', icon: LocalizacionIcon, link: '/localizacion', component: Localizacion },
        { text: 'Tracking', icon: TrackingIcon, link: '/tracking', component: Tracking },
        { text: 'Vehiculos', icon: BusIcon, link: '/vehiculos', component: Vehiculos },
        { text: 'Operadores', icon: OperadorIcon, link: '/operadores', component: Operadores },
        { text: 'Dispositivos', icon: PointOfSaleIcon, link: '/dispositivos', component: Dispositivos },
        { text: 'Rutas', icon: RouteIcon, link: '/rutas', component: Rutas },
        { text: 'Perfiles', icon: PerfilIcon, link: '/perfiles', component: Perfiles },
        { text: 'Productos', icon: ProductosIcon, link: '/productos', component: Productos },
        { text: 'Transbordos', icon: TransbordoIcon, link: '/transbordos', component: Transbordos },
        { text: 'Listas LAM', icon: LamsIcon, link: '/lams', component: Lams},
        { text: 'Listas LAPR', icon: LaprsIcon, link: '/laprs', component: Laprs},
        { text: 'Listas LAPV', icon: LaprsIcon, link: '/lapv', component: Lapv},
        { text: 'FIMPE', icon: CloudUploadIcon, link: '/fimpe', component:Fimpe}
    ]

    return (
        <Router>
            <Switch>
                <Route path="/login" element={<Login />} />
                    {routes.map((item, index) => {
                        return (
                            <Route key={index} index={item.link === '/' ? true : false} path={item.link} element={
                                <Layout routes={routes}>
                                    <RequireAuth >
                                    <item.component />
                                    </RequireAuth> 
                                </Layout> }  />
                        )
                    })}
            </Switch>
        </Router>
            
    )
}

export {Main}