import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import TransbordoIcon from '@mui/icons-material/Transform';
import { Avatar, Box, IconButton, List, ListItem, ListItemAvatar, ListItemText, TextField, Button, Paper ,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions  } from "@mui/material";
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import { useTransbordos } from "./useTransbordos";
import { Loading } from "../components/Loading";
import { DeleteModal } from "./DeleteModal";
import { useVersiones } from "../components/useVersiones";
import dayjs from 'dayjs';





const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Transbordos(){
  const [ change, setChange ] = React.useState(false)
  const { transbordos, loading, updateTransbordos, deleteTransbordos, getTransbordos } = useTransbordos()
  const [ modal, setModal ] = React.useState(false)
  const [ search, setSearch ] = React.useState('')
  const [ localTransbordos, setLocalTransbordos ] = React.useState([])
  const [ payload, setPayload ] = React.useState(null)
  const [ deleteModal, setDeleteModal ] = React.useState(false)


  const { versiones, loadingVersiones, getVersiones, updateVersiones, deleteVersiones } = useVersiones("Transbordos")
  const [openConfirmUpdateDialog, setOpenConfirmUpdateDialog] = React.useState(false);
  const [lastUpdate, setLastUpdate] = React.useState(null);



  React.useEffect(() =>{
   
    if(versiones[versiones.length - 1] != undefined)
    {
      setLastUpdate(versiones[versiones.length - 1].payload.ultima_actualizacion);
    }

  },[versiones])

  const handleOpenConfirmUpdate = () => {
    setOpenConfirmUpdateDialog(true);
  };

  const handleCloseConfirmUpdate = () => {
    setOpenConfirmUpdateDialog(false);
  };
  

  const handleConfirmUpdate = async () => {
    let send = null
    console.log("versiones")
    console.log(versiones)
    if (versiones[versiones.length - 1] != undefined){
      console.log("not undefined")
        send = await updateVersiones({catalogo:"Transbordos", ultima_actualizacion: dayjs().format('YYYY-MM-DD HH:mm:ss')}, versiones[versiones.length - 1].id)
    }
    else{
        send = await updateVersiones({catalogo:"Transbordos", ultima_actualizacion: dayjs().format('YYYY-MM-DD HH:mm:ss')})
    }

    if (send){
      setLastUpdate(dayjs().format('YYYY-MM-DD HH:mm:ss'));
      getVersiones();
    }

    handleCloseConfirmUpdate();
  };


  
  React.useEffect(() =>{
    
    if(search.length > 0){
      let searching = search.toLocaleLowerCase()
      let valores = transbordos.filter((item) => {
        const itemText = item.payload.label.toLowerCase()
        return itemText.includes(searching)
      })
        setLocalTransbordos(valores)
    }else{
      setLocalTransbordos(transbordos)
    }
  },[search, transbordos])

  React.useEffect(getTransbordos, [change])
  return (

    <Paper elevation={0}  sx={{width:"95%", marginTop:10, marginLeft:4}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:0, fontSize: 35}}>Transbordos</Box>
    

    <Dialog
        open={openConfirmUpdateDialog}
        onClose={handleCloseConfirmUpdate}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Esta seguro en actualizar todos los dispositivos?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Se actualizara la configuracion de los transbordos 
            en todos los dispositivos.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmUpdate}>Cancelar</Button>
          <Button onClick={handleConfirmUpdate} autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>



 
      <Box
      sx={{flexGrow: 1}}
      >
        <Container >
        <TextField sx={{width: '30%', mr: '2%'}}
          placeholder="Escriba para buscar"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          />
          
          <Button sx={{width: '8%', height: 50,mr: '2%'}} onClick={() => {setModal(true); setPayload(null)}} variant='contained' >
            Agregar
          </Button>

          <Button  sx={{width: '18%', height: 50,mr: '20%'}} onClick={handleOpenConfirmUpdate} variant='contained' >
            Actualizar Dispositivos
          </Button>

          <Box sx={{display: 'flex', marginBottom:0, marginLeft:0, fontSize: 20}}>Ultima actualizacion:{lastUpdate}</Box>
        
        </Container> 
        <List >
          {localTransbordos.map((item, index)=>(
            <ListItem
            key={index}
              secondaryAction={
                  <Box>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setModal(true); setPayload(item)}}>
                        <EditIcon />
                    </IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setDeleteModal(true); setPayload(item)}} >
                        <DeleteIcon />
                    </IconButton>
                  </Box>
              }
            >
              <ListItemAvatar>
                <Avatar>
                  <TransbordoIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.payload.nombre}
                secondary={item.payload.external_id}
              />
            </ListItem>
          ))}
        </List>
        <DeleteModal status={ deleteModal } setStatus={ setDeleteModal } success={ change } setSucess={ setChange } item={ payload } action={ deleteTransbordos } loading={loading} />
        <Loading status={loading}></Loading>
        <Formulario status={modal} loading={loading} success={change} setSuccess={setChange} setStatus={setModal} action={updateTransbordos} item={payload}></Formulario>
      </Box></Paper>
  )
}

export {Transbordos}