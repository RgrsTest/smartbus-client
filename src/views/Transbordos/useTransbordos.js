import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useTransbordos() {
    const { jwt } = React.useContext(Context)
    const [ transbordos, setTransbordos ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const transbordosCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'transbordos',
    }
    
    async function getTransbordos(){
      setLoading(true)
      try{
        const response = await transbordosCatalogo.consultarCatalogo(payload)
        setTransbordos(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updateTransbordos(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await transbordosCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteTransbordos(id){
      setLoading(true)
      try{
        payload.id = id
        const response = await transbordosCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)
      }
      setLoading(false)
      return false
    }


    React.useEffect(getTransbordos, [])
    return {
      transbordos,
      loading,
      error,
      setLoading,
      setError,
      getTransbordos,
      updateTransbordos,
      deleteTransbordos
    }
}

export {useTransbordos}