import { Box, Button, Card, CardActionArea, List, CardActions, CardContent, CardHeader, FormGroup, FormLabel, IconButton, InputBase, ListItem, Modal, TextField, ListItemButton } from "@mui/material";
import React from "react";
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from "@mui/icons-material/Delete";
import { Loading } from "../components/Loading";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function Formulario( { status, setStatus, loading, success, setSuccess, action, item = null } ){
    const [ externalId,  setExternalId] = React.useState('')
    const [ label, setLabel ] = React.useState('')
    const [ transbordos, setTransbordos ] = React.useState([])
    const [ errorExternalId, setErrorExternalId ] = React.useState(false)
    const [ errorLabel, setErrorLabel ] = React.useState(false)
    const [ errorTransbordos, setErrorTransbordos ] = React.useState(false)
    const [ errorLegendExternalId, setErrorLegendExternalId ] = React.useState('')
    const [ errorLegendLabel, setErrorLegendLabel ] = React.useState('')
    const [ errorLegendTransbordo, setErrorLegendTransbordos ] = React.useState('')
    const [ localModal, setLocalModal ] = React.useState(false)
    const [ idRuta, setIdRuta ] = React.useState('')
    const [ tiempo, setTiempo ] = React.useState('')
    const [ tarifa, setTarifa ] = React.useState('')
    const [ errorIdRuta, setErrorIdRuta ] = React.useState(false)
    const [ errorTiempo, setErrorTiempo ] = React.useState(false)
    const [ errorTarifa, setErrorTarifa ] = React.useState(false)
    const [ errorLegendIdRuta, setErrorLegendIdRuta ] = React.useState('')
    const [ errorLegendTiempo, setErrorLegendTiempo ] = React.useState('')
    const [ errorLegendTarifa, setErrorLegendTarifa ] = React.useState('')
    const [ title, setTitle ] = React.useState('Crear nuevo transbordo')
    const [ button, setButton ] = React.useState('Agregar')

    const handlePayload = () =>{
        if (item){
            setButton('Guardar')
            setTitle('Editar ruta')

            item.payload.external_id && setExternalId(item.payload.external_id)
            item.payload.label && setLabel(item.payload.label)
            setTransbordos(item.payload.transbordos)
        }
        else{
            setButton('Agregar')
            setTitle('Crear nuevo transbordo')
            setExternalId('')
            setLabel('')
            setTransbordos([])
        }
    }

    React.useEffect(handlePayload, [item])

    const closeModal = () => {
        setExternalId('')
        setLabel('')
        setTransbordos([])
        setErrorExternalId(false)
        setErrorLabel(false)
        setErrorTransbordos(false)
        setErrorLegendExternalId('')
        setErrorLegendLabel('')
        setErrorLegendTransbordos('')
        setStatus(false)
    }

    const handleLabel = () => {
        if ( errorLabel ) {
            setErrorLabel(false)
            setErrorLegendLabel('')
        }
    }

    

    const handleExternalId = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && externalId.length >= 4 ){
            event.preventDefault()
        }
        else {
            if ( errorExternalId ) {
                setErrorLegendExternalId('')
            }
        }
    }

    const handleUpdateTransbordos = async () => {
        let ok = true
        if ( !externalId ) {
            setErrorExternalId(true)
            setErrorLegendExternalId('Este campo es obligatorio')
            ok = false
        }
        else if ( externalId.length < 4 ) {
            setErrorExternalId(true)
            setErrorLegendExternalId('External ID debe ser un numero hexadecimal de 4 digitos')
            ok = false
        }
        if ( !label ) {
            setErrorLabel(true)
            setErrorLegendLabel('Este campo es obligatorio')
            ok = false
        }
        if ( transbordos.length === 0 ) {
            setErrorTransbordos(true)
            setErrorLegendTransbordos('Este campo es obligatorio')
            ok = false
        }
        
        if (ok) {
            
            let send = null
            if (item) {
                send =  await action({ external_id: externalId.toLowerCase(), label: label.toUpperCase(), transbordos: transbordos}, item.id)
            }
            else {
                send =  await action({ external_id: externalId.toLowerCase(), label: label.toUpperCase(), transbordos: transbordos})
            }

            if (send){
                setExternalId('')
                setLabel('')
                setTransbordos([])
                setStatus(false)
                setSuccess(!success)
            }
        }
    }

    const handleAgregarTransbordo = () => {
        let ok = true
        if (idRuta.length === 0){
            setErrorIdRuta(true)
            setErrorLegendIdRuta('Este campo es obligatorio')
            ok = false
        }
        if (tiempo.length === 0){
            setErrorTiempo(true)
            setErrorLegendTiempo('Este campo es obligatorio')
            ok = false
        }
        if (tarifa.length === 0){
            setErrorTarifa(true)
            setErrorLegendTarifa('Este campo es obligatorio')
            ok = false
        }
        if (ok){
            let actual = transbordos
            actual.push({id_ruta: idRuta.toLowerCase(), tiempo: parseInt(tiempo), tarifa: parseInt(tarifa)})
            setTransbordos(actual)
            handleCloseLocalModal()
        }
    }

    const handleRemoveTransbordo = (valor) => {
        setTransbordos(transbordos.filter((buscado) =>  valor !== buscado ))
    }

    const handleCloseLocalModal = () => {
        setIdRuta('')
        setTiempo('')
        setTarifa('')
        setErrorIdRuta(false)
        setErrorLegendIdRuta('')
        setErrorTiempo(false)
        setErrorLegendTiempo('')
        setErrorTarifa(false)
        setErrorLegendTarifa('')
        setLocalModal(false)
    }

    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                        <TextField required error={errorExternalId} helperText={errorLegendExternalId} onKeyPress={(e) => handleExternalId(e)} sx={{m: 2, width: 400}} label='External ID' value={ externalId } onChange={ (e) => setExternalId(e.target.value) } />
                        <TextField required error={errorLabel} helperText={errorLegendLabel} onKeyPress={handleLabel} sx={{m: 2, width: 400}} label='Label' value={ label } onChange={ (e) => setLabel(e.target.value) } />
                        <FormGroup>
                            <FormLabel sx={{m: 1}}>Transbordos</FormLabel>
                            <List sx={{maxHeight: 80, overflow: 'auto'}} dense>
                                {
                                    transbordos.map((item, index) => (
                                        <ListItem key={index}>
                                            <Box>
                                                <IconButton onClick={() => handleRemoveTransbordo(item)} >
                                                    <DeleteIcon />
                                                </IconButton>
                                            </Box>
                                            {item.id_ruta}
                                        </ListItem>
                                    ))
                                }
                            </List>
                            <IconButton onClick={() => setLocalModal(!localModal)}>
                                <AddIcon />
                            </IconButton>
                        </FormGroup>
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleUpdateTransbordos()}>
                            {button}
                        </Button>
                        <Button onClick={() => closeModal()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Modal open={localModal}>
                    <Card  style={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: 500,
                        bgcolor: 'background.paper',
                        boxShadow: 24,
                        p: 4
                    }}>
                        <CardHeader component="div" title="Agregar Transbordo" />
                        <CardContent>
                            <TextField helperText={errorLegendIdRuta} error={errorIdRuta} onChange={(e) => setIdRuta(e.target.value)} value={idRuta} sx={{m: 1, width: 417}} label='ID Ruta' />
                            <TextField helperText={errorLegendTiempo} error={errorTiempo} onChange={(e) => setTiempo(e.target.value)} value={tiempo} type='number' sx={{m: 1, width: 200}} label='Tiempo de espera' />
                            <TextField helperText={errorLegendTarifa} error={errorTarifa} onChange={(e) => setTarifa(e.target.value)} value={tarifa} type='number' sx={{m: 1, width: 200}} label='Tarifa' />
                        </CardContent>
                        <CardActions>
                            <Button onClick={() => handleAgregarTransbordo()}>
                                Agregar
                            </Button>
                            <Button onClick={() => handleCloseLocalModal()}>
                                Cancelar
                            </Button>
                        </CardActions>
                    </Card>
                </Modal>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}