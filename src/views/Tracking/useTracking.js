import React from 'react';
import { Documentos } from '../../Api';
import { Context } from '../../Context';

function useTracking(){
    const { jwt } = React.useContext(Context)
    const [ tracking, setTracking ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const documento = Documentos(jwt)
    const payload = {
        schema: 'tracking'
    }

    async function getTracking(vehiculo, date) {
        setLoading(true)
        payload['with-storage'] = ['*']
        payload['filter'] = {where: [{field: 'payload->encabezado->fecha', operator: '=', value: date}, {field: 'payload->encabezado->id_vehiculo', operator: '=', value: vehiculo}]}
        console.log("tracking: ")
        console.log(payload)
        const localizacion = await documento.consultar(payload)
        if (localizacion.status == 'success'){
            console.log(localizacion)
            setTracking(localizacion.data)
        }
        else{
            setTracking([])
        }

        
        setLoading(false)
    }

    return {
        loading,
        tracking,
        getTracking
    }
}

export {useTracking}