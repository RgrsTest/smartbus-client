import { Box, Collapse, IconButton, Table, TableBody, TableCell, TableHead, TableRow, Typography,Slider } from "@mui/material";
import ArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import React, { useState } from "react";




import { RiFileExcel2Fill ,RiSearchLine } from "react-icons/ri";
import { IoSearchCircleSharp } from "react-icons/io5";
import { SlRefresh } from "react-icons/sl"

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import TimePicker from 'react-time-picker';



function valuetext(value) {
    return `${value}°C`;
  }




function DatePickerTracking({startDate,setStartDate ,handleDate}) {
    const [ open, setOpen ] = React.useState(false)
  
    return (
        
        <React.Fragment>
                           
                            <TableRow  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                             
                                

                                <TableCell align="left" size="small"  sx={{ mx: 'auto', width: 200 }}> 
                                 Fecha: 
                                <DatePicker  dateFormat="dd/MM/yy" selected={startDate} onChange={(date) => setStartDate(date)} />    
                                </TableCell>


                                <TableCell>
                                             <TimePicker/>
                                </TableCell>
                                
                                <TableCell>
                                             <TimePicker/>
                                </TableCell>

                               
                               
                                
                                <TableCell align="left" size="small">
                                    
                                    <IconButton variant="solid" onClick={handleDate}>
                                         <RiSearchLine />
                                        
                                    </IconButton>

                                </TableCell>


                                <Box sx={{ width: 300 }}>
                                <Slider
                                    aria-label="Temperature"
                                    defaultValue={30}
                                    getAriaValueText={valuetext}
                                    valueLabelDisplay="auto"
                                    step={10}
                                    marks
                                    min={10}
                                    max={110}
                                />
                                
                                </Box>

                           

                              



                               
                             

                              

                                
                               
                               
                                
                                

                                

                                
                              
                            </TableRow>
                          
                       
        </React.Fragment>
    )
}

export {DatePickerTracking}