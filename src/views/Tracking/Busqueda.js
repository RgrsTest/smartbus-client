import React from "react";
import { Card, CardHeader, CardContent, TextField} from '@mui/material'

function Busqueda(){
    return (
        <Card>
            <CardHeader component="div" title='Buscar tracking' />
            <CardContent>
                <TextField />
            </CardContent>
        </Card>
    )
}

export { Busqueda }