import React from "react";
import 'leaflet/dist/leaflet.css'
import Leaflet from 'leaflet' 


//import { MapContainer, TileLayer, Popup, Marker, Tooltip, LayersControl, Polyline, Circle, CircleMarker } from 'react-leaflet'
import { useTracking } from "./useTracking";
import { Autocomplete,Box, IconButton, Slider, TextField,Typography, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow,CardHeader ,Container, ListItem} from "@mui/material";
import { useVehiculos } from "../Vehiculos/useVehiculos";
import { DatePicker, LocalizationProvider,TimePicker} from "@mui/lab";

import DateAdapter from '@mui/lab/AdapterDateFns'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';


import SearchIcon from '@mui/icons-material/Search';
import { Loading } from "../components/Loading";

import {GoogleMap,useLoadScript, InfoWindow,Marker,Polyline} from "@react-google-maps/api";

import {  Tooltip, CircleMarker } from 'react-leaflet'

import dayjs from 'dayjs';


import { RiMapPin2Fill } from "react-icons/ri";

import { VscDebugBreakpointData } from "react-icons/vsc";


var icono = Leaflet.icon({
  iconUrl: '/assets/camion_conectado.png',
  iconSize: [20, 20]
})

function createPath(data) {

  
  return data.map((item) => {
   
      return [item.latitud, item.longitud]
    })
    
  }



function createPath2(data,fromTime,toTime) {

  
  let path = [];
  
   data.forEach((item) => {
    if(item.hora >= fromTime && item.hora <= toTime){
      console.log("hora filtered" + item.hora)
      path.push({lat: item.latitud, lng: item.longitud})
     
   
    }
    
  })

  return path
}

function createMarks(data){
  let result = data.map((element,index) => {
    console.log("mark value:" + (100/data.length) * index)
    console.log("mark label:" + element.label)
    return {
      value: (100/data.length) * index,
      label: element.label
    }
  })
  return result
}

export default function Home(pathCoordinates,markerPos,vehiculo,points) {


  const [selectedCenter, setSelectedCenter] = React.useState([]);


  const center = React.useMemo(() => ({ lat: 20.6763989, lng: -103.3479102 }), []);

 /* const pathCoordinates = [
    { lat: 20.6763989,lng:  -103.3479102 },
    { lat: 20.681423, lng: -103.412683 }
];*/

const options = {
  strokeColor: '#FF0000',
  strokeOpacity: 0.8,
  strokeWeight: 3,
  fillColor: '#FF0000',
  fillOpacity: 0.35,
  clickable: false,
  draggable: false,
  editable: false,
  visible: true,
  radius: 30000
 
  

};

const divStyle = {
  background: `white`,
  padding: 0
}





const onLoad = polyline => {
  console.log('polyline: ', polyline)
};



  console.log("PATH" );
  console.log( pathCoordinates);

  const { isLoaded } = useLoadScript({
    googleMapsApiKey:"AIzaSyB5Ze6Ew9nhetaeNN63Tf6X0HFNl-ueVYE",
  });

  if (!isLoaded) return <div>Loading...</div>;
  return <GoogleMap zoom={12} center={center} mapContainerClassName="map-container">
 
    {pathCoordinates.pathCoordinates.map((item,index) => (
        <Marker icon={"/assets/loc3.png"} position={item}
                onClick={() => {
                    console.log("Index " + index)
                   // console.log(pathCoordinates.points[index])

                    if(typeof selectedCenter !== 'undefined' && selectedCenter.length > 0)
                    {
                      let newSelected = [...selectedCenter];
                      newSelected[index] = item;
                      setSelectedCenter(newSelected);
                    }
                    else{
                      let newSelected =[];
                      newSelected[index] = item;
                      setSelectedCenter(newSelected);
                    }
                    
                    
                    
                }} >  
                  
           
            {selectedCenter[index] && (
                        <InfoWindow
                          
                        onCloseClick={() => {
                              

                          if(typeof selectedCenter !== 'undefined' && selectedCenter.length > 0)
                          {
                            let closeSelected = [...selectedCenter];
                            closeSelected[index] = null;
                            setSelectedCenter(closeSelected);
                          }
                          else{
                            let closeSelected =[];
                            closeSelected[index] = null;
                            setSelectedCenter(closeSelected);
                          }


                        
                         
                        }}
                        
                            position={item}
                          
                        >
                           <div style={divStyle}>
                  <h4>{pathCoordinates.points[index]['hora']}</h4>
                  <h4>Pasajeros:{pathCoordinates.points[index]['subidas']-pathCoordinates.points[index]['bajadas']}</h4>
               
                </div>
               
              </InfoWindow>
              )}

        </Marker>
    ))}
    



    <Polyline
      onLoad={onLoad}
      path={pathCoordinates.pathCoordinates}
      options={options}
    />

    
   
    
       

  </GoogleMap>;
  
}
/*
<Polyline positions={path} />
<Marker icon={icono} position={markerPos}>
  <Tooltip permanent={true}>{vehiculo}</Tooltip>
</Marker>
{ points.map((item, index) => (
  <CircleMarker center={[item.latitud, item.longitud]} radius={3} key={index} >
    <Tooltip>{`Hora ${item.hora} \n Coordenadas: ${item.latitud} , ${item.longitud} `}</Tooltip>
  </CircleMarker>
))}*/




function Tracking(){
  const { loading, tracking, getTracking } = useTracking()
  const { vehiculos, getVehiculos } = useVehiculos()
  const [ vehiculo, setVehiculo ] = React.useState(null)
  const [ dateSearch, setDateSearch ] = React.useState('')
  const [ sliderVisible, setSliderVisible ] = React.useState(false)
  const [ searchDisabled, setSearchDisabled ] = React.useState(true)
  const [ path, setPath ] = React.useState([])
  const [ marks, setMarks ] = React.useState([])
  const [ sliderValue, setSliderValue ] = React.useState(0)
  const [ markerPos, setMarkerPos ] = React.useState([0,0])
  const [ points, setPoints ] = React.useState([])

  const [ fromDate, setFromDate] = React.useState(new Date())
  const [ toDate, setToDate] = React.useState(new Date())

  const [ startDate, setStartDate] = React.useState(new Date())
  const [ finalDate, setFinalDate] = React.useState(new Date())


  const [startTime, setStartTime] = React.useState(dayjs().set('hour', 0).set('minute', 0).set('second', 0))
  const [finalTime, setFinalTime] = React.useState(dayjs().set('hour', 23).set('minute', 59).set('second', 59))




  const [path2,setPath2] = React.useState([])
 



  
  React.useEffect(getVehiculos, [])
  React.useEffect(() => {
    if (dateSearch && vehiculo){
      setSearchDisabled(false)
    }
    else {
      setSearchDisabled(true)
    }
  },[vehiculo, dateSearch])

  React.useEffect(() => {
    if (tracking.length > 0){
      if(tracking[0].payload.recorrido.length > 0)
      {
      let localPath = createPath(tracking[0].payload.recorrido)
      setPath(localPath)
      setMarks(createMarks(tracking[0].payload.recorrido))
      setMarkerPos(localPath[0])
      setPoints(tracking[0].payload.recorrido)
      //setSliderVisible(true)
      }
    }
  },[tracking])

  React.useEffect(() => {
    if (path.length > 0){
      setMarkerPos(path[sliderValue])
    }
  }, [sliderValue])

  const handleClickAction = () => {
    let vehiculoId = vehiculos.find((element) => element.payload.label === vehiculo)
    getTracking(vehiculoId.id, dateSearch.toISOString())
  }

  const handleShowLabel = (value, index) => {
    if(tracking[0].payload.recorrido.length > 0)
      {
        return tracking[0].payload.recorrido[value].hora
      }
  }

  const handleDate = () => {
    setFromDate(startDate);
    setToDate(finalDate);
  }


  React.useEffect(() => {
    if (tracking.length > 0){
      if(tracking[0].payload.recorrido.length > 0)
      {
        let localPath2 = createPath2(tracking[0].payload.recorrido,startTime.format("HH:mm:ss"),finalTime.format("HH:mm:ss"))
        console.log("localpath")
        console.log(localPath2)
        setPath2(localPath2)
      }
    }
    else{
      console.log("no tracking data")
    }
  },[startTime,finalTime])

  

  
  return (
    <div>
      <TableHead>         
          <TableCell  sx={{ mx: 'auto', width: 400 }}>
            <Typography className="title-validaciones" variant="h4" component='div'>Tracking</Typography></TableCell>

            <TableCell size="small">
                                  
          </TableCell>
        </TableHead>

      <Box sx={{width: '100%', height: 80, display: 'flex',marginTop:2}}>
        <Autocomplete 
        sx={{width: 200, marginRight: 2 }}
        options={vehiculos.map((item) => item.payload.label)}
        value={vehiculo}
        onChange={(e, newValue) => setVehiculo(newValue)}
        renderInput={(params) => <TextField {...params} label='Vehiculo' /> }
        />
        <LocalizationProvider dateAdapter={DateAdapter}>
          <DatePicker label='Seleccione Fecha' value={dateSearch}   onChange={(newValue) => setDateSearch(newValue)} renderInput={(params) => <TextField sx={{marginRight: 1}} error={false} {...params} />} />
      
        </LocalizationProvider>

        <IconButton onClick={() => handleClickAction()} sx={{marginBottom: 2}} disabled={searchDisabled} size='large'>
          <SearchIcon /> 
        </IconButton>

        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <TimePicker
            label="Hora Inicial"
            value={startTime}
            onChange={(newValue) => {
              setStartTime(newValue);
            }}
            renderInput={(params) => <TextField sx={{marginLeft: 4}} {...params} />}
          />
        </LocalizationProvider>


        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <TimePicker
            label="Hora Final"
            value={finalTime}
            onChange={(newValue) => {
              setFinalTime(newValue);
            }}
            renderInput={(params) => <TextField sx={{marginLeft:1 }} {...params} />}
          />
        </LocalizationProvider>
        

      
       
        
      </Box>
      <Box>
      { sliderVisible && <Slider marks={marks} track={false} valueLabelFormat={(value, index) => handleShowLabel(value, index)} valueLabelDisplay='auto' value={sliderValue} onChange={(e, newValue) => setSliderValue(newValue)} ></Slider > }
      </Box>

      <Home pathCoordinates={path2} markerPos={markerPos} vehiculo={vehiculo} points={points}/>
        
        <Loading status={loading}></Loading>
    </div>
    
  )
}

export {Tracking}
