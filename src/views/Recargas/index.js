import React from "react";
import { Paper, Table, TableBody,TextField, Box,TableCell, TableContainer, TableHead, TableRow,Stack,CardHeader ,Container,IconButton } from "@mui/material";
import { useRecargas } from "./useRecargas";
import { Loading } from "../components/Loading";
import { CustomRow } from "../components/CustomRow";
import { DatePickerRow } from "../components/DatePickerRow";
import { useDownloadExcel,downloadExcel } from 'react-export-table-to-excel';
import { DatePicker, LocalizationProvider,TimePicker} from "@mui/lab";
import DateAdapter from '@mui/lab/AdapterDateFns';
import { SlRefresh } from "react-icons/sl";
import { RiFileExcel2Fill ,RiSearchLine } from "react-icons/ri";

const columns = [
  { id: 'filtro', label:'Vehiculo',  marginL : 9,minWidth: 180 },
  { 
    id: 'recargas', 
    label: 'Recargas', 
    minWidth: 180,
    marginL : 6,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX')
  },
  { 
    id: 'recargas_recaudo', 
    label: 'Recargas Recaudo', 
    minWidth: 200,
    marginL : 8,
    align: 'right',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  }
]

const detailsColumns = [
  { id: 'fecha_hora_servidor', label: 'Fecha Hora Servidor'},
  { id: 'fecha_hora', label: 'Fecha y Hora' },
  { id: 'uid', label: 'Tarjeta' },
  { id: 'folio_transaccion', label: 'Folio de Transaccion'},
  { id: 'id_producto', label: 'Producto', format: (id) => productoNameByID(id) },
  { id: 'perfil', label: 'Perfil' , format: (id) => perfilNameByID(id)},
  {
    id: 'monto',
    label: 'Monto',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },
  { id: 'saldo_inicial', label:'Saldo Inicial'},
  { id: 'saldo_final', label:'Saldo Final'},
  {id:'id_sam',label:'ID SAM'},
  {id:'consecutivo_sam',label:'Consecutivo SAM'},
  {id:'latitud',label:'Ubicacion'},
  {id:'longitud'},
  { id: 'subidas', label: 'Subidas' },
  { id: 'bajadas', label: 'Bajadas' }
]

function uniqueValuesArray(data){
  let result = []
  data.forEach((item) => {
    if (result.findIndex((item) => item) === -1 ){
      result.push(item)}
  })
  return result
}

function arraySum(data){
  let result = 0
  data.forEach((item) =>{
    result += item
  })
  return result
}

function createData(data, filter){
  let result = []
  if(data.length > 0){
    const valores = data.map( (item) => item.payload )
    
    let filtro = uniqueValuesArray(valores.map((item) => item.encabezado._relaciones[filter].payload.label))

    filtro.forEach( (value) => {
      var recargas = valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value))
      var recargas_recaudo = arraySum(recargas.map((item) => item.encabezado.monto))
      
      let localValue = {
        filtro: value,
        recargas: recargas.length,
        recargas_recaudo: recargas_recaudo,
        details: valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value))
      }
      result.push(localValue)
    })
  }
  return result
}

var productosObj = {
 
};

var perfilesObj = {
 
};

function getProductos(data)
{
   
  if(data.length > 0){
 
    
    data.forEach((item) => {

        let key = item.payload.id_producto
        let value = item.payload.producto_nombre

        productosObj[key] = value;
    })
    }
}


function getPerfiles(data)
{
   
  if(data.length > 0){
 
    
    data.forEach((item) => {

        let key = item.payload.codigo_perfil
        let value = item.payload.usuario_tipo

        perfilesObj[key] = value;
    })

   
    }
  
  
}


function productoNameByID(identifier)
{
  console.log(identifier)
  return productosObj[identifier]
}

function perfilNameByID(identifier)
{
  
  console.log(identifier)
  return perfilesObj[identifier]
}




function Recargas(){

  const [ fromDate, setFromDate] = React.useState(new Date())
  const [ toDate, setToDate] = React.useState(new Date())

  const [ startDate, setStartDate] = React.useState(new Date())
  const [ finalDate, setFinalDate] = React.useState(new Date())

  const { loading,  recargas, productos,perfiles } = useRecargas(fromDate,toDate)



  getProductos(productos)
  getPerfiles(perfiles)


  

  const header = [];
  const excel = [];
  header.push("Unidad");

  detailsColumns.forEach((item) => {


    header.push(item.label);


   })

   console.log("header");
   console.log(header)

function handleDownloadExcel() {
  downloadExcel({
    fileName: "ReporteRecargas",
    sheet: "Transacciones",
    tablePayload: {
      header,

      body: excel,
    },
  });
}

const handleDate = () => {
  setFromDate(startDate);
  setToDate(finalDate);
}



  

  return (
    <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
     
     
     <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Recarga de Tarjetas</Box>
      
     
     <Box>

      <TableCell sx={{ marginLeft:0,marginTop:10,width: '100vw'}}>
     

      <LocalizationProvider dateAdapter={DateAdapter}>
        <DatePicker label='Fecha Inicial' value={startDate} 
        inputFormat="dd/MM/yyyy"
        onChange={(newValue) => setStartDate(newValue)}
        renderInput={(params) => <TextField variant="standard" 
        sx={{width: 220,marginLeft:3,justifyContent: 'left'}} 
        error={false} {...params} />} />

     </LocalizationProvider>

     <LocalizationProvider dateAdapter={DateAdapter}>
        <DatePicker label='Fecha Final'  value={finalDate} 
        inputFormat="dd/MM/yyyy"
        onChange={(newValue) => setFinalDate(newValue)}
        renderInput={(params) => <TextField variant="standard" 
        sx={{width: 220,marginLeft:4}} 
        error={false} {...params} />} />

     </LocalizationProvider>

     
      <IconButton   sx={{marginLeft:7, marginTop:1.6, ":hover": {
          bgcolor: "#F2F4F4",
          color: "#1661c9"
        }}}  variant="solid" onClick={handleDate}>
                  <RiSearchLine />
                                        
      </IconButton>

      <IconButton  sx={{marginLeft:3, marginTop:1.5 , ":hover": {
          bgcolor: "#F2F4F4",
          color: "#1661c9"
        }}}  variant="solid"  onClick={handleDownloadExcel}>
          <RiFileExcel2Fill />
                                                          
      </IconButton>


      </TableCell>
    
      </Box>
      <Box sx={{display: 'flex' ,justifyContent: 'flex-start'}}>
                {columns.map((column) => (
                    < Box
                    key={column.id}
                    sx={{marginLeft:column.marginL, minWidth:column.minWidth}}
                    
                    >
                      {column.label}
                    </Box>
                  ))}
                            
       </Box>                 


    <TableBody>
    {createData(recargas, 'vehiculo').map(( row, index ) => (
      <CustomRow key={index} row={row} fields={columns} detailFields={detailsColumns} excel={excel} ></CustomRow>
    ))}
  </TableBody>
 

 

      
      
      <Loading status={loading}></Loading>
    </Paper>
  )
}

export {Recargas}