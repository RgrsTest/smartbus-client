import React from "react";
import { Documentos , Catalogos} from "../../Api";
import { Context } from "../../Context";


function useRecargas(fromDate,toDate){
    const { jwt } = React.useContext(Context)
    const [ loading, setLoading ] = React.useState(false)
    const [ recargas, setRecargas ] = React.useState([])
    const [ productos, setProductos ] = React.useState([])
    const [ perfiles, setPerfiles ] = React.useState([])


    const recargasDocumentos = Documentos(jwt)
    const productoCatalogos = Catalogos(jwt)
    const perfilesCatalogos = Catalogos(jwt)

    const payload = {
        schema: 'recargas',
        filter: {

            where:[
                {
                    field:"payload->encabezado->fecha_hora",
                    operator: ">=",
                    value : fromDate
                },

                {
                    field:"payload->encabezado->fecha_hora",
                    operator: "<=",
                    value : toDate
                }

        

            ]

        }
    }


    const payloadProductos = {
        schema: 'productos'
    }

    const payloadPerfiles = {
        schema: 'perfiles'
    }

    async function getRecargas(){
        setLoading(true)
        payload['with-storage'] = ['*']
        const response = await recargasDocumentos.consultar(payload)
        if (response.status === 'success'){
            setRecargas(response.data)
        }
        setLoading(false)
    }

    async function getProductos(){
        setLoading(true)
        payloadProductos['with-storage'] = ['*']
        const response = await productoCatalogos.consultarCatalogo(payloadProductos)
        if (response.status === 'success'){
            setProductos(response.data)
        }
        setLoading(false)
    }

    async function getPerfiles(){
        setLoading(true)
        payloadPerfiles['with-storage'] = ['*']
        const response = await perfilesCatalogos.consultarCatalogo(payloadPerfiles)
        if (response.status === 'success'){
            setPerfiles(response.data)
        }
        setLoading(false)
    }



    React.useEffect(getProductos, [])
    React.useEffect(getPerfiles, [])


    React.useEffect(getRecargas, [fromDate,toDate])

    return {
        loading,
        recargas,
        productos,
        perfiles
    }
}

export { useRecargas }