import React from 'react';
import { Context } from '../../Context';
import { Catalogos } from '../../Api';

function useLapv(){
    const { jwt } = React.useContext(Context);
    const [ lapv, setLapv ] = React.useState([]);
    const [ error, setError ] = React.useState(false);
    const [ loading, setLoading ] = React.useState(false);
    const lapvCatalogos = Catalogos(jwt);
    var payload = {
        schema: 'lapv'
    };

    async function getLapv(){
        setLoading(true);
        try{
            const response = await lapvCatalogos.consultarCatalogo(payload);
            if (response.status === 'success'){
                setLapv(response.data);
                setLoading(false);
            }
        }
        catch {
            setError(true);
        }
        setLoading(false);
    }

    async function updateLapv(data, id=null){
       
        setLoading(true)
        try{
          if (id) payload.id = id
          payload.payload = data
          const response = await lapvCatalogos.actualizarCatalogo(payload)
          if (response.status === 'success'){
            setLoading(false)
            return true
          }
        }
        catch (e){
          setError(true)  
        }
        setLoading(false)
        return false
    }


    async function deleteLapv(id){
        setLoading(true)
        try{
          payload.id = id
          const response = await lapvCatalogos.eliminarCatalogo(payload)
          if (response.status === 'success'){
            setLoading(false)
            return true
          }
        }
        catch (e){
          setError(true)
        }
        setLoading(false)
        return false
      }

    return {
        lapv,
        loading,
        error,
        getLapv,
        updateLapv,
        deleteLapv
    }
}

export {useLapv}