import { Box, Button, Card, CardActions, CardContent, CardHeader, FormGroup, Modal, TextField, FormLabel, FormControl } from "@mui/material";
import React from "react";
import { Loading } from "../components/Loading";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    maxHeight: 500,
    overflow: 'auto'
  };


function Formulario( { status, setStatus, success, setSuccess, loading, action, item=null } ){
    const [ idProducto,  setIdProducto ] = React.useState('')
    const [ productoNombre, setProductoNombre ] = React.useState('')
    const [ claveProducto, setClaveProducto ] = React.useState('')
    const [ recargaSAM, setRecargaSAM ] = React.useState('')
    const [ recargaPICC, setRecargaPICC ] = React.useState('')
    const [ validacionSAM, setValidacionSAM ] = React.useState('')
    const [ validacionPICC, setValidacionPICC ] = React.useState('')
    const [ contratoSAM, setContratoSAM ] = React.useState('')
    const [ contratoPICC, setContratoPICC ] = React.useState('')
    const [ servicioSAM, setServicioSAM ] = React.useState('')
    const [ servicioPICC, setServicioSPICC ] = React.useState('')
    const [ errorIdProducto, setErrorIdProducto ] = React.useState(false)
    const [ errorLegendIdProducto, setErrorLegendIdProducto ] = React.useState('')
    const [ errorProductoNombre, setErrorProductoNombre ] = React.useState(false)
    const [ errorLegendProductoNombre, setErrorLegendProductoNombre ] = React.useState('')
    const [ errorClaveProducto, setErrorClaveProducto ] = React.useState(false)
    const [ errorLegendClaveProducto, setErrorLegendClaveProducto ] = React.useState('')
    const [ errorRecargaSAM, setErrorRecargaSAM ] = React.useState(false)
    const [ errorLegendRecargaSAM, setErrorLegendRecargaSAM ] = React.useState('')
    const [ errorRecargaPICC, setErrorRecargaPICC ] = React.useState(false)
    const [ errorLegendRecargaPICC, setErrorLegendRecargaPICC ] = React.useState('')
    const [ errorValidacionSAM, setErrorValidacionSAM ] = React.useState(false)
    const [ errorLegendValidacionSAM, setErrorLegendValidacionSAM ] = React.useState('')
    const [ errorValidacionPICC, setErrorValidacionPICC ] = React.useState(false)
    const [ errorLegendValidacionPICC, setErrorLegendValidacionPICC ] = React.useState('')
    const [ errorContratoSAM, setErrorContratoSAM ] = React.useState(false)
    const [ errorLegendContratoSAM, setErrorLegendContratoSAM ] = React.useState('')
    const [ errorContratoPICC, setErrorContratoPICC ] = React.useState(false)
    const [ errorLegendContratoPICC, setErrorLegendContratoPICC ] = React.useState('')
    const [ errorServicioSAM, setErrorServicioSAM ] = React.useState(false)
    const [ errorLegendServicioSAM, setErrorLegendServicioSAM] = React.useState('')
    const [ errorServicioPICC, setErrorServicioPICC ] = React.useState(false)
    const [ errorLegendServicioPICC, setErrorLegendServicioPICC ] = React.useState('')
    const [ title, setTitle ] = React.useState('Crear nuevo producto')
    const [ button, setButton ] = React.useState('Agregar')

    const handlePayload = () => {
        if (item){
            setTitle('Editar producto')
            setButton('Guardar')
            setIdProducto(item.payload.id_producto)
            setProductoNombre(item.payload.producto_nombre)
            setClaveProducto(item.payload.clave)
            setRecargaSAM(item.payload.opciones.recarga.llaveSAM)
            setRecargaPICC(item.payload.opciones.recarga.llavePICC)
            setValidacionSAM(item.payload.opciones.validacion.llaveSAM)
            setValidacionPICC(item.payload.opciones.validacion.llavePICC)
            setContratoSAM(item.payload.opciones.contrato.llaveSAM)
            setContratoPICC(item.payload.opciones.contrato.llavePICC)
            setServicioSAM(item.payload.opciones.servicio.llaveSAM)
            setServicioSPICC(item.payload.opciones.servicio.llavePICC)
        }
        else {
            setTitle('Crear nuevo producto')
            setButton('Agregar')
            setIdProducto('')
            setProductoNombre('')
            setClaveProducto('')
            setRecargaSAM('')
            setRecargaPICC('')
            setValidacionSAM('')
            setValidacionPICC('')
            setContratoSAM('')
            setContratoPICC('')
            setServicioSAM('')
            setServicioSPICC('')
        }
    }

    

    React.useEffect(handlePayload, [item])

    const handleProductoNombre = () => {
        if ( errorProductoNombre ) {
            setErrorProductoNombre(false)
            setErrorLegendProductoNombre('')
        }
    }

    const handleClaveProducto = () => {
        if ( claveProducto ) {
            setErrorClaveProducto(false)
            setErrorLegendClaveProducto('')
        }
    }

    const handleIdProducto = (evt) => {
        handleHexadecimal(evt, idProducto)
        if ( errorIdProducto ) {
            setErrorIdProducto(false)
            setErrorLegendIdProducto('')
        }
    }

    const handleRecargaSAM = (evt) => {
        handleHexadecimal(evt, recargaSAM, 2)
        if (errorRecargaSAM){
            setErrorRecargaSAM(false)
            setErrorLegendRecargaSAM('')
        }
    }
    const handleRecargaPICC = (evt) => {
        handleHexadecimal(evt, recargaPICC, 2)
        if (errorRecargaPICC) {
            setErrorRecargaPICC(false)
            setErrorLegendRecargaPICC('')
        }
    }
    const handleValidacionSAM = (evt) => {
        handleHexadecimal(evt, validacionSAM, 2)
        if (errorValidacionSAM) {
            setErrorValidacionSAM(false)
            setErrorLegendValidacionSAM('')
        }
    }
    const handleValidacionPICC = (evt) => {
        handleHexadecimal(evt, validacionPICC, 2)
        if (errorValidacionPICC){
            setErrorValidacionPICC(false)
            setErrorLegendValidacionPICC('')
        }
    }
    const handleContratoSAM = (evt) => {
        handleHexadecimal(evt, contratoSAM, 2)
        if (errorContratoSAM){
            setErrorContratoSAM(false)
            setErrorLegendContratoSAM('')
        }
    }
    const handleContratoPICC = (evt) => {
        handleHexadecimal(evt, contratoPICC, 2)
        if (errorContratoPICC){
            setErrorContratoPICC(false)
            setErrorLegendContratoPICC('')
        }
    }
    const handleServicioSAM = (evt) => {
        handleHexadecimal(evt, servicioSAM, 2)
        if (errorServicioSAM){
            setErrorServicioSAM(false)
            setErrorLegendServicioSAM('')
        }
    }
    const handleServicioPICC = (evt) => {
        handleHexadecimal(evt, servicioPICC, 2)
        if (errorServicioPICC){
            setErrorServicioPICC(false)
            setErrorLegendServicioPICC('')
        }
    }

    const handleHexadecimal = (event, field, size=4 ) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && field.length >= size ){
            event.preventDefault()
        }
    }
    const handleAction = async () => {
        let ok = true
        if (!idProducto) {
            setErrorIdProducto(true)
            setErrorLegendIdProducto('Este campo es obligatorio')
            ok = false
        }
        if (!productoNombre) {
            setErrorProductoNombre(true)
            setErrorLegendProductoNombre('Este campo es obligatorio')
            ok = false
        }
        if (!claveProducto){
            setErrorClaveProducto(true)
            setErrorLegendClaveProducto('Este campo es obligatorio')
            ok = false
        }
        if (!recargaSAM){
            setErrorRecargaSAM(true)
            setErrorLegendRecargaSAM('Este campo es obligatorio')
            ok = false
        }
        if (!recargaPICC){
            setErrorRecargaPICC(true)
            setErrorLegendRecargaPICC('Este campo es obligatorio')
            ok = false
        }
        if (!validacionSAM){
            setErrorValidacionSAM(true)
            setErrorLegendValidacionSAM('Este Campo es obligatorio')
            ok = false
        }
        if (!validacionPICC){
            setErrorValidacionPICC(true)
            setErrorLegendValidacionPICC('Este Campo es obligatorio')
            ok = false
        }
        if (!contratoSAM){
            setErrorContratoSAM(true)
            setErrorLegendContratoSAM('Este Campo es obligatorio')
            ok = false
        }
        if (!contratoPICC){
            setErrorContratoPICC(true)
            setErrorLegendContratoPICC('Este Campo es obligatorio')
            ok = false
        }
        if (!servicioSAM){
            setErrorServicioSAM(true)
            setErrorLegendServicioSAM('Este campo es obligatorio')
            ok = false
        }
        if (!servicioPICC){
            setErrorServicioPICC(true)
            setErrorLegendServicioPICC('Este campo es obligatorio')
            ok = false
        }

        if (ok){
            let send = null
            if (item){
                send = await action({id_producto: idProducto.toLowerCase(), producto_nombre: productoNombre.toUpperCase(), clave: claveProducto.toUpperCase(), opciones: handleDataStructure()}, item.id)
            }
            else{
                send = await action({id_producto: idProducto.toLowerCase(), producto_nombre: productoNombre.toUpperCase(), clave: claveProducto.toUpperCase(), opciones: handleDataStructure()})
            }
            if (send){
                setSuccess(!success)
                handleClose(true)
            }
        }
    }
    const handleClose = (close= false) => {
        if (item === null || close){
            setIdProducto('')
            setProductoNombre('')
            setClaveProducto('')
            setErrorIdProducto(false)
            setErrorProductoNombre(false)
            setErrorClaveProducto(false)
            setErrorLegendIdProducto('')
            setErrorLegendProductoNombre('')
            setErrorLegendClaveProducto('')
            setErrorRecargaSAM(false)
            setErrorLegendRecargaSAM('')
            setErrorRecargaPICC(false)
            setErrorLegendRecargaPICC('')
            setRecargaSAM('')
            setRecargaPICC('')
            setValidacionSAM('')
            setValidacionPICC('')
            setErrorValidacionSAM(false)
            setErrorValidacionPICC(false)
            setErrorLegendValidacionSAM('')
            setErrorLegendValidacionPICC('')
            setContratoSAM('')
            setContratoPICC('')
            setErrorContratoSAM(false)
            setErrorContratoPICC(false)
            setErrorLegendContratoSAM('')
            setErrorLegendContratoPICC('')
            setServicioSAM('')
            setServicioSPICC('')
            setErrorServicioSAM(false)
            setErrorServicioPICC(false)
            setErrorLegendServicioSAM('')
            setErrorLegendServicioPICC('')
        }
        setStatus(false)
    }

    const handleDataStructure = () =>{
        let obj = {
            recarga: {
                llaveSAM: recargaSAM.toUpperCase(),
                llavePICC: recargaPICC.toUpperCase(),
            },
            validacion: {
                llaveSAM: validacionSAM.toUpperCase(),
                llavePICC: validacionPICC.toUpperCase()
            },
            contrato: {
                llaveSAM: contratoSAM.toUpperCase(),
                llavePICC: contratoPICC.toUpperCase()
            },
            servicio: {
                llaveSAM: servicioSAM.toUpperCase(),
                llavePICC: servicioPICC.toUpperCase()
            }
        }
        return obj
    }
    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent sx={{overflow: 'auto'}}>
                        <TextField required error={errorIdProducto} helperText={errorLegendIdProducto} sx={{m: 1, width: 200}} label='Id Producto' value={ idProducto } onKeyPress={(e) => handleIdProducto(e)} onChange={ (e) => setIdProducto(e.target.value) } />
                        <TextField required error={errorClaveProducto} helperText={errorLegendClaveProducto} sx={{m: 1, width: 200}} label='Clave Producto' value={ claveProducto } onKeyPress={() => handleClaveProducto()} onChange={(e) => { setClaveProducto(e.target.value)}} />
                        <TextField required error={errorProductoNombre} helperText={errorLegendProductoNombre} sx={{m: 1, width: 415}} label='Producto Nombre' value={ productoNombre }  onKeyPress={()=> handleProductoNombre()} onChange={ (e) => {setProductoNombre(e.target.value);} } />
                        <FormControl>
                            <FormGroup >
                                <FormLabel sx={{m: 1}} >Recarga</FormLabel>
                                <TextField onKeyPress={(e) => handleRecargaSAM(e)} error={errorRecargaSAM} helperText={errorLegendRecargaSAM} value={recargaSAM} onChange={(e) => setRecargaSAM(e.target.value)} sx={{m: 1, width: 200}} label='Llave SAM' required />
                                <TextField onKeyPress={(e) => handleRecargaPICC(e)} error={errorRecargaPICC} helperText={errorLegendRecargaPICC} value={recargaPICC} onChange={(e) => setRecargaPICC(e.target.value)} sx={{m: 1, width: 200}} label='Llave PICC' required />    
                            </FormGroup>
                        </FormControl>
                        <FormControl>
                            <FormGroup >
                                <FormLabel sx={{m: 1}} >Validacion</FormLabel>
                                <TextField onKeyPress={(e) => handleValidacionSAM(e)} error={errorValidacionSAM} helperText={errorLegendValidacionSAM} value={validacionSAM} onChange={(e) => setValidacionSAM(e.target.value)} sx={{m: 1, width: 200}} label='Llave SAM' required />
                                <TextField onKeyPress={(e) => handleValidacionPICC(e)} error={errorValidacionPICC} helperText={errorLegendValidacionPICC} value={validacionPICC} onChange={(e) => setValidacionPICC(e.target.value)} sx={{m: 1, width: 200}} label='Llave PICC' required />
                            </FormGroup>
                        </FormControl>
                        <FormControl>
                            <FormGroup >
                                <FormLabel sx={{m: 1}} >Contrato</FormLabel>
                                <TextField onKeyPress={(e) => handleContratoSAM(e) } error={errorContratoSAM} helperText={errorLegendContratoSAM} value={contratoSAM} onChange={(e) => setContratoSAM(e.target.value)} sx={{m: 1, width: 200}} label='Llave SAM' required />
                                <TextField onKeyPress={(e) => handleContratoPICC(e) } error={errorContratoPICC} helperText={errorLegendContratoPICC} value={contratoPICC} onChange={(e) => setContratoPICC(e.target.value)} sx={{m: 1, width: 200}} label='Llave PICC' required />
                            </FormGroup>
                        </FormControl>
                        <FormControl>
                            <FormGroup >
                                <FormLabel sx={{m: 1}} >Servicio</FormLabel>
                                <TextField onKeyPress={(e) => handleServicioSAM(e)} error={errorServicioSAM} helperText={errorLegendServicioSAM} value={servicioSAM} onChange={(e) => setServicioSAM(e.target.value)} sx={{m: 1, width: 200}} label='Llave SAM' required />
                                <TextField onKeyPress={(e) => handleServicioPICC(e)} error={errorServicioPICC} helperText={errorLegendServicioPICC} value={servicioPICC} onChange={(e) => setServicioSPICC(e.target.value)} sx={{m: 1, width: 200}} label='Llave PICC' required />
                            </FormGroup>
                        </FormControl>
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleAction()}>
                            {button}
                        </Button>
                        <Button onClick={() => handleClose()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}