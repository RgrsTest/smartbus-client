import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useProductos() {
    const { jwt } = React.useContext(Context)
    const [ productos, setProductos ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const productosCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'productos',
    }
    
    async function getProductos(){
      setLoading(true)
      try{
        const response = await productosCatalogo.consultarCatalogo(payload)
        setProductos(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updateProductos(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await productosCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteProductos(id){
      try{
        payload.id = id
        const response = await productosCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          return true
        }
      }
      catch (e){
        setError(true)
      }
      return false
    }

    React.useEffect(getProductos, [])
    return {
      productos,
      loading,
      error,
      setLoading,
      setError,
      getProductos,
      updateProductos,
      deleteProductos
    }
}

export {useProductos}