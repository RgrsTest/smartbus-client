import React from 'react';
import {Autocomplete, Divider,  Button,TextField, FormGroup,IconButton,Grid,MenuItem,Menu,Box,Tabs, Tab,ListItemIcon,Collapse,List,ListItem ,ListItemButton,ListItemText,Checkbox,TableCell,TableContainer,TableRow,Paper,Table,TableHead,TableBody,Stack,BottomNavigation,Card,CardContent,Typography,CardActions} from '@mui/material';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import RestoreIcon from '@mui/icons-material/Restore';
import FavoriteIcon from '@mui/icons-material/Favorite';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import DeleteIcon from '@mui/icons-material/Delete';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import { deepPurple } from '@mui/material/colors';
import Avatar from '@mui/material/Avatar';
import { green, purple } from '@mui/material/colors';
import { Info } from './Info';
import TimelineIcon from '@mui/icons-material/Timeline';
import CssBaseline from '@mui/material/CssBaseline';
import Switch from '@mui/material/Switch';
import { DatePicker, LocalizationProvider,TimePicker} from "@mui/lab";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import DateAdapter from '@mui/lab/AdapterDateFns'
import ViewModuleIcon from '@mui/icons-material/ViewModule';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import TableRowsRoundedIcon from '@mui/icons-material/TableRowsRounded';

import DirectionsBusIcon from '@mui/icons-material/DirectionsBus';
import Grow from '@mui/material/Grow';
import FormControlLabel from '@mui/material/FormControlLabel';
import Container from '@mui/material/Container';
import {useMemo} from 'react';

import {GoogleMap,useLoadScript,Marker,OverlayView, InfoWindow,Polyline} from "@react-google-maps/api";
import FmdGoodRoundedIcon from '@mui/icons-material/FmdGoodRounded';

import DirectionsBusRoundedIcon from '@mui/icons-material/DirectionsBusRounded';

import Leaflet, { setOptions } from 'leaflet';
import SearchIcon from '@mui/icons-material/Search';
import { GrStatusCriticalSmall } from "react-icons/gr";

import { useVehiculos } from "../Vehiculos/useVehiculos";
import { letterSpacing } from '@mui/system';

import { useRutas } from "../Rutas/useRutas";

import { useTracking } from "../Tracking/useTracking";

import LensIcon from '@mui/icons-material/Lens';
import { MdInsertEmoticon, MdSignalWifi4Bar } from "react-icons/md";
import { MdSignalWifiBad } from "react-icons/md";
import ColorLensIcon from '@mui/icons-material/ColorLens';
import { FiMapPin } from 'react-icons/fi';
import InputColor from 'react-input-color';
import EditIcon from '@mui/icons-material/Edit';
import './ClickableBoxList.css'; // Estilos personalizados
import RoomRoundedIcon from '@mui/icons-material/RoomRounded';
import { FaLocationPin } from "react-icons/fa";

import PlayCircleIcon from '@mui/icons-material/PlayCircle';
import FastForwardIcon from '@mui/icons-material/FastForward';
import FastRewindIcon from '@mui/icons-material/FastRewind';
import PauseCircleIcon from '@mui/icons-material/PauseCircle';




import './VerticalTimeline.css'; // Estilos personalizados
  var mapsApiKey = "AIzaSyB5Ze6Ew9nhetaeNN63Tf6X0HFNl-ueVYE";

  function Home(ruta) {
      const { isLoaded } = useLoadScript({
        googleMapsApiKey:"AIzaSyB5Ze6Ew9nhetaeNN63Tf6X0HFNl-ueVYE",
      });
  
      
    
      if (!isLoaded) return <div>Loading...</div>;
      return <Map />;
    }



    const checkVehiculoConnection = (ultimaConexion, latitud, longitud, path) => {
    
       let status = 0;
      // console.log("path " + path[40].lat + "lng range" +  path[40].lng);
      if(ultimaConexion === undefined)
      {
       // console.log("undefined")
        return status;
      }
      else{
        let lastconnection = new Date(ultimaConexion);
   //   console.log("ultimaConexion")
      lastconnection.setMinutes(lastconnection.getMinutes() - 55);

    //  console.log(lastconnection)
    //  console.log("date");
      let dateNow = new Date();
   //   console.log(dateNow)

   
      if(lastconnection > dateNow)
      {
        if(path != null){

          status = 2;
        
        path.map((item) => {

          let latitudRange = latitud - item.lat;
          let longitudRange = longitud - item.lng;

         

        if(Math.abs(latitudRange) < 0.0020 && Math.abs(longitudRange) < 0.0020)
         {
          status = 1;
       
         }
      
          
        }) }




      
      }
      else
      {
       

        if(path != null){

          status = 3;
        
        path.map((item) => {

          let latitudRange = latitud - item.lat;
          let longitudRange = longitud - item.lng;

         // console.log("lat range" +latitudRange + "lng range" + longitudRange)
        
        if(Math.abs(latitudRange) < 0.0020 && Math.abs(longitudRange) < 0.0020)
         {
          status = 0;
            //    console.log("dentro del rango");
            return status;
         }
        
      
          
        }) }






       
      }

      return status;
      }
  
     

    }



    const CustomVehiculoMarker = ({ item, rutasInfo}) => {

      const [isHovered, setIsHovered] = React.useState(false);
      const [isOpen, setIsOpen] = React.useState(false);

      const [fontSize, setFontSize] = React.useState(8);
      

      let path = null;

      rutasInfo.map((itemRuta) => {

    //    console.log("rutaLabel" + item.rutaLabel);
       // console.log("label" + itemRuta.payload.label);

        if(item.rutaLabel  == itemRuta.payload.label)
          {
            path =  itemRuta.path;
            
          }
        

      })


      let connectionStatus =   checkVehiculoConnection(item.fecha_hora, item.lat, item.lng, path);

   

      const boxStyle = {
        position: 'absolute',
        maxWidth: '500px',
        maxHeight: '500px',
        height: '95px',
        top: '-105px', // Ajusta la posición vertical del cuadro
        left: '50%', // Centra horizontalmente el cuadro
        transform: 'translateX(-50%)', // Centra horizontalmente el cuadro
        background: 'white', // Color del cuadro
        padding: '4px 8px',
        borderRadius: '10px',
        boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.5)',
        whiteSpace: 'nowrap',
        display: (isHovered ) ? 'block' : 'none', // Mostrar el cuadro solo cuando está abierto
        zIndex: 7, // Coloca el cuadro sobre el overlay
      };


      const dot = {
       
        width: '5px',
        height: '5px',
       
        borderRadius: '50%',
      
        backgroundColor:  connectionStatus == 2 ? 'red' : connectionStatus == 1 ? 'green':'orange'
      };

      const textBoxStyle = {
       
        padding: '1px',
        height:'9px',
        marginLeft:'0px',
        maxWidth: '500px',
        fontSize: `${fontSize}px`,
        textAlign: 'left',
      };

      const titleBoxStyle = {
       
        padding: '1px',
        height:'12px',
        marginLeft:'0px',
        maxWidth: '500px',
        fontSize: `10px`,
        textAlign: 'left',
      };


    
      return (
        <OverlayView
        position={{lat: item.payload == null ?  item.lat :   item.payload.location.latitud, 
          lng: item.payload == null ? item.lng :  item.payload.location.longitud}}
        mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
        getPixelPositionOffset={(width, height) => ({
         x: -(40 / 2),
         y:   -40, // Ajusta para centrar verticalmente el punto
       })}
      >
       <div style={{
       width: '40px',
       height: '40px',
       borderRadius: '50%',
       transform: 'translateX(50%)',
       boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
       transition: 'transform 0.3s, box-shadow 0.3s',
       transform: isHovered ? 'scale(1.8)' : 'scale(1)',
       transformOrigin: 'center bottom', 
       display: 'flex',
       zIndex: 8,
       alignItems: 'center',
       justifyContent: 'center',
       display:  item.isVisible || item.isSelected ? 'block' : 'none',
       }}
       onMouseEnter={() => {setIsHovered(true); }}
       onMouseLeave={() => {setIsHovered(false); setIsOpen(false);  }}
       onClick={() => setIsOpen(!isOpen)}>
 
    <img src={ connectionStatus == 2 ? "/assets/bus3-red.png" : connectionStatus == 1 ? "/assets/bus3-green.png": "/assets/bus3-orange.png" } alt="Icono" style={{ width: '75%', height: '100%'  }}/>
    <div style={boxStyle}>
    <div style={titleBoxStyle}>    
       <strong>{item.rutaLabel}</strong> <br />
       <strong style={{...textBoxStyle,fontSize:'8.5px'}}>{item.label}</strong><br />
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}>
        <div style={dot}></div><div style={{...textBoxStyle,padding:'3px'}}>{ connectionStatus == 2 ? 'Fuera de ruta' : connectionStatus == 1 ? 'Conectado': 'Desconectado' }</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>{item.fecha_hora}</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>Velocidad: {item.velocidad} km/hr</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>Distancia: {item.recorrido} km</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>subidas:{item.subidas}</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>bajadas:{item.bajadas}</div></div>

     </div>
     
 </div>
       
  </div>
  </OverlayView>
      );

    };


    const CustomVehiculoMarkerTracking = ({ item, rutasInfo}) => {

      const [isHovered, setIsHovered] = React.useState(false);
      const [isOpen, setIsOpen] = React.useState(false);

      const [fontSize, setFontSize] = React.useState(8);
      

      let path = null;

      rutasInfo.map((itemRuta) => {

       

        if(item.rutaLabel  == itemRuta.payload.label)
          {
          //  console.log("rutaLabel" + item.rutaLabel);
          //  console.log("label" + itemRuta.payload.label);
            path =  itemRuta.path;

          //  console.log("path" + path[0].lat +  path[0].lng);
            
          }
        

      })


      let connectionStatus =  checkVehiculoConnection(item.fecha_hora, item.lat, item.lng, path);

   

      const boxStyle = {
        position: 'absolute',
        maxWidth: '300px',
        maxHeight: '500px',
        width:'100px',
        height: '95px',
        top: '-105px', // Ajusta la posición vertical del cuadro
        left: '50%', // Centra horizontalmente el cuadro
        transform: 'translateX(-50%)', // Centra horizontalmente el cuadro
        background: 'white', // Color del cuadro
        padding: '4px 8px',
        borderRadius: '10px',
        boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.5)',
        whiteSpace: 'nowrap',
        display: (isHovered ) ? 'block' : 'none', // Mostrar el cuadro solo cuando está abierto
        zIndex: 3, // Coloca el cuadro sobre el overlay
      };

      const dot = {
       
        width: '5px',
        height: '5px',
       
        borderRadius: '50%',
      
        backgroundColor:   connectionStatus == 0 ? 'green':'red'
      };
      

      const textBoxStyle = {
       
        padding: '1px',
        height:'9px',
        marginLeft:'0px',
        maxWidth: '300px',
        fontSize: `${fontSize}px`,
        textAlign: 'left',
      };

      const titleBoxStyle = {
       
        padding: '1px',
        height:'12px',
        marginLeft:'0px',
        maxWidth: '300px',
        fontSize: `10px`,
        textAlign: 'left',
      };


    
      return (
        <OverlayView
        position={{lat: item.payload == null ?  item.lat :   item.payload.location.latitud, 
          lng: item.payload == null ? item.lng :  item.payload.location.longitud}}
        mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
        getPixelPositionOffset={(width, height) => ({
         x: -(40 / 2),
         y:   -40, // Ajusta para centrar verticalmente el punto
       })}
      >
       <div style={{
       width: '40px',
       height: '40px',
       borderRadius: '50%',
       transform: 'translateX(50%)',
       boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
       transition: 'transform 0.3s, box-shadow 0.3s',
       transform: isHovered ? 'scale(1.8)' : 'scale(1)',
       transformOrigin: 'center bottom', 
       display: 'flex',
       zIndex: 2,
       alignItems: 'center',
       justifyContent: 'center',
       display:  item.isVisible || item.isSelected ? 'block' : 'none',
       }}
       onMouseEnter={() => {setIsHovered(true); }}
       onMouseLeave={() => {setIsHovered(false); setIsOpen(false);  }}
       onClick={() => setIsOpen(!isOpen)}>
 
    <img src={"/assets/bus3.png"} alt="Icono" style={{ width: '75%', height: '100%'  }}/>
    <div style={boxStyle}>
    <div style={titleBoxStyle}>    
       <strong>{item.rutaLabel}</strong> <br />
       <strong style={{...textBoxStyle,fontSize:'8.5px'}}>{item.label}</strong><br />
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}>
       <div style={dot}></div><div style={{...textBoxStyle,padding:'3px'}}>{ connectionStatus == 0 ? 'En ruta':'Fuera de ruta'}</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>{item.fecha_hora}</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>Velocidad: {item.velocidad} km/hr</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>Distancia: {item.recorrido} km</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>subidas:{item.subidas}</div></div>
       <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '0px' }}><div style={textBoxStyle}>bajadas:{item.bajadas}</div></div>
     </div>
     
 </div>
       
  </div>
  </OverlayView>
      );

    };




    const CustomOverlay = ({ item,strokeColor, estacion, isVisible}) => {
      const [isHovered, setIsHovered] = React.useState(false);
      const [isOpen, setIsOpen] = React.useState(false);
   

      const boxStyle = {
        position: 'absolute',
        maxWidth: '150px',
        maxHeight: '30px',
        top: '-30px', // Ajusta la posición vertical del cuadro
        left: '50%', // Centra horizontalmente el cuadro
        transform: 'translateX(-50%)', // Centra horizontalmente el cuadro
        background: 'white', // Color del cuadro
        padding: '4px 8px',
        borderRadius: '0px',
        boxShadow: '0px 2px 4px rgba(0, 0, 0, 0)',
        whiteSpace: 'nowrap',
        display: (isHovered ) ? 'block' : 'none', // Mostrar el cuadro solo cuando está abierto
        zIndex: 1, // Coloca el cuadro sobre el overlay
      };

      const pinStyle = {
        position: 'absolute',
        bottom: '22px', // Ajusta la posición vertical de la punta
        left: '50%', // Centra horizontalmente la punta
        transform: 'translateX(-50%)', // Centra horizontalmente la punta
        width: 0,
        height: 0,
        borderLeft: '17px solid transparent', // Ancho de la base de la punta
        borderRight: '17px solid transparent', // Ancho de la base de la punta
        borderTop: '10px solid white', // Altura de la punta y color
        zIndex: 1, // Coloca la punta sobre el cuadro y el overlay
        display: ( isHovered ) ? 'block' : 'none',
      };
    
    
      return (
        <OverlayView
                 position={{lat:item.latitud,lng:item.longitud}}
                 mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                 getPixelPositionOffset={(width, height) => ({
                  x: -(20 / 2),
                  y: -10, // Ajusta para centrar verticalmente el punto
                })}
               >
                <div style={{
                width: '20px',
                height: '20px',
                backgroundColor: strokeColor,
                borderRadius: '50%',
                transform: 'translateX(50%)', // Centra horizontalmente el cuadro
                boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.5)',
                transition: 'transform 0.3s, box-shadow 0.3s',
                transform: isHovered ? 'scale(1.5)' : 'scale(1)', // Cambia el tamaño al hacer hover
                boxShadow: isHovered ? '0px 0px 15px rgba(0, 0, 0, 0.7)' : '0px 0px 10px rgba(0, 0, 0, 0.5)', // Cambia la sombra al hacer hover
                zIndex: 1,
                display: isVisible ?'block' : 'none',
                }}
                onMouseEnter={() => setIsHovered(true)}
                onMouseLeave={() => {setIsHovered(false); setIsOpen(false)}}
                onClick={() => setIsOpen(!isOpen)}>
          <div style={boxStyle}>{estacion}</div>
          <div style={pinStyle}></div>
           </div>
         </OverlayView>
      );
    };
    
    function Map() {
  
      const [selectedCenter, setSelectedCenter] = React.useState([]);
      const { vehiculos, loading, getVehiculos, updateVehiculos } = useVehiculos();
      const [ change, setChange ] = React.useState(false);
     
  
      const uluru = { lat: -25.363, lng: 131.044 };

      const { rutas, getRutas } = useRutas()
      const [ ruta, setRuta ] = React.useState(null)
      const [ rutaLabel, setRutaLabel ] = React.useState(null)


      const [ pathEstaciones, setPathEstaciones] = React.useState([])
      const [rutasInfo, setRutasInfo] = React.useState([])

      const [value, setValue] = React.useState(0);

      const { loading2, tracking, getTracking } = useTracking()
     
      const [ vehiculo, setVehiculo ] = React.useState(null)
      const [ dateSearch, setDateSearch ] = React.useState('')
      const [ searchDisabled, setSearchDisabled ] = React.useState(true)
      const [ path, setPath ] = React.useState([])
      const [ marks, setMarks ] = React.useState([])
      const [ markerPos, setMarkerPos ] = React.useState([0,0])
      const [ points, setPoints ] = React.useState([])
    
      const [ fromDate, setFromDate] = React.useState(new Date())
      const [ toDate, setToDate] = React.useState(new Date())
    
      const [ startDate, setStartDate] = React.useState(new Date())
      const [ finalDate, setFinalDate] = React.useState(new Date())
    
    
      const [startTime, setStartTime] = React.useState(dayjs().set('hour', 0).set('minute', 0).set('second', 0))
      const [finalTime, setFinalTime] = React.useState(dayjs().set('hour', 23).set('minute', 59).set('second', 59))

      const [path2,setPath2] = React.useState([])

      const [openUnidades, setOpenUnidades] = React.useState(false);
      const [checkedUnidades, setCheckedUnidades] = React.useState([0]);
      const [menuHeight, setMenuHeight] = React.useState(230);


      const [polylineOptions,setPolylineOptions] = React.useState([]);


      const [rutasPaths, setRutasPaths] = React.useState({});
      const [rutasOptions,setRutasOptions] = React.useState({});

      const [colorRuta, setColorRuta] = React.useState({});
      
      const [anchorEl, setAnchorEl] = React.useState(null);

      const [currentColor, setCurrentColor] = React.useState('#0000FF');

      const [currentRutaChecked, setCurrentRutaChecked] = React.useState(false);

      const [isHovered, setIsHovered] = React.useState(false);

      const [showVehiculoInfo,setShowVehiculoInfo] = React.useState(false);

      const [currentVehiculo,setCurrentVehiculo] = React.useState(null);


      const [currentRutaLabel,setCurrentRutaLabel] = React.useState(null);

      const [vehiculoFixed,setVehiculoFixed] = React.useState(false);

      const [time, setTime] = React.useState(0);

      const [showComponent, setShowComponent] = React.useState(false);



      const [timePlay, setTimePlay] = React.useState(0);
      const [play, setPlay] = React.useState(false);
      const [showUnit, setShowUnit] = React.useState(false);
      const [unitPos, setUnitPos] = React.useState(0);

      const [speedTime, setSpeedTime] = React.useState(1000);

      const [centerLatitud, setCenterLatitud] = React.useState( 20.6763989);
      const [centerLongitud, setCenterLongitud] = React.useState(-103.3479102);
      const [zoomUnit, setZoomUnit] = React.useState(12);
      const [timeZoom, setTimeZoom] = React.useState(4000);

      const [zoomIn, setZoomIn] = React.useState(false);
      const [zoomOut, setZoomOut] = React.useState(false);

      const [unidadActual, setUnidadActual] = React.useState(null);
      //const [isCheckedUnit, setIsCheckedUnit] = useState(false);
      const center = useMemo(() => ({ lat: centerLatitud, lng: centerLongitud}), [centerLatitud, centerLongitud]);
      const zoom = useMemo(() => (zoomUnit), [zoomUnit]);

      const [rutaOptions, setRutaOptions] = React.useState( {
        strokeColor:currentColor,
        strokeOpacity: 0.6,
        strokeWeight: 6,
        fillColor: '#0000FF',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: false,
        radius: 30000
      });

      
      const colors = [
        '#FF0000', '#00FF00', '#0000FF',
        '#FFFF00', '#FF00FF', '#00FFFF',
 
      ];

      
      const boxStyle = {
      
       
       
   
      };
    


      React.useEffect(() => {

    

        const updateRutas = [...rutasInfo];

 
        const rutaIndex = updateRutas.findIndex(item => item.payload.label === rutaLabel);
    
     
        if (rutaIndex !== -1) {
     
          const updatedRuta = { ...updateRutas[rutaIndex] };
    
        
          updatedRuta.payload.options.visible = currentRutaChecked;

          updatedRuta.payload.options.strokeColor = currentColor;
    
       
          updateRutas[rutaIndex] = updatedRuta;
    
     
         setRutasInfo(updateRutas);

        }


     
  
       
      },[currentColor,currentRutaChecked])


      const handleColorSelect = (color) => {
        if(color.hex !== undefined){
          setCurrentColor(color.hex);

        }
       
      };


      
      const enablePintarRuta = (event) => {

        setCurrentRutaChecked(event.target.checked);     
      };





   



  
      React.useEffect(getVehiculos, [change,showComponent])

      React.useEffect(() => {
        if (dateSearch && vehiculo){
          setSearchDisabled(false)
        }
        else {
          setSearchDisabled(true)
        }
      },[vehiculo, dateSearch])


      React.useEffect(getRutas, [])


      React.useEffect(() => {
        const timer = setTimeout(() => {
           setShowComponent(!showComponent);
           
           if(showComponent){setTime(10000)}
           else{setTime(0)}
       }, time);
       }, [showComponent]);


   
   

      const options = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        fillColor: '#0000FF',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: false,
        radius: 30000
       
        
      
      };

      const options2 = {
        strokeColor:currentColor,
        strokeOpacity: 0.6,
        strokeWeight: 6,
        fillColor: '#0000FF',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        radius: 30000

      
      };

      const options3 = {
        strokeColor:'red',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        fillColor: '#00FFFF',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        radius: 30000
       
        
      
      };
  
  /*
      vehiculos.map((item,index) => {(

        console.log(item['payload']['label'] + " Ruta: " + item['payload']['id_ruta'])
        
      )}
  
  
      )*/
      
     
  
      var pos = { lat: 20.6763989, lng: -103.3479102 };
  
      const divStyle = {
        background: `white`,
        padding: 0
      }

  
      const handleRuta = (newValue) => {
  
          let rutaLocal = null;
        rutas.forEach((item) => {

          if(item.payload.label === newValue){

            setRutaLabel(item.payload.label);
        
            setCurrentRutaChecked(item.payload.options.visible);

            setCurrentColor(item.payload.options.strokeColor);
          
            setRuta(item);

          //  console.log("ruta item" + item.payload.options.strokeColor);
          rutaLocal = item;

          }})

        
          let height = 0;
          vehiculos.map((value) => {
            console.log("ruta" + rutaLocal);
                                   
            if(value['payload']['id_ruta'] == rutaLocal.payload.external_id){  
             
              height += 45;  
            
            }})

            setMenuHeight(230 + height);
            

      }

   

        
      

      

      const defaultMapOptions = {
        fullscreenControl: false,
        mapTypeControl: false,
      };

      const [trackingPanel, setTrackingPanel] = React.useState(false);
      const [menuPanel, setMenuPanel] = React.useState(false);

        



        const handleToggle = (value) => () => {
          const currentIndex = checkedUnidades.indexOf(value);
          const newChecked = [...checkedUnidades];
      
          if (currentIndex === -1) {
            newChecked.push(value);
          } else {
            newChecked.splice(currentIndex, 1);
          }
      
          setCheckedUnidades(newChecked);
        };
       
      

 const PintadorRutasMarkers = React.useCallback((mode) => {
       

        return( <div>
          {rutasInfo.map((subarray, index) => (
            <div key={index}>
              {subarray.payload.estaciones.map((item, subIndex) => (
               <CustomOverlay item={item} strokeColor={subarray.payload.options.strokeColor} estacion={item.estacion}  isVisible={subarray.payload.options.visible}/>
              ))}
            </div>
          ))}
        </div> )

        
     
      }, [rutasInfo]);

  

      const PintadorRutas = React.useCallback((mode) => {
       

        return(
        <div>

        {rutasInfo.map((item) => (
       
          <Polyline
        
            path={item["path"]}
            options={item["payload"]["options"]}
          />

          
        ))}

     
           
            </div>
        )

        
     
      }, [rutasInfo]);

      
     
      React.useEffect(() => {

    

        rutas.forEach((item) => {

          let path = [];
                item.payload.recorrido.forEach((location) => {
        
                  path.push({lat: location.latitud, lng: location.longitud})
                
                 })

        item["path"] = path;    

       // console.log("ruta opt:" + item.payload.options)

    
  
        })

        setRutasInfo(rutas);
  
       
      },[rutas])


      const [selectedTab, setSelectedTab] = React.useState(0);
      const [selectedBox, setSelectedBox] = React.useState(null);

      
        const [selectedItem, setSelectedItem] = React.useState(null);
      
        const handleItemClick = (item) => {

        setVehiculoFixed(!vehiculoFixed);


             
        const updatedItems = path2.map(pitem => {
          if (pitem.fecha_hora === item.fecha_hora) {
            
            return { ...pitem, isSelected: !pitem.isSelected}; 
          }
          return { ...pitem, isSelected: false}; ;
        });
       // console.log(updatedItems);

       setPath2(updatedItems);
          
        };
      
      const handleTabChange = (event, newValue) => {

      //  console.log("handleTabChange"+ newValue);
        setSelectedTab(newValue);
      };


      const DisableMarker = (item) => {
       /*
if(!vehiculoFixed){
       
        const updatedItems = path2.map(pitem => {
          if (pitem.fecha_hora === item.fecha_hora) {
            return { ...pitem, isVisible: false }; 
          }
          return pitem;
        });


       setPath2(updatedItems);
}*/
       
      };

 

      const EnableMarker = (item) => {

      /*  if(!vehiculoFixed ){

        const updatedItems = path2.map(pitem => {
          if (pitem.fecha_hora === item.fecha_hora) {
            return { ...pitem, isVisible: true }; 
          }
          return pitem;
        });


      setPath2(updatedItems);}*/

       };

       const handleCheckboxChange = (event,item) => {
        console.log("item" + item.payload);
        console.log("event" + event.target.checked);
      };
      


      const PanelMode = React.useCallback((mode) => {

        if(mode.trackingPanel)
            {
              return (
                <Box sx={{marginTop:0, display: 'flex'}}>
                  <Grow
                    
                    in={mode.trackingPanel}
                    style={{ transformOrigin: '0 150 500' }}
                    {...(mode.trackingPanel ? { timeout: 0 } : {})}
                    sx={{ marginTop:1,marginLeft:2, zIndex:'modal'}}
                  >
                    <Paper sx={{ m: 0 ,background:'white', width: 150, height: 600}} elevation={4}>
                  
                    <Tabs
                        value={selectedTab}
                        onChange={handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered
                      >
                        <Tab label="Busqueda" />
                        <Tab label="Historial" />
            
                      </Tabs>
                      {selectedTab === 0 && <div>
                        <Box sx={{width: 270, height: 400} }> 

                            <Autocomplete 


                            sx={{marginLeft: 3, width: 220,marginTop: 2,marginBottom: 2}}

                            options={rutas.map((item) => item.payload.label)}
                            value={rutaLabel}
                            onChange={(e, newValue) => handleRuta(newValue)}
                            renderInput={(params) => <TextField {...params} label='Ruta' variant="standard" /> }

                            />


                            <Autocomplete 
                              sx={{marginLeft: 3, width: 220,marginTop: 2,marginBottom: 2}}
                              options={vehiculos.map((item,index) => { console.log("item vehiculo" + item); return item.payload.label})}
                              value={vehiculo}
                              
                              onChange={(e, newValue) => setVehiculo(newValue)}
                              renderInput={(params) => <TextField {...params} variant="standard" label='Vehiculo' /> }
                              />


                            <LocalizationProvider dateAdapter={DateAdapter}>
                                  <DatePicker label='Seleccione Fecha'  value={dateSearch} 
                                  inputFormat="dd/MM/yyyy"
                                  onChange={(newValue) => setDateSearch(newValue)}
                                  renderInput={(params) => <TextField variant="standard" 
                                  sx={{width: 220,marginLeft: 0,marginTop:1}} 
                                  error={false} {...params} />} />

                            </LocalizationProvider>




                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <TimePicker
                              label="Hora Inicial"
                              value={startTime}
                              onChange={(newValue) => {
                                setStartTime(newValue);
                              }}
                              renderInput={(params) => <TextField variant="standard" sx={{width: 220,marginLeft: 0,marginTop: 2}} {...params} />}
                            />
                            </LocalizationProvider>

                          <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <TimePicker
                            label="Hora Final"
                            value={finalTime}
                            onChange={(newValue) => {
                              setFinalTime(newValue);
                            }}
                            renderInput={(params) => <TextField variant="standard" sx={{width: 220,marginLeft: 0,marginTop: 2}} {...params} />}
                          />
                          </LocalizationProvider>

                            <IconButton onClick={() => handleClickAction()} sx={{marginLeft: 1,marginTop: 2, ":hover": {
                                    bgcolor: "#F2F4F4",
                                    color: "black"
                                  }}}  size='large' >
                            <SearchIcon /> 
                            </IconButton>

                                  </Box>
                        </div>}

                    {selectedTab === 1 &&    <div className="item-list">

                     

                    <Box sx={{ marginLeft:'o'}}> 

                    <IconButton onClick={() => handleFastRew()} sx={{ marginLeft:'0px', ":hover": {
                                   
                                    color: "black"
                                  }}}  >
                    <FastRewindIcon sx={{ height:'35px',width:'35px'}} /> 
                    </IconButton>

                    <IconButton onClick={() => handlePause()} sx={{ marginLeft:'0px', ":hover": {
                                  
                                    color: "black"
                                  }}}  >
                    <PauseCircleIcon sx= {{ marginLeft:'7px',height:'35px',width:'35px'}}/> 
                    </IconButton>

                    <IconButton onClick={() => handlePlay()} sx={{ marginLeft:'0px', 
                                 ":hover": {
                                  
                                  color: "black"
                                }
                                  
                                  }}  >
                    <PlayCircleIcon sx= {{marginLeft:'7px',height:'35px',width:'35px'}}/> 
                    </IconButton>

                    <IconButton onClick={() => handleFastForward()} sx={{ marginLeft:'0px', ":hover": {
                                   
                                    color: "black"
                                  }}}  >
                    <FastForwardIcon sx= {{ marginLeft:'7px',height:'35px',width:'35px'}}/> 
                    </IconButton>


                    </Box>

                    <Box sx={{ marginLeft:'o'}}> 
                    {speedTime/1000} segundos/punto
                    </Box>


                    <List 
                      sx={{
                        width: '100%',
                        maxWidth: 360,
                        bgcolor: 'background.paper',
                        position: 'relative',
                        overflow: 'auto',
                        maxHeight: 400,
                        '& ul': { padding: 0 },
                      }}>
                    {path2.map((item) => {

                      return (
                        
                        <ListItem
                          button
                          selected={item.isSelected}
                          onClick={() => {handleItemClick(item)}}
                          onMouseEnter={() => {EnableMarker(item)}}
                          onMouseLeave={() => {DisableMarker(item)}}
                       
                         
                        >
                          <ListItemText primary={item.fecha_hora} />
                        </ListItem>

                      );


                            })}
                             </List>
                  
                  </div> }
                      
                
              </Paper>
              </Grow>
              </Box> 
              )
             
            }
            else if(mode.menuPanel)
            {
              return(
                <Box sx={{marginTop:0, display: 'flex'}}>
                <Grow
                  
                  in={mode.menuPanel}
                  style={{ transformOrigin: '0 150 500' }}
                  {...(mode.menuPanel ? { timeout: 0 } : {})}
                  sx={{ marginTop:1,marginLeft:2, zIndex:'modal'}}
                >
                  <Paper sx={{ m: 0 ,background:'white', width: 150, height: 500}} elevation={4}>
                
                  <Box sx={{width: 280, height: menuHeight} }> 

                  <Autocomplete 
                    
                    
                    sx={{marginLeft: 3, width: 220,marginTop: 2,marginBottom: 2}}
                
                    options={rutas.map((item) => item.payload.label)}
                    value={rutaLabel}
                    onChange={(e, newValue) => handleRuta(newValue)}
                    renderInput={(params) => <TextField {...params} label='Ruta' variant="standard" /> }

                  />

                  <  Box
                        sx={{
                          display: 'flex',
                          gap: 2
                        }}
                      >   
                    
                
                         <Checkbox  sx={{
                          display: 'flex',
                          mt: -1,
                          ml:1,
                          mr:-2
                        }}
                        checked={currentRutaChecked}
                        onChange={enablePintarRuta}   /> 
                         Pintar Ruta


                    <InputColor
                 
                          
                      initialValue={currentColor}
                      onChange={handleColorSelect}
                      placement="left"
                     />
                 
                  
                  </Box>                     

                <ListItemButton key="list1" onClick={handleClickUnidades}>
                    <ListItemIcon key="list2" >
                      <DirectionsBusIcon />
                    </ListItemIcon>
                  <ListItemText key="list3"  primary="Unidades" />
                  {openUnidades ? <ExpandLess /> : <ExpandMore /> }
                  
                </ListItemButton>


                <Collapse in={openUnidades}  timeout="auto" unmountOnExit>




                  <List component="div" disablePadding>
                  <ListItem
                          key={value}
                       
                          disablePadding
                        >
                          <ListItemButton role={undefined} onClick={handleToggle(value)} dense>
                            <ListItemIcon>
                              <Checkbox
                                edge="start"
                                checked={checkedUnidades.indexOf(value) !== -1}
                                tabIndex={-1}
                                disableRipple
                                sx={{marginLeft: 1}}
                              />
                            </ListItemIcon>
                            <ListItemText  primary={'Todos'} />
                          </ListItemButton>
                        </ListItem>
                                {vehiculos.map((value) => {
                                
                                 // console.log(ruta)
                                 // console.log(value.payload.id_ruta)
                      if(ruta != null){




                      if(value.payload.id_ruta == ruta.payload.external_id){
                      const labelId = `checkbox-list-label-${value.payload.label}`;
                     
                      return (
                        <ListItem
                          key={value}
                         
                          disablePadding
                        > 
                          <ListItemButton  onClick={() => {handleClickUnidadList(value)} }>
                            <ListItemIcon>
                              <Checkbox
                                edge="start"
                                checked={1}
                                tabIndex={-1}
                                disableRipple
                                onChange={(e, newValue) => handleCheckboxChange(e,value)}                               
                                inputProps={{ 'aria-labelledby': labelId }}
                                sx={{marginLeft: 1}}
                              />
                            </ListItemIcon>
                            <ListItemText sx={{marginLeft:0}} id={labelId} primary={value.payload.label} />
                            
                      
                          </ListItemButton>
                        </ListItem>
                      );}}
                    })}
                  </List>
                </Collapse>
                    
                  
                  </Box>
                  
                </Paper>
                </Grow>
             
              </Box>



              )
            }
            else 
            {
              return (<Box></Box>)
            }
      }, [trackingPanel,menuPanel,openUnidades,ruta,dateSearch,startTime,finalTime,rutasInfo,menuHeight,selectedTab,speedTime]);

   

        const handleClickUnidades = () => {
                
        
          setOpenUnidades((prev) => !prev);
       
          

        };

     

      const 
      handleChange = () => {
        setTrackingPanel((prev) => !prev);
        setMenuPanel(false);
    
      };

      const handlemenuPanel = () => {
       setMenuPanel((prev) => !prev);
       setTrackingPanel(false);
       
          

        }


      

      const handleDate = () => {
        setFromDate(startDate);
        setToDate(finalDate);
      }

      const handleClickAction = () => {
        let vehiculoId = vehiculos.find((element) => element.payload.label === vehiculo)
       // console.log("vehiculo id")
        
  
        getTracking(vehiculoId.id, dateSearch.toISOString().slice(0,10))
       
      }

     
      const disminuirZoomProgresivo = (newValue) => {
       
    
        if (zoomUnit > 12) { // Limita el zoom mínimo a 1
          setZoomUnit(zoomUnit - 1);
          setTimeout(disminuirZoomProgresivo, 1000); // Disminuye el zoom cada segundo (ajusta según tus necesidades)

        
        }
        else{
          aumentarZoomProgresivo(newValue);
        }

      };

      const aumentarZoomProgresivo = (newValue) => {

       
      
    
        if (zoomUnit < 17) { // Limita el zoom máximo a 20
          setZoomUnit(zoomUnit + 1);
          console.log("zoom" + zoomUnit)
          setTimeout(aumentarZoomProgresivo, 1000); // Aumenta el zoom cada segundo (ajusta según tus necesidades)
        }
      };



      const handleClickUnidadList = (newValue) => {
       
        setUnidadActual(newValue);

        if(newValue != null){

          if(zoomUnit > 12)
          {
          setZoomOut(true);
            }
          else 
          {
           setZoomOut(false);
            setZoomIn(true);
          }


         


        
        
       
        }
       }



       React.useEffect(() => {

      
        const timer = setTimeout(() => {
        
       



           if(zoomUnit > 12 && zoomOut){
            setZoomUnit(zoomUnit - 1);
          
          
          } 
          else if (zoomUnit ==  12 && zoomOut)
          {
            
            setZoomIn(true);
            setZoomOut(false);
           
            setCenterLatitud(unidadActual.payload.location.latitud)
            setCenterLongitud(unidadActual.payload.location.longitud)
          
          } 

          else if (zoomUnit ==  12 && zoomIn)
          {
            
            setZoomUnit(zoomUnit + 1);
            setCenterLatitud(unidadActual.payload.location.latitud)
            setCenterLongitud(unidadActual.payload.location.longitud)
          
          } 
          
          else if(zoomUnit < 17 && zoomIn){
            
            setZoomUnit(zoomUnit + 1);
          
          
          }

          else if (zoomUnit == 17){
            
          
            setZoomOut(false);
            setZoomIn(false);

           
          } 
          
                     
        
    }, 150);
                             
     
  
   
   
  }, [zoomUnit,zoomIn,zoomOut]);


  

     /*  const handleClickUnidadList = React.useCallback((newValue) => {
       
        if(newValue != null){


           


            setCenterLatitud(newValue.payload.location.latitud)
            setCenterLongitud(newValue.payload.location.longitud)
          
            if (zoomUnit < 17) {
           const timer = setTimeout(() => {
              
            setZoomUnit(zoomUnit + 1);
            console.log("zoom" + zoomUnit)
              
          }, 1000);
        }
          

        }
    
       }, [zoom]);*/


      const handleFastRew = () => {
        setSpeedTime(speedTime*2);
       
      }

      const handleFastForward = () => {
        if(speedTime > 100){
      setSpeedTime(speedTime/2);

       }
      }

      const handlePause= () => {
        //setTimePlay(6000000);
      //  console.log("pausa")
        setPlay(false);
      }




      const handlePlay= () => {


      
       setPlay(!play);
      }


     


      React.useEffect(() => {

      
              const timer = setTimeout(() => {
              
             

                if(play){handleItemClick(path2[unitPos]); setTimePlay(speedTime); setUnitPos(unitPos + 1);} 
                else{
                  setTimePlay(0);
                }               
              
          }, timePlay);
                                   
           
        
         
         
        }, [play,unitPos]);






      const handleShowLabel = (value, index) => {
        if(tracking[0].payload.recorrido.length > 0)
          {
            return tracking[0].payload.recorrido[value].hora
          }
      }




      React.useEffect(() => {
        if (tracking.length > 0){
          if(tracking[0].payload.recorrido.length > 0)
          {
          
            let localPath2 = [];

            tracking[0].payload.recorrido.forEach((item) => {
              if(item.hora >= startTime.format("HH:mm:ss") && item.hora <= finalTime.format("HH:mm:ss")){
               // console.log("hora filtered" + item.hora)

                let rlabel ='';

                rutasInfo.map((ritem) => { 
    
                   /*       
                  if(ritem.payload.external_id == tracking[0].payload.encabezado._relaciones.vehiculo.payload.id_ruta) 
                  {   
                    rlabel = ritem.payload.label;
                  }*/
                  if(ritem.payload.external_id == item.id_ruta) 
                  {   
                    rlabel = ritem.payload.label;
                  }
                  
                  })


                const info = {
                  isSelected:false,
                  isVisible:false,
                  rutaLabel:rlabel,
                  lat: item.latitud,
                  lng: item.longitud,
                  label: tracking[0].payload.encabezado._relaciones.vehiculo.payload.label,
                  fecha_hora:  item.hora,
                  velocidad:  item.velocidad,
                  recorrido:   item.kilometros,
                  subidas:   item.subidas,
                  bajadas: item.bajadas
    
                }
                
    
                localPath2.push(info)
               
             
              }
              
            })

            setPath2(localPath2);
            
          }
        }
        else{
          setPath2([]);
        }
      },[startTime,finalTime,tracking])





      React.useEffect(() => {
        rutasInfo.map((item) => { 

          
          if(item.payload.external_id == currentVehiculo.id_ruta) 
          {   
            setCurrentRutaLabel(item.payload.label)
          }
          
          }
       
      )},[currentVehiculo])


      

      function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
      }

      

      const divStyles = {
        whiteSpace: 'nowrap', // Evita saltos de línea
        overflow: 'hidden', // Puede ser útil si el contenido es más largo que el ancho del div
        textOverflow: 'ellipsis', // Agrega puntos suspensivos (...) si el contenido desborda
        width: '1200px', // Ajusta el ancho según tus necesidades
        border: '0px solid #ccc',
        padding: '15px',
      };

      
      const hoverCircleStyle = {
        transform: 'scale(1.1)', // Aumenta el tamaño al hacer hover
        boxShadow: '0px 0px 15px rgba(0, 0, 0, 0.7)', // Cambia la sombra al hacer hover
      };
     
      return (
        <Box sx={{ marginTop:0, height: '100vh' }}>
    

        <GoogleMap  mapContainerStyle={{
          margin: "auto",
          width: "100%",
          height: "100%"
        }} zoom={zoom} center={center} mapContainerClassName="map-container"  options={defaultMapOptions} >
        
   
        

   
<Box sx={{display:'flex'}}>
       

        <IconButton
        
                 sx={{paddingLeft:1, marginTop:10, height:30, marginLeft:2,background:'white',borderRadius:4,":hover": {
                  bgcolor: "#F2F4F4",
                  color: "black"
                } }} 
                onClick={() => handlemenuPanel()}
               

                >
                    <TableRowsRoundedIcon />
        </IconButton>
      

        
        <Button variant="contained"  color="primary" sx={{ marginTop:10, marginLeft:1, height:30,borderRadius:4,  color: 'black',background:'white', ":hover": {
          bgcolor: "#F2F4F4",
          color: "black"
        } }}  startIcon={<TimelineIcon />}   onClick={handleChange}>
          Tracking
        </Button>



        </Box>


   
               
          

        <PanelMode trackingPanel={trackingPanel} menuPanel={menuPanel}/>

    
 
        

        <div>


        {vehiculos.map((item,index) => {

          let rlabel ='';

        

            rutasInfo.map((ritem) => { 

                      
              if(ritem.payload.external_id == item.payload.id_ruta) 
              {   
                rlabel = ritem.payload.label;
              }
              
              })

              if(item['payload']['location'] != null){

            const info = {
              isSelected:false,
              isVisible:true,
              rutaLabel: rlabel,
              lat: item['payload']['location']['latitud'],
              lng: item['payload']['location']['longitud'],
              label: item['payload']['label'],
              fecha_hora: item['payload']['location']['fecha_hora'],
              velocidad: item['payload']['location']['velocidad'],
              recorrido: item['payload']['location']['kilometros'],
              subidas:  item['payload']['location']['subidas'],
              bajadas:  item['payload']['location']['bajadas']
            }
          
  
  return (
    <CustomVehiculoMarker item={info}  currentVehiculo={info} rutasInfo={rutasInfo}/>
   
  );}
})}



</div>


  


    <PintadorRutas/>

   


  
    {showComponent &&  <PintadorRutasMarkers/>}
   
  
    <Polyline
     
     path={path2}
     options={options3}
   />

   
    {path2.map((item,index) => {

    return (
      <CustomVehiculoMarkerTracking item={item} currentVehiculo={currentVehiculo} rutasInfo={rutasInfo}/>

    );

    })}

    
          

        </GoogleMap>
      </Box>
     
     
      );
    }



const RenderMap = () => {
    const [showComponent, setShowComponent] = React.useState(false);
    const [time, setTime] = React.useState(0);

  
    /*
    React.useEffect(() => {
         const timer = setTimeout(() => {
            setShowComponent(!showComponent);
            if(showComponent){setTime(2000)}
            else{setTime(0)}
        }, time);
    }, [showComponent]);


  
    return <>{showComponent && <Home />}</>;
*/

    return <Home />;

};

export default RenderMap;


  //    {checkVehiculoConnection(new Date(value['payload']['location']['fecha_hora'])) ?   <MdSignalWifi4Bar size='25px'/>  : <MdSignalWifiBad size='25px'/> }
                           