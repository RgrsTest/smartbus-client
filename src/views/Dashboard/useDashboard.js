import React from 'react';
import { Dashboard } from '../../Api';
import { Context } from '../../Context';

function useDashboard() {
    const { jwt } = React.useContext(Context)
    const dashboard = Dashboard(jwt)
    const [ dashboardData, setDashboardData ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)

    async function getDashboardData() {
        setLoading(true)
        const response = await dashboard.getDashboard()
        if (response.status === 'success'){

            setDashboardData(response.data)
        }
        setLoading(false)
    }

    React.useEffect(() => getDashboardData(),[])

    return {
        dashboardData,
        loading
    }
}

export {useDashboard}