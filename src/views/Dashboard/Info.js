import React from "react";
import {Card, CardContent, CardHeader, Typography} from '@mui/material'


function Info({title, content}){
    return(
        <Card
            sx={{maxWidth: 400, m: 2}}
            variant="outlined"
            >
              <CardHeader
              component="div"
              title={title}
              >
              </CardHeader>
              <CardContent>
                  <Typography variant='h4' >
                    {content}
                  </Typography>
              </CardContent>
            </Card>
    )
}

export {Info}