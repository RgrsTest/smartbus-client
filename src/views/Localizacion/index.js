import React from "react";
import 'leaflet/dist/leaflet.css'
import Leaflet from 'leaflet' 
import { MapContainer, TileLayer, Popup, Marker, Tooltip } from 'react-leaflet'
import { useLocalizacion } from "./useLocalizacion";

var icono = Leaflet.icon({
  iconUrl: '/assets/camion_conectado.png',
  iconSize: [20, 20]
})

function Localizacion(){
  const { localizaciones } = useLocalizacion()

  return (
        <MapContainer center={[20.679814516209408, -103.35538718081504]} zoom={13} style={{height:"80vh", width: '100%'}}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {localizaciones.map((item, index) => (
             <Marker icon={icono} position={[ item.posicion_actual.latitud, item.posicion_actual.longitud ]} key={index}>
               <Tooltip permanent={true}>{item.label}</Tooltip>
            </Marker>
          ))}
        </MapContainer>
  )
}

export { Localizacion }