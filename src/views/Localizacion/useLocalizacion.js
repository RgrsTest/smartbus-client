import React from 'react';
import { Tracking } from '../../Api';
import { Context } from '../../Context';

function useLocalizacion(){
    const { jwt } = React.useContext(Context)
    const { getInicialStatus, initSocket } = Tracking(jwt)
    const [ localizaciones, setLocalizaciones ] = React.useState([])

    function replaceLocalizacion(payload, initValue){
        let indexValue = initValue.findIndex((value) => value.id_vehiculo === payload.payload.id_vehiculo)
        if (indexValue !== -1){
            let actualValues = initValue
            actualValues.splice(indexValue, 1, payload.payload)
            setLocalizaciones(actualValues)
        }
        else{
            let resultado = initValue
            resultado.push(payload.payload)
            setLocalizaciones(resultado)
        }
    }
    React.useEffect(async () => {
        let datos = await getInicialStatus()
        setLocalizaciones(datos.data)
        initSocket(replaceLocalizacion, datos.data)
    },[])


    return {
        localizaciones
    }
}

export {useLocalizacion}