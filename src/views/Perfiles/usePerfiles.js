import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function usePerfiles() {
    const { jwt } = React.useContext(Context)
    const [ perfiles, setPerfiles ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const perfilesCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'perfiles',
    }
    
    async function getPerfiles(){
      setLoading(true)
      try{
        const response = await perfilesCatalogo.consultarCatalogo(payload)
        setPerfiles(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updatePerfiles(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await perfilesCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deletePerfiles(id){
      try{
        payload.id = id
        const response = await perfilesCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          return true
        }
      }
      catch (e){
        setError(true)
      }
      return false
    }

    React.useEffect(getPerfiles, [])
    return {
      perfiles,
      loading,
      error,
      setLoading,
      setError,
      getPerfiles,
      updatePerfiles,
      deletePerfiles
    }
}

export {usePerfiles}