import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { Avatar, Box, Button, Fab, IconButton, List, ListItem, ListItemAvatar, ListItemText, Modal, TextField, Paper, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from "@mui/material";
import PerfilIcon from '@mui/icons-material/PeopleAlt';
import { usePerfiles } from './usePerfiles'
import { useVersiones } from "../components/useVersiones";
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import { Loading } from "../components/Loading";
import { DeleteModal } from './DeleteModal'
import { useProductos } from "../Productos/useProductos";
import dayjs from 'dayjs';

const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Perfiles(){
  const [ modal, setModal ] = React.useState(false)
  const [ change, setChange ] = React.useState(false)
  const { perfiles, loading, getPerfiles,  updatePerfiles, deletePerfiles } = usePerfiles()
  const { versiones, loadingVersiones, getVersiones, updateVersiones, deleteVersiones } = useVersiones("Perfiles")
  const { productos } = useProductos()
  const [ search, setSearch ] = React.useState('')
  const [ localPerfiles, setLocalPerfiles ] = React.useState([])
  const [ payload, setPayload ] = React.useState(null)
  const [ deleteModal, setDeleteModal ] = React.useState(false)

  const [openConfirmUpdateDialog, setOpenConfirmUpdateDialog] = React.useState(false);

  const [lastUpdate, setLastUpdate] = React.useState(null);

  

  

  

  React.useEffect(() =>{
   
    if(versiones[versiones.length - 1] != undefined)
    {
      setLastUpdate(versiones[versiones.length - 1].payload.ultima_actualizacion);
    }

  },[versiones])



  
  const handleOpenConfirmUpdate = () => {
    setOpenConfirmUpdateDialog(true);
  };

  const handleCloseConfirmUpdate = () => {
    setOpenConfirmUpdateDialog(false);
  };
  

  const handleConfirmUpdate = async () => {
    let send = null
    console.log("versiones")
    console.log(versiones)
    if (versiones[versiones.length - 1] != undefined){
      console.log("not undefined")
        send = await updateVersiones({catalogo:"Perfiles", ultima_actualizacion: dayjs().format('YYYY-MM-DD HH:mm:ss')}, versiones[versiones.length - 1].id)
    }
    else{
        send = await updateVersiones({catalogo:"Perfiles", ultima_actualizacion: dayjs().format('YYYY-MM-DD HH:mm:ss')})
    }

    if (send){
      setLastUpdate(dayjs().format('YYYY-MM-DD HH:mm:ss'));
      getVersiones();
    }

    handleCloseConfirmUpdate();
  };







  React.useEffect(() =>{
    
    if(search.length > 0){
        let searching = search.toLocaleLowerCase()
        let valores = perfiles.filter((item) => {
            const itemText = item.payload.label.toLowerCase()
            return itemText.includes(searching)
        })
        setLocalPerfiles(valores)
    }else{
        setLocalPerfiles(perfiles)
    }
  },[search, perfiles])

  React.useEffect( getPerfiles, [change])

  

  return (
    <Paper elevation={0}  sx={{width:"95%", marginTop:10, marginLeft:4}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:0, fontSize: 35}}>Perfiles</Box>

    <Dialog
        open={openConfirmUpdateDialog}
        onClose={handleCloseConfirmUpdate}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Esta seguro en actualizar todos los dispositivos?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Se actualizara la configuracion de los perfiles 
            en todos los dispositivos.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmUpdate}>Cancelar</Button>
          <Button onClick={handleConfirmUpdate} autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>



      <Box
      sx={{flexGrow: 1}}
      >
        <Container >
          <TextField sx={{width: '30%', mr: '2%'}}
          placeholder="Escriba para buscar"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          />
          <Button sx={{width: '8%', height: 50,mr: '2%'}} onClick={() => {setModal(true); setPayload(null)}} variant='contained' >
            Agregar
          </Button>

          <Button  sx={{width: '18%', height: 50,mr: '20%'}} onClick={handleOpenConfirmUpdate} variant='contained' >
            Actualizar Dispositivos
          </Button>

          <Box sx={{display: 'flex', marginBottom:0, marginLeft:0, fontSize: 20}}>Ultima actualizacion:{lastUpdate}   </Box>
        
        </Container> 
        <List >
          {localPerfiles.map((item, index) => (
            
            <ListItem
              key={index}
              secondaryAction={
                <Box>
                  <IconButton edge="end" aria-label="delete" onClick={() => {setModal(true); setPayload(item)}}>
                      <EditIcon />
                  </IconButton>
                  <IconButton edge="end" aria-label="delete" onClick={() => {setDeleteModal(true); setPayload(item)}}>
                      <DeleteIcon />
                  </IconButton>
                    
                </Box>
              }
            >
              <ListItemAvatar>
                <Avatar>
                  <PerfilIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.payload.usuario_tipo}
                secondary={'Codigo: ' + item.payload.codigo_perfil}
              />
            </ListItem>
            ))
          }
        </List>
        <DeleteModal status={deleteModal} setStatus={setDeleteModal} item={payload} success={change} setSucess={setChange} action={deletePerfiles} loading={loading} />
        <Loading status={loading} />
        <Formulario status={modal} setStatus={setModal} success={change} setSuccess={setChange} action={updatePerfiles} loading={loading} item={payload} productos={productos} />
      </Box> </Paper>
  )
}

export {Perfiles}