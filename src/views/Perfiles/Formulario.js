import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardHeader, FormControlLabel, FormGroup, Modal, TextField, Checkbox, FormControl, FormLabel, IconButton, Select, MenuItem, List, ListItem } from "@mui/material";
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from "@mui/icons-material/Delete";
import React from "react";
import { Loading } from "../components/Loading";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };


function Formulario( { status, setStatus, success, setSuccess, loading, action, productos = [], item=null } ){
    const [ codigoPerfil,  setCodigoPerfil] = React.useState('')
    const [ usuarioTipo, setUsuarioTipo ] = React.useState('')
    const [ antiPassback, setAntiPassback ] = React.useState('')
    const [ credito, setCredito ] = React.useState(false)
    const [ transbordo, setTransbordo ] = React.useState(false)
    const [ errorCodigoPerfil, setErrorCodigoPerfil ] = React.useState(false)
    const [ errorLegendCodigoPerfil, setErrorLegendCodigoPerfil ] = React.useState('')
    const [ errorUsuarioTipo, setErrorUsuarioTipo ] = React.useState(false)
    const [ errorLegendUsuarioTipo, setErrorLegendUsuarioTipo ] = React.useState('')
    const [ errorAntiPassback, setErrorAntiPassback ] = React.useState(false)
    const [ errorLegendAntiPassback, setErrorLegendAntiPassback ] = React.useState('')
    const [ title, setTitle ] = React.useState('Crear nuevo perfil')
    const [ button, setButton ] = React.useState('Agregar')
    const [ localModal, setLocalModal ] = React.useState(false)
    const [ productosPerfil, setProductosPerfil ] = React.useState([])
    const [ productoSelected, setProductoSelected ] = React.useState({})
    const [ tarifa, setTarifa ] = React.useState('')
    const [ monto, setMonto ] = React.useState('')
    const [ errorProductoSelected, setErrorProductoSelected ] = React.useState(false)
    const [ errorLegendProductoSelected, setErrorLegendProductoSelected ] = React.useState('')
    const [ errorTarifa, setErrorTarifa ] = React.useState(false)
    const [ errorLegendTarifa, setErrorLegendTarifa ] = React.useState('')
    const [ errorMonto, setErrorMonto ] = React.useState(false)
    const [ errorLegendMonto, setErrorLegendMonto ] = React.useState('')

    const handlePayload = () => {
        if (item){
            setTitle('Editar perfil')
            setButton('Guardar')
            setCodigoPerfil(item.payload.codigo_perfil)
            setUsuarioTipo(item.payload.usuario_tipo)
            setAntiPassback(item.payload.anti_passback)
            item.payload.transbordo && setTransbordo(true)
            item.payload.credito && setCredito(true)
            item.payload.productos && setProductosPerfil(item.payload.productos)
        }
        else {
            setTitle('Crear nuevo perfil')
            setButton('Agregar')
            setCodigoPerfil('')
            setUsuarioTipo('')
            setAntiPassback('')
            setTransbordo(false)
            setCredito(false)
            setErrorCodigoPerfil(false)
            setErrorUsuarioTipo(false)
            setErrorAntiPassback(false)
            setErrorLegendCodigoPerfil('')
            setErrorLegendUsuarioTipo('')
            setErrorLegendAntiPassback('')
            setProductosPerfil([])
        }
    }

    React.useEffect(handlePayload, [item])

    const handleUsuarioTipo = () => {
        if ( errorUsuarioTipo ) {
            setErrorUsuarioTipo(false)
            setErrorLegendUsuarioTipo('')
        }
    }

    const handleAntipassBack = () => {
        if ( antiPassback ) {
            setErrorAntiPassback(false)
            setErrorLegendAntiPassback('')
        }
    }

    const handleCodigoPerfil = () => {
        if ( errorCodigoPerfil ) {
            setErrorCodigoPerfil(false)
            setErrorLegendCodigoPerfil('')
        }
    }

    const handleAction = async () => {
        let ok = true
        if (codigoPerfil.length > 0) {
            setErrorCodigoPerfil(true)
            setErrorLegendCodigoPerfil('Este campo es obligatorio')
            ok = false
        }
        if (!usuarioTipo) {
            setErrorUsuarioTipo(true)
            setErrorLegendUsuarioTipo('Este campo es obligatorio')
            ok = false
        }
        if (!antiPassback){
            setErrorAntiPassback(true)
            setErrorLegendAntiPassback('Este campo es obligatorio')
            ok = false
        }
        if (ok){
            let send = null
            if (item){
                send = await action({codigo_perfil: parseInt(codigoPerfil), usuario_tipo: usuarioTipo.toUpperCase(), anti_passback: parseInt(antiPassback), credito: credito, transbordo: transbordo, productos: productosPerfil}, item.id)
            }
            else{
                send = await action({codigo_perfil: parseInt(codigoPerfil), usuario_tipo: usuarioTipo.toUpperCase(), anti_passback: parseInt(antiPassback), credito: credito, transbordo: transbordo, productos: productosPerfil})
            }

            if (send){
                setCodigoPerfil('')
                setUsuarioTipo('')
                setAntiPassback('')
                setSuccess(!success)
                handleClose(true)
            }
        }
    }
    const handleClose = (close = false) => {
        if (item == null || close){
            setCodigoPerfil('')
            setUsuarioTipo('')
            setAntiPassback('')
            setTransbordo(false)
            setCredito(false)
            setErrorCodigoPerfil(false)
            setErrorUsuarioTipo(false)
            setErrorAntiPassback(false)
            setErrorLegendCodigoPerfil('')
            setErrorLegendUsuarioTipo('')
            setErrorLegendAntiPassback('')
            setProductosPerfil([])
        }
        setStatus(false)
    }

    const handleAgregarProducto = () => {
        let ok = true
        if (productoSelected === {}){
            setErrorProductoSelected(true)
            setErrorLegendProductoSelected('Este campo es obligatorio')
            ok = false
        }
        if (!tarifa){
            setErrorTarifa(true)
            setErrorLegendTarifa('Este campo es obligatorio')
            ok = false
        }
        if (!monto){
            setErrorMonto(true)
            setErrorLegendMonto('Este campo es obligatorio')
            ok = false
        }
        if (ok){
            let objeto = productosPerfil
            objeto.push({
                productoId: productoSelected.payload.id_producto,
                tarifa: parseInt(tarifa),
                maximoValor: parseInt(monto)
            })
            
            setProductosPerfil(objeto)
            handleCloseProducto()
        }
    }

    const handleRemoveProducto = (valor) => {
        let newObjeto = productosPerfil.filter((find) => find !== valor)
        setProductosPerfil(newObjeto)
    } 

    const handleCloseProducto = () => {
        setProductoSelected({})
        setTarifa('')
        setMonto('')
        setErrorMonto(false)
        setErrorTarifa(false)
        setErrorLegendMonto('')
        setErrorLegendTarifa('')
        setLocalModal(false)
    }

    const handleMonto = () => {
        if (errorMonto){
            setErrorMonto(false)
            setErrorLegendMonto('')
        }
    }

    const handleTarifa = () => {
        if (errorTarifa){
            setErrorTarifa(false)
            setErrorLegendTarifa('')
        }
    }

    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                        <TextField type='number' required error={errorCodigoPerfil} helperText={errorLegendCodigoPerfil} sx={{m: 1, width: 200}} label='Codigo Perfil' value={ codigoPerfil } onKeyPress={(e) => handleCodigoPerfil(e)} onChange={ (e) => setCodigoPerfil(e.target.value) } />
                        <TextField type='number' required error={errorAntiPassback} helperText={errorLegendAntiPassback} sx={{m: 1, width: 200}} label='Anti Passback' value={ antiPassback } onKeyPress={() => handleAntipassBack()} onChange={(e) => { setAntiPassback(e.target.value)}} />
                        <TextField required error={errorUsuarioTipo} helperText={errorLegendUsuarioTipo} sx={{m: 1, width: 415}} label='Tipo De Usuario' value={ usuarioTipo }  onChange={ (e) => {setUsuarioTipo(e.target.value); handleUsuarioTipo()} } />
                        <FormGroup>                        
                            <FormControlLabel sx={{m: 1}} control={<Checkbox />} checked={credito} onChange={(e) => setCredito(e.target.checked)} label="Credito" />
                            <FormControlLabel sx={{m: 1}} control={<Checkbox />} checked={transbordo} onChange={(e) => setTransbordo(e.target.checked)} label="Transbordo" />
                        </FormGroup>
                        <FormControl>
                            <FormGroup>
                                <FormLabel>Productos</FormLabel>
                                <List sx={{maxHeight: 80, overflow: 'auto'}}>
                                    {productosPerfil.map((item, index) => (
                                        <ListItem
                                        key={index}
                                        secondaryAction={
                                          <Box>
                                            <IconButton edge="end" aria-label="delete" onClick={() => handleRemoveProducto(item)}>
                                                <DeleteIcon />
                                            </IconButton>
                                          </Box>
                                        }
                                      >
                                          {item.productoId}
                                      </ListItem>
                                    ))}
                                </List>
                                <IconButton onClick={() => setLocalModal(!localModal)}>
                                    <AddIcon />
                                </IconButton>
                            </FormGroup>
                        </FormControl>
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleAction()}>
                            {button}
                        </Button>
                        <Button onClick={() => handleClose()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Modal open={localModal}>
                    <Card style={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: 500,
                        bgcolor: 'background.paper',
                        boxShadow: 24,
                        p: 4
                      }
                    }>
                        <CardHeader component="div"  title='Agregar Producto' />
                        <CardContent>
                            <Select error={errorProductoSelected} helperText={errorLegendProductoSelected}  sx={{m: 1, width: 417}} label='Selecciona un producto' value={productoSelected} onChange={(e) => setProductoSelected(e.target.value)} >
                                { productos.map((item, index) => (
                                <MenuItem value={item} key={index} >{item.payload.producto_nombre}</MenuItem>
                                ))}
                            </Select>
                            <TextField required onChange={(e) => setTarifa(e.target.value)} onKeyPress={() => handleTarifa()} type='number' error={errorTarifa} helperText={errorLegendTarifa} sx={{m: 1, width: 200}} label='Tarifa' />
                            <TextField required onChange={(e) => setMonto(e.target.value)} onKeyPress={() => handleMonto()} type='number' error={errorMonto} helperText={errorLegendMonto} sx={{m: 1, width: 200}} label='Monto Maximo' />
                        </CardContent>
                        <CardActions>
                            <Button onClick={() => handleAgregarProducto()} >
                                Aceptar
                            </Button>
                            <Button onClick={() => handleCloseProducto()}>
                                Cancelar
                            </Button>
                        </CardActions>
                    </Card>
                </Modal>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}