import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useOperadores() {
    const { jwt } = React.useContext(Context)
    const [ operadores, setOperadores ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const operadoresCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'operadores',
    }
    
    async function getOperadores(){
      setLoading(true)
      try{
        const response = await operadoresCatalogo.consultarCatalogo(payload)
        setOperadores(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updateOperadores(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await operadoresCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteOperadores(id){
      try{
        payload.id = id
        const response = await operadoresCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          return true
        }
      }
      catch (e){
        setError(true)
      }
      return false
    }

    React.useEffect(getOperadores, [])
    return {
      operadores,
      loading,
      error,
      setLoading,
      setError,
      getOperadores,
      updateOperadores,
      deleteOperadores
    }
}

export {useOperadores}