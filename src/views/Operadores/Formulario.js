import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardHeader, Modal, TextField } from "@mui/material";
import React from "react";
import { Loading } from "../components/Loading";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };


function Formulario( { status, setStatus, success, setSuccess, loading, action, item=null } ){
    const [ externalId,  setExternalId] = React.useState('')
    const [ label, setLabel ] = React.useState('')
    const [ nombre, setNombre ] = React.useState('')
    const [ errorExternalId, setErrorExternalId ] = React.useState(false)
    const [ errorLegendExternalId, setErrorLegendExternalId ] = React.useState('')
    const [ errorLabel, setErrorLabel ] = React.useState(false)
    const [ errorLegendLabel, setErrorLegendLabel ] = React.useState('')
    const [ errorNombre, setErrorNombre ] = React.useState(false)
    const [ errorLegendNombre, setErrorLegendNombre ] = React.useState('')
    const [ title, setTitle ] = React.useState('Crear nuevo operador')
    const [ button, setButton ] = React.useState('Agregar')

    const handlePayload = () => {
        if (item){
            setTitle('Editar operador')
            setButton('Guardar')
            item.payload.externalId && setExternalId(item.payload.external_id)
            item.payload.label && setLabel(item.payload.label)
            item.payload.nombre && setNombre(item.payload.nombre)
        }
        else {
            setTitle('Crear nuevo operador')
            setButton('Agregar')
            setExternalId('')
            setLabel('')
            setNombre('')
        }
    }

    React.useEffect(handlePayload, [item])

    const handleLabel = () => {
        if ( errorLabel ) {
            setErrorLabel(false)
            setErrorLegendLabel('')
        }
    }

    const handleExternalId = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && externalId.length >= 4 ){
            event.preventDefault()
        }
        else {
            if ( errorExternalId ) {
                setErrorExternalId(false)
                setErrorLegendExternalId('')
            }
        }
    }

    const handleAction = async () => {
        let ok = true
        if (!externalId) {
            setErrorExternalId(true)
            setErrorLegendExternalId('Este campo es obligatorio')
            ok = false
        }
        else if (externalId.length < 4) {
            setErrorExternalId(true)
            setErrorLegendExternalId('External ID debe ser un numero hexadecimal de 4 digitos')
            ok = false
        }
        if (!label) {
            setErrorLabel(true)
            setErrorLegendLabel('Este campo es obligatorio')
            ok = false
        }
        if (!nombre){
            setErrorNombre(true)
            setErrorLegendNombre('Este campo es obligatorio')
            ok = false
        }
        if (ok){
            let send = null
            if (item){
                send = await action({external_id: externalId.toLowerCase(), label: label.toUpperCase(), nombre: nombre.toUpperCase()}, item.id)
            }
            else{
                send = await action({external_id: externalId.toLowerCase(), label: label.toLowerCase(), nombre: nombre.toUpperCase()})
            }
            if (send){
                setExternalId('')
                setLabel('')
                setNombre('')
                setSuccess(!success)
                setStatus(false)
            }
        }
    }
    const handleClose = () => {
        setExternalId('')
        setLabel('')
        setNombre('')
        setErrorExternalId(false)
        setErrorLabel(false)
        setErrorNombre(false)
        setErrorLegendExternalId('')
        setErrorLegendLabel('')
        setErrorLegendNombre('')
        setStatus(false)
    }
    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                            <TextField required error={errorExternalId} helperText={errorLegendExternalId} sx={{m: 2, width: 400}} label='External ID' value={ externalId } onKeyPress={(e) => handleExternalId(e)} onChange={ (e) => setExternalId(e.target.value) } />
                            <TextField required error={errorLabel} helperText={errorLegendLabel} sx={{m: 2, width: 400}} label='Label' value={ label }  onChange={ (e) => {setLabel(e.target.value); handleLabel()} } />
                            <TextField required error={errorNombre} helperText={errorLegendNombre} sx={{m: 2, width: 400}} label='Nombre' value={ nombre } onChange={(e) => setNombre(e.target.value)} />
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleAction()}>
                            {button}
                        </Button>
                        <Button onClick={() => handleClose()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}