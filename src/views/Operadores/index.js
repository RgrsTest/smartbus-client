import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { Avatar, Box, Button, Fab, IconButton, List, ListItem, ListItemAvatar, ListItemText, Modal, TextField ,Paper} from "@mui/material";
import {useOperadores} from './useOperadores'
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import OperadorIcon from '@mui/icons-material/Face';
import { Loading } from "../components/Loading";
import { DeleteModal } from './DeleteModal'

const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Operadores(){
  const [ modal, setModal ] = React.useState(false)
  const [ change, setChange ] = React.useState(false)
  const { operadores, loading, getOperadores, updateOperadores, deleteOperadores } = useOperadores()
  const [ search, setSearch ] = React.useState('')
  const [ localOperadores, setLocalOperadores ] = React.useState([])
  const [ payload, setPayload ] = React.useState(null)
  const [ deleteModal, setDeleteModal ] = React.useState(false)

  React.useEffect(() =>{
    
    if(search.length > 0){
        let searching = search.toLocaleLowerCase()
        let valores = operadores.filter((item) => {
            const itemText = item.payload.label.toLowerCase()
            return itemText.includes(searching)
        })
        setLocalOperadores(valores)
    }else{
        setLocalOperadores(operadores)
    }
  },[search, operadores])

  React.useEffect( getOperadores, [change])
  return (
    <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Operadores</Box>
      <Box
      sx={{width:"95%", marginTop:1, marginLeft:4}}
      >
        <Container >
          <TextField sx={{width: '50%', mr: '10%'}}
          placeholder="Escriba para buscar"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          />
          <Button sx={{width: '40%', height: 50}} onClick={() => (setModal(true))} variant='contained' >
            Agregar
          </Button>
        
        </Container> 
        <List >
          {localOperadores.map((item, index) => (
            
            <ListItem
              key={index}
              secondaryAction={
                <Box>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setModal(true); setPayload(item)}}>
                        <EditIcon />
                    </IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setDeleteModal(true); setPayload(item)}}>
                        <DeleteIcon />
                    </IconButton>
                </Box>
              }
            >
              <ListItemAvatar>
                <Avatar>
                  <OperadorIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.payload.label}
                secondary={item.payload.nombre}
              />
            </ListItem>
            ))
          }
        </List>
        <DeleteModal status={deleteModal} setStatus={setDeleteModal} item={payload} success={change} setSucess={setChange} action={deleteOperadores} loading={loading} />
        <Loading status={loading} />
        <Formulario status={modal} setStatus={setModal} success={change} setSuccess={setChange} action={updateOperadores} loading={loading} item={payload} />
      </Box> </Paper>
  )
}

export {Operadores}