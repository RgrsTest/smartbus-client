import React from 'react'
import { Box, TextField, Button, Card, CardHeader, CardContent, Modal, CircularProgress, Divider ,CardMedia} from '@mui/material';
import { Context } from '../../Context';
import { authenticate } from '../../Api';
import { useLocation, useNavigate } from 'react-router';

import Image from  './bus3.jpg'; 


const style = {
  position: 'absolute',
  top: '50%',
  left: '15%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  height:600,
  backgroundColor: 'white',
  boxShadow: 24,
  p: 2  
};

function LoginMIU() {

  const [ username, setUsername ] = React.useState('')
  const [ password, setPassword ] = React.useState('')
  const [ usernameError, setUsernameError ] = React.useState(false)
  const [ passwordError, setPasswordError ] = React.useState(false)
  const [ usernameHelper, setUsernameHelper ] = React.useState('')
  const [ passwordHelper, setPasswordHelper ] = React.useState('')
  const { jwt, auth, setAuth, handleSetJWT, setLoading, loading } = React.useContext(Context)
  const navigate = useNavigate()
  const location = useLocation()
  
  let from = location.state?.from?.pathname || '/'

  React.useEffect(()=> {
    if (auth && jwt !== ''){
      navigate(from, {replace: true})
    }
  },[jwt, auth, location])

  const handleAuthentication = async (data) => {

    style.backgroundColor = 'transparent';
    setLoading( true )
    try{
      let jwt = await authenticate(data.username, data.password)
      handleSetJWT(jwt)
      setAuth(true)
      navigate(from, {replace: true})
    }
    catch(e){
      if (e.errors.correo) {
        setUsernameError(true)
        setUsernameHelper(e.errors.correo[0])
      } 
      if (e.errors.contrasena) {
        setPasswordError(true)
        setPasswordHelper(e.errors.contrasena[0])
      }
    }
    setLoading(false)

    
  }

  return (
    <Box
      component="form"
      sx={{
       display: 'flex',
       backgroundImage: `url(${Image})`,
       backgroundSize: "cover",
      height: "100vh",
      color: "#f5f5f5"
      }}
      autoComplete="off"
    >
      <Card
      sx={style}
      variant="outlined"
      
      >
         <CardMedia
        sx={{ height: 200,width:300, justifyContent:'center', marginLeft:6, marginTop:4, marginBottom:8 }}
        image="/assets/smartbus2.png"
        
      />
       
        <CardContent>
          <TextField 
          label="Usuario"
          required
          error={usernameError}
          type="email"
          value = {username}
          helperText= {usernameHelper}
          onChange = {(e) => setUsername(e.target.value)}
          onKeyDown = {(e)=> {if(e.key==="Enter") handleAuthentication({username: username, password: password})}}
          sx={{m: 1, width: '35ch', marginBottom:3}}
          />
          <TextField 
          label="Contraseña"
          required
          error={passwordError}
          helperText= {passwordHelper} 
          type="password"
          value = {password}
          onChange ={(e) => setPassword(e.target.value)}
          onKeyDown = {(e)=> {if(e.key==="Enter") handleAuthentication({username: username, password: password})}}
          sx={{m: 1, width: '35ch',marginBottom:6}}
          />
          <Divider></Divider>
          <Button
          variant='contained'
          sx={{m: 1, width: '25ch'}}
          onClick={()=> {
            handleAuthentication({ username: username, password: password })} }
          >
            Ingresar
          </Button>
        </CardContent>
      </Card>
          
      <Modal
      open={loading}
      >
        <Card sx={style}>
          <CardHeader
          component="div"
          title="Cargando"
          sx={{marginLeft:17, marginTop:25, marginBottom:5}}
          />
          <CardContent>
          <CircularProgress  sx={{marginLeft:20}} />
          </CardContent>
        </Card>
      </Modal>
    </Box>
  );
}


export { LoginMIU }