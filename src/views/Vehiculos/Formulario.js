
import { Box, Button, Card, CardActions,Autocomplete, CardContent, CardHeader, List, ListItem, ListItemButton, Modal, TextField, Typography } from "@mui/material";
import React from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function Formulario( { status, setStatus, success, setSuccess, action, item, data = [] , rutas} ){
    const [ externalId, setExternalId ] = React.useState('')
    const [ label, setLabel ] = React.useState('')
    const [ dispositivo, setDispositivo ] = React.useState('')
    const [ dispositivoId, setDispositivoId ] = React.useState('')
    const [ errorExternalId, setErrorExternalId ] = React.useState(false)
    const [ errorLabel, setErrorLabel ] = React.useState(false)
    const [ errorLegendLabel, setErrorLegendLabel ] = React.useState('')
    const [ errorLegendExternalId, setErrorLegendExternalId ] = React.useState('')
    const [ title, setTitle ] = React.useState('Crear nuevo vehiculo')
    const [ button, setButton ] = React.useState('Agregar')
    const [ dataMostrar, setDataMostrar ] = React.useState([])

    const [ rutaLabel, setRutaLabel ] = React.useState(null)
    const [ rutaID, setRutaID ] = React.useState(null)


  //  console.log('Rutas')
  //  console.log(rutas)

    React.useEffect(() => {
        if (item){
            setButton('Guardar')
            setTitle('Editar Vehiculo')
            item.payload.external_id && setExternalId(item.payload.external_id)
            item.payload.label && setLabel(item.payload.label)
            if (item.payload.dispositivo){
                setDispositivoId(item.payload.dispositivo)
                let dis = data.find((valor) => valor.id === item.payload.dispositivo)
                setDispositivo(dis.payload.label)
            }
            if (item.payload.id_ruta){
               const rutaVehiculo = rutas.filter(ruta => parseInt(ruta.payload.external_id) == item.payload.id_ruta)
               console.log('Ruta vehiculo')
               console.log(rutaVehiculo)
             setRutaLabel(rutaVehiculo[0].payload.label);
            }

        }
        else {
            setButton('Agregar')
            setTitle('Crear nuevo vehiculo')
            setExternalId('')
            setLabel('')
            setDispositivo('')
            setDispositivoId('')
        }
    }, [item])

    React.useEffect(() => {
        if (dispositivo.length > 0){
            let searching = dispositivo.toLocaleLowerCase()
            let valores = data.filter((item) => {
              const itemText = item.payload.label.toLowerCase()
              return itemText.includes(searching)
            })
              setDataMostrar(valores)
          }else{
            setDataMostrar(data)
          }
    },[data, dispositivo])

    const handleExternalId = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && externalId.length >= 4 ){
            event.preventDefault()
        }
        else {
            if ( errorExternalId ) {
                setErrorExternalId(false)
                setErrorLegendExternalId('')
            }
        }
    }

    const handleDispositivoBlur = () => {
        if (!dispositivoId) setDispositivo('')
    }

    const handleLabel = () => {
        if ( errorLabel ) {
            setErrorLabel(false)
            setErrorLegendLabel('')
        }
    }

    const handleUpdateVehiculos = async () => {
        let ok = true
        if (!externalId){
            setErrorExternalId(true)
            setErrorLegendExternalId('Este campo es obligatorio')
            ok = false
        }
        else if (externalId.length < 4) {
            setErrorExternalId(true)
            setErrorLegendExternalId('El External ID debe ser un numero hexadecimal de 4 digitos')
            ok = false
        }
        if (!label){
            setErrorLabel(true)
            setErrorLegendLabel('Este campo es obligatorio')
            ok = false
        }

        if (ok) {
            let valores = {
                external_id: externalId.toLowerCase(),
                label: label.toUpperCase(),
                id_ruta: parseInt(rutaID)
            }
            if (dispositivoId) valores.dispositivo = parseInt(dispositivoId)

            let send = null
            if (item){
                send = await action(valores, item.id)
            }
            else {
                send = await action(valores)
            }

            if(send){
                setSuccess(!success)
                setExternalId('')
                setLabel('')
                setErrorExternalId(false)
                setErrorLabel(false)
                setErrorLegendExternalId('')
                setErrorLegendLabel('')
                setDispositivo('')
                setDispositivoId('')
                setStatus(false)
            }
        }
    }

    const handleClose = () => {
        if(!item){
            setExternalId('')
            setLabel('')
            setErrorExternalId(false)
            setErrorLabel(false)
            setErrorLegendExternalId('')
            setErrorLegendLabel('')
            setDispositivo('')
            setDispositivoId('')
        }
        setStatus(false)
    }

    const handleClickList = (value) =>{
        setDispositivo(value.payload.label)
        setDispositivoId(value.id)
    }

    const handleRuta = (newValue) => {

        rutas.forEach((item) => {
          if(item.payload.label === newValue){
          
            
            setRutaID(item.payload.external_id);

    
          }})

      }

    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                        <TextField error={errorExternalId} helperText={errorLegendExternalId} value={externalId} onKeyPress={(e) => handleExternalId(e)} onChange={(e) => {setExternalId(e.target.value)}} sx={{ m: 2, width: 400 }} label='External ID' required />
                        <TextField error={errorLabel} helperText={errorLegendLabel} value={ label } onKeyPress={() => handleLabel()} onChange={(e) => setLabel(e.target.value)} sx={{ m: 2, width: 400 }} label='Label'  required />
                        <TextField value={dispositivo} onChange={(e) => setDispositivo(e.target.value)} helperText='Escriba para mostrar la lista de coincidencias' sx={{ m: 2, width: 400 }} label='Dispositivo' onBlur={() => handleDispositivoBlur()} />
                        
                        <Autocomplete 
                    
                    
                          
                            sx={{ m: 2, width: 400 }}
                            options={rutas.map((itemRutas) => itemRutas.payload.label)}
                            value={rutaLabel}
                            onChange={(e, newValue) => handleRuta(newValue)}
                            renderInput={(params) => <TextField {...params} label='Ruta' /> }

                        />
                        
                        
                        <Typography sx={{ m: 1, width: 400 }}>
                            Dispositivos
                        </Typography>
                        <List dense sx={{maxHeight: 50, overflow: 'auto'}}>
                            { dataMostrar.map((dat, index) => (
                                <ListItemButton selected={dispositivoId === dat.id} onClick={() => handleClickList(dat)} key={index}>{dat.payload.label}</ListItemButton>
                            ))}
                        </List>
                        
                    </CardContent>
                    <CardActions>
                        <Button onClick={handleUpdateVehiculos}>
                            {button}
                        </Button>
                        <Button onClick={handleClose}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
            </Box>
        </Modal>
    )
}

export {Formulario}