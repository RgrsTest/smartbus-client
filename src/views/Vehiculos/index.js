import React from "react";
import EditIcon from "@mui/icons-material/Edit";
import BusIcon from '@mui/icons-material/DirectionsBus';
import { Avatar, Box, Button, Fab, IconButton, List, ListItem, ListItemAvatar, ListItemText, Modal, Switch, TextField , Paper} from "@mui/material";
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import { useVehiculos } from "./useVehiculos";
import { useRutas } from "../Rutas/useRutas";
import { Loading } from "../components/Loading";
import { useDispositivos } from '../Dispositivos/useDispositivos'
import { RiLayoutMasonryLine } from "react-icons/ri";

const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Vehiculos(){
  const [ modal, setModal] = React.useState(false)
  const { vehiculos, loading, getVehiculos, updateVehiculos } = useVehiculos()
  const { dispositivos } = useDispositivos()
  const [ search, setSearch ] = React.useState('')
  const [ localVehiculos, setLocalVehiculos ] = React.useState([])
  const [ change, setChange ] = React.useState(false)
  const [ payload, setPayload ] = React.useState(null)


  const { rutas, loadingRutas, updateRutas, deleteRutas, getRutas, remplazarRutas } = useRutas()

  React.useEffect(() =>{
    
    if(search.length > 0){
      let searching = search.toLocaleLowerCase()
      let valores = vehiculos.filter((item) => {
        const itemText = item.payload.label.toLowerCase()
        return itemText.includes(searching)
      })
        setLocalVehiculos(valores)
    }else{
      setLocalVehiculos(vehiculos)
    }
  },[search, vehiculos])

  React.useEffect(getVehiculos, [change])

  const changeSwitch = async (evt, item) => {
    const send = await updateVehiculos({active: evt.target.checked}, item.id)
    if (send){
      setChange(!change)
    }
  }

  const handleChecked = (item) => {
    if (item.payload.active === undefined){
      return true
    }
    else {
      return item.payload.active
    }
  }

  return (
    <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
         <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Vehiculos</Box>
      

      <Box
      sx={{width:"95%", marginTop:1, marginLeft:4}}
      >
        <Container >
          <TextField sx={{width: '50%', mr: '10%'}}
          placeholder="Escriba para buscar"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          />
          <Button sx={{width: '40%', height: 50}} onClick={() => {setModal(true); setPayload(null)}} variant='contained' >
            Agregar
          </Button>
        
        </Container> 
        <List >
          {localVehiculos.map((item, index) => (
            
            <ListItem
              key={index}
              
              secondaryAction={
                <Box>
                    <IconButton edge="end" aria-label="edit" onClick={() => {setModal(true); setPayload(item)}} disabled={!handleChecked(item)}>
                        <EditIcon />
                    </IconButton>
                    <Switch checked={handleChecked(item)} onChange={(evt) => changeSwitch(evt, item)}></Switch>
                </Box>
              }
            >
              <ListItemAvatar>
                <Avatar>
                  <BusIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.payload.external_id}
                secondary={item.payload.label}
              />
            </ListItem>
            ))
          }
        </List>
        <Loading status={loading} />
        <Formulario status={modal} setStatus={setModal} success={change} setSuccess={setChange} loadin={loading} action={updateVehiculos} item={payload} data={dispositivos} rutas= {rutas}/>
      </Box>  </Paper>
  )
}

export {Vehiculos}