import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useVehiculos() {
    const { jwt } = React.useContext(Context)
    const [ vehiculos, setVehiculos ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const vehiculosCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'vehiculos',
    }
    
    async function getVehiculos(){
      setLoading(true)
      try{
        const response = await vehiculosCatalogo.consultarCatalogo(payload)
        setVehiculos(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updateVehiculos(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await vehiculosCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteVehiculos(id){
      setLoading(true)
      try{
        payload.id = id
        const response = await vehiculosCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)
      }
      setLoading(false)
      return false
    }
  
    return {
      vehiculos,
      loading,
      error,
      setLoading,
      setError,
      getVehiculos,
      updateVehiculos,
      deleteVehiculos
    }
}

export {useVehiculos}