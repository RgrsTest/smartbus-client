import { Backdrop, CircularProgress } from "@mui/material";
import React from "react";

function Loading( { status} ) {
    return (
        <Backdrop open={ status }>
            <CircularProgress />
        </Backdrop>
    )
}


export { Loading }