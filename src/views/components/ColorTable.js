import React from 'react';
import { Grid, Paper } from '@material-ui/core';

const ColorTable = ({ colors, onColorSelect }) => {
  return (
    <Grid container spacing={1}>
      {colors.map((color, index) => (
        <Grid key={index} item xs={3}>
          <Paper
            style={{
              backgroundColor: color,
              cursor: 'pointer',
              height: '50px',
            }}
            onClick={() => onColorSelect(color)}
          />
        </Grid>
      ))}
    </Grid>
  );
};

export default ColorTable;