import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useVersiones(catalogo) {
    const { jwt } = React.useContext(Context)
    const [ versiones, setVersiones ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const versionesCatalogo = Catalogos(jwt)
    let payloadConsultar = {
        "schema":"versiones",
        "filter":{
            "where":[{
                "field":"payload->catalogo",
                "operator":"=",
                "value":catalogo
            }]
    
        }
    }

    let payload = {
        "schema":"versiones"
    }
    
    async function getVersiones(){
      setLoading(true)
      try{
        const response = await versionesCatalogo.consultarCatalogo(payloadConsultar)
        setVersiones(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updateVersiones(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await versionesCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteVersiones(id){
      try{
        payload.id = id
        const response = await versionesCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          return true
        }
      }
      catch (e){
        setError(true)
      }
      return false
    }

    React.useEffect(getVersiones, [])
    return {
      versiones,
      loading,
      error,
      setLoading,
      setError,
      getVersiones,
      updateVersiones,
      deleteVersiones
    }
}

export {useVersiones}