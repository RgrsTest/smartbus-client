


function TrackingPanel({ rutas, fields, detailFields, excel }) {

    return (
        <Box sx={{marginTop:0, display: 'flex'}}>
          <Grow
            
            in={mode.trackingPanel}
            style={{ transformOrigin: '0 150 500' }}
            {...(mode.trackingPanel ? { timeout: 0 } : {})}
            sx={{ marginTop:1,marginLeft:2, zIndex:'modal'}}
          >
            <Paper sx={{ m: 0 ,background:'white', width: 150, height: 600}} elevation={4}>
          
            <Box sx={{width: 270, height: 400} }> 

            <Autocomplete 
            
            
            sx={{marginLeft: 3, width: 220,marginTop: 2,marginBottom: 2}}
        
            options={rutas.map((item) => item.payload.label)}
            value={rutaLabel}
            onChange={(e, newValue) => handleRuta(newValue)}
            renderInput={(params) => <TextField {...params} label='Ruta' variant="standard" /> }

          />
            
          
          <Autocomplete 
              sx={{marginLeft: 3, width: 220,marginTop: 2,marginBottom: 2}}
              options={vehiculos.map((item) => item.payload.label)}
              value={vehiculo}
              
              onChange={(e, newValue) => setVehiculo(newValue)}
              renderInput={(params) => <TextField {...params} variant="standard" label='Vehiculo' /> }
              />

          

          

          <LocalizationProvider dateAdapter={DateAdapter}>
                  <DatePicker label='Seleccione Fecha'  value={dateSearch} 
                  inputFormat="dd/MM/yyyy"
                  onChange={(newValue) => setDateSearch(newValue)}
                  renderInput={(params) => <TextField variant="standard" 
                  sx={{width: 220,marginLeft: 0,marginTop:1}} 
                  error={false} {...params} />} />

          </LocalizationProvider>
          

     

          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <TimePicker
              label="Hora Inicial"
              value={startTime}
              onChange={(newValue) => {
                setStartTime(newValue);
              }}
              renderInput={(params) => <TextField variant="standard" sx={{width: 220,marginLeft: 0,marginTop: 2}} {...params} />}
            />
          </LocalizationProvider>

          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <TimePicker
              label="Hora Final"
              value={finalTime}
              onChange={(newValue) => {
                setFinalTime(newValue);
              }}
              renderInput={(params) => <TextField variant="standard" sx={{width: 220,marginLeft: 0,marginTop: 2}} {...params} />}
            />
          </LocalizationProvider>

          <IconButton onClick={() => handleClickAction()} sx={{marginLeft: 1,marginTop: 2, ":hover": {
                    bgcolor: "#F2F4F4",
                    color: "black"
                  }}}  size='large' >
            <SearchIcon /> 
          </IconButton>
  


        </Box>
        
      </Paper>
      </Grow>
      </Box>
      )



}