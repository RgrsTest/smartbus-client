import { Box, Collapse, IconButton, Table, TableBody, TableCell, TableHead, TableRow, Typography,Slider } from "@mui/material";
import ArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import React, { useState } from "react";




import { RiFileExcel2Fill ,RiSearchLine } from "react-icons/ri";
import { IoSearchCircleSharp } from "react-icons/io5";
import { SlRefresh } from "react-icons/sl"

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";



import { registerLocale, setDefaultLocale } from  "react-datepicker";
import es from 'date-fns/locale/es';
registerLocale('es', es)


function setExcel(handleDownloadExcel)

    {
      
        return(

            <TableCell  align="left" size="small">
       
                <IconButton variant="solid"  onClick={handleDownloadExcel}>
                    <RiFileExcel2Fill />
                                                                    
                </IconButton>
            </TableCell>   
        )
        

    }
    



function DatePickerRow({startDate,setStartDate,finalDate,setFinalDate, handleDate, handleDownloadExcel,handleTracking}) {
    const [ open, setOpen ] = React.useState(false)
  
    return (
        
        <React.Fragment>
                           
                            <TableRow  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                             
                                

                                <TableCell align="left" size="small"  sx={{ mx: 'auto', width: 200 }}> 
                                 Fecha desde: 
                                <DatePicker  dateFormat="dd/MM/yy" selected={startDate} onChange={(date) => setStartDate(date)} />    
                                </TableCell>

                                <TableCell align="left" size="small"  sx={{ mx: 'auto', width: 200 }}> 
                                 Fecha hasta:                             
                                <DatePicker dateFormat="dd/MM/yy"  selected={finalDate} onChange={(date) => setFinalDate(date)} />
                                </TableCell>
                               
                                
                                <TableCell align="left" size="small">
                                    
                                    <IconButton variant="solid" onClick={handleDate}>
                                         <RiSearchLine />
                                        
                                    </IconButton>

                                </TableCell>

                                <TableCell  align="left" size="small">
       
                                    <IconButton variant="solid"  onClick={handleDownloadExcel}>
                                        <RiFileExcel2Fill />
                                                                                        
                                    </IconButton>
                                </TableCell>   
                              
                            </TableRow>
                          
                       
        </React.Fragment>
    )
}

export {DatePickerRow}