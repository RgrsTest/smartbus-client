import { Box, Collapse, IconButton, Table, TableBody, TableCell, TableHead, TableRow, Typography } from "@mui/material";
import ArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import React, { useState } from "react";


import { RiFileExcel2Fill } from "react-icons/ri";

import { ImLocation2 } from "react-icons/im";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

function CustomRow({ row, fields, detailFields, excel }) {
    const [ open, setOpen ] = React.useState(false)


    let url_maps="https://www.google.com/maps/search/?api=1&query=";
    let mapUrl;
    


    // Excel storing
    let vehiculoRow = {};
    vehiculoRow["unidad"] = "";
    

   

    





   row.details.forEach((item, index) => {

      //Do Excel
       let newRow = {};
                            


       let vehiculoLabel =  item.encabezado._relaciones.vehiculo.payload.label;
       vehiculoRow["unidad"] = vehiculoLabel;
       newRow["Unidad"] = vehiculoLabel;

     

       detailFields.forEach((campo, valor) => {

        const value = item.encabezado[campo.id]

                if(campo.id === 'latitud')
                {
                    mapUrl = url_maps;
                    mapUrl+= value;

                
                }
                else if( campo.id === 'longitud')
                {
                    mapUrl+= ',' + value;


                    newRow['Ubicacion'] = mapUrl;

                }
                else{

                    newRow[campo.id] =  campo.format && ( typeof value === 'number' || typeof value === 'string' ) ? campo.format(value) : value;
                }

         })

       
        excel.push(newRow);
        
        
    });


  
  

    const showInMapClicked = mapUrl => () => {
        console.log(mapUrl);
        window.open(mapUrl);
      };
    
    return (
        <React.Fragment>
            <Box sx={{display:'flex'}}>
                <TableCell>
                    <IconButton onClick={() => setOpen(!open)}>
                        { open ? <ArrowUpIcon /> : <ArrowDownIcon /> }
                    </IconButton>
                </TableCell>

               
               
                {fields.map((element, index) => {
                    const value = row[element.id]
                    return (
                    <TableCell sx={{marginLeft:5,  minWidth:180}} key={index} >
                        { element.format && typeof value === 'number'
                      ? element.format(value)
                      :value
                      }
                    </TableCell>
                    
                )})}

                        
               
                </Box>
                <TableRow >
                <Box >
                    <Collapse in={open} timeout='auto' unmountOnExit>
                        <Box sx={{m:0, display:'flex',width:'96vw',marginLeft:2}}>

                      
                              
                        <Table >
                                <TableHead>
                                    <TableRow >
                                        {detailFields.map((item, index) => (
                                            <TableCell  key={index} >{item.label}</TableCell>
                                        ))}
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {row.details.map((item, index) => (
                                        
                                      

                                        <TableRow key={index} >
                                            
                                            {detailFields.map((campo, valor) => {
                                                const value = item.encabezado[campo.id]

                                                if(campo.id === 'latitud')
                                                {
                                                    mapUrl = url_maps;
                                                    mapUrl+= value;

                                                   
                                                }
                                                else if( campo.id === 'longitud')
                                                {
                                                    mapUrl+= ',' + value;

                                                    return( 
                                                        <TableCell key={valor}>
                                                        <IconButton   onClick={showInMapClicked(mapUrl)}>
                                                                    <ImLocation2 />
                                                                                                                    
                                                        </IconButton>
                                                          </TableCell>
                                                     )
                                                  
                                                }
                                                else{

                                                return (
                                                <TableCell key={valor} > 
                                                {campo.format && ( typeof value === 'number' || typeof value === 'string' )
                                                ? campo.format(value)
                                                : value
                                                }
                                                </TableCell>
                                                )
                                                }
                                            })}


                                           





                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                           
                          
                       
                        </Box>
                    </Collapse>
                </Box>
            </TableRow>
        </React.Fragment>
    )
}

export {CustomRow}