import React from "react";
import { Catalogos } from "../../Api";
import { Context } from "../../Context";

function useRutas() {
    const { jwt } = React.useContext(Context)
    const [ rutas, setRutas ] = React.useState([])
    const [ loading, setLoading ] = React.useState(false)
    const [ error, setError ] = React.useState(false)
    const rutasCatalogo = Catalogos(jwt)
    let payload = {
      schema: 'rutas',
    }
    
    async function getRutas(){
      setLoading(true)
      try{
        const response = await rutasCatalogo.consultarCatalogo(payload)
        setRutas(response.data)
      }
      catch (e) {
        setError(true)
      }
      setLoading(false)
    }
  
    async function updateRutas(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await rutasCatalogo.actualizarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }

    async function remplazarRutas(data, id= null){
      setLoading(true)
      try{
        if (id) payload.id = id
        payload.payload = data
        const response = await rutasCatalogo.remplazarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)  
      }
      setLoading(false)
      return false
    }
  
    async function deleteRutas(id){
      setLoading(true)
      try{
        payload.id = id
        const response = await rutasCatalogo.eliminarCatalogo(payload)
        if (response.status === 'success'){
          setLoading(false)
          return true
        }
      }
      catch (e){
        setError(true)
      }
      setLoading(false)
      return false
    }


    React.useEffect(getRutas, [])
    return {
      rutas,
      loading,
      error,
      setLoading,
      setError,
      getRutas,
      updateRutas,
      deleteRutas,
      remplazarRutas
    }
}

export {useRutas}