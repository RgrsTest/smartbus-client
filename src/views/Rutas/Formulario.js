import { Box, Button, Card, CardActionArea, TextareaAutosize,CardActions, CardContent, CardHeader, FormControlLabel, FormGroup, Modal, TextField, Checkbox, FormControl, FormLabel, IconButton, Select, MenuItem, List, ListItem } from "@mui/material";

import React from "react";
import { Loading } from "../components/Loading";

import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import InputColor from 'react-input-color';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function Formulario( { status, setStatus, loading, success, setSuccess, action, item = null } ){
    const [ externalId,  setExternalId] = React.useState('')
    const [ label, setLabel ] = React.useState('')
    const [ longitudVuelta, setLongitudVuelta ] = React.useState('')
    const [ errorExternalId, setErrorExternalId ] = React.useState(false)
    const [ errorLabel, setErrorLabel ] = React.useState(false)
    const [ errorLegendExternalId, setErrorLegendExternalId ] = React.useState('')
    const [ errorLegendLabel, setErrorLegendLabel ] = React.useState('')
    const [ errorLongitudVuelta, setErrorLongitudVuelta] = React.useState(false)
    const [ errorLegendLongitudVuelta, setErrorLegendLongitudVuelta ] = React.useState('')
    const [ title, setTitle ] = React.useState('Crear nueva ruta')
    const [ button, setButton ] = React.useState('Agregar')

    const [ estaciones, setEstaciones ] = React.useState([])
    const [ estacion, setEstacion ] = React.useState('')
    const [ localModal, setLocalModal ] = React.useState(false)

    const [ edit, setEdit ] = React.useState(null)


    const [ latitud, setLatitud ] = React.useState('')
    const [ longitud, setLongitud] = React.useState('')
    const [currentColor, setCurrentColor] = React.useState('#0000FF');

    const [ recorrido, setRecorrido ] = React.useState([])
    const [ recorridoText, setRecorridoText ] = React.useState('')

    const [ options, setOptions ] = React.useState({
        strokeColor:currentColor,
        strokeOpacity: 0.6,
        strokeWeight: 6,
        fillColor: '#0000FF',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: false,
        radius: 30000
             
      })


   
      React.useEffect(() => {

    
        const updatedOptions = { ...options, strokeColor: currentColor };

         setOptions(updatedOptions);

       
      },[currentColor])





      

    const handlePayload = () =>{
        if (item){
            setButton('Guardar')
            setTitle('Editar ruta')

            item.payload.external_id && setExternalId(item.payload.external_id)
            item.payload.label && setLabel(item.payload.label)
            item.payload.longitud_vuelta && setLongitudVuelta(item.payload.longitud_vuelta) 
            item.payload.estaciones && setEstaciones(item.payload.estaciones) 
            item.payload.recorrido && setRecorrido(item.payload.recorrido) 
            item.payload.options && setOptions(item.payload.options)

            console.log(item.payload.options)


            


            let recorridotxt = '';
            item.payload.recorrido.forEach((location) => {


                recorridotxt += location.longitud;
                recorridotxt += ",";
                recorridotxt += location.latitud;
                recorridotxt += "\n";
               
              
               })
          
            setRecorridoText(recorridotxt);

            console.log(recorridotxt);


        }
        else{
            setButton('Agregar')
            setTitle('Crear nueva ruta')
            setExternalId('')
            setLabel('')
            setLongitudVuelta('')
        }
    }

    React.useEffect(handlePayload, [item])

    const closeModal = () => {
        setExternalId('')
        setLabel('')
        setLongitudVuelta('')
        setErrorExternalId(false)
        setErrorLabel(false)
        setErrorLongitudVuelta(false)
        setErrorLegendExternalId('')
        setErrorLegendLabel('')
        setErrorLegendLongitudVuelta('')
        setStatus(false)
    }

    const handleLabel = () => {
        if ( errorLabel ) {
            setErrorLabel(false)
            setErrorLegendLabel('')
        }
    }

    const handleLongitudVuelta = () => {
        if (errorLongitudVuelta) {
            setErrorLongitudVuelta(false)
            setErrorLegendLongitudVuelta('')
        }
    }

    const handleExternalId = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && externalId.length >= 4 ){
            event.preventDefault()
        }
        else {
            if ( errorExternalId ) {
                setErrorLegendExternalId('')
            }
        }
    }

    const handleUpdateRutas = async () => {
        let ok = true
        if ( !externalId ) {
            setErrorExternalId(true)
            setErrorLegendExternalId('Este campo es obligatorio')
            ok = false
        }
        else if ( externalId.length < 4 ) {
            setErrorExternalId(true)
            setErrorLegendExternalId('External ID debe ser un numero hexadecimal de 4 digitos')
            ok = false
        }
        if ( !label ) {
            setErrorLabel(true)
            setErrorLegendLabel('Este campo es obligatorio')
            ok = false
        }
        if ( !longitudVuelta ) {
            setErrorLongitudVuelta(true)
            setErrorLegendLongitudVuelta('Este campo es obligatorio')
            ok = false
        }
        if (ok) {
            //setStatus(false)
            let send = null
            if (item) {
                send =  await action({ external_id: externalId.toLowerCase(), label: label.toUpperCase(),estaciones: estaciones,longitud_vuelta: parseFloat(longitudVuelta), recorrido:recorrido,  options:options}, item.id)
            }
            else {
                send =  await action({ external_id: externalId.toLowerCase(), label: label.toUpperCase(),estaciones: estaciones ,longitud_vuelta: parseFloat(longitudVuelta),recorrido:recorrido,  options:options})
            }

            if (send){
                setExternalId('')
                setLabel('')
                setLongitudVuelta('')
                setStatus(false)
                setSuccess(!success)
            }
        }
    }

    const handleRemoveEstacion = (valor) => {
        let newObjeto = estaciones.filter((find) => find !== valor)
        setEstaciones(newObjeto)
    } 

    const handleColorSelect = (color) => {
        if(color.hex !== undefined){
          setCurrentColor(color.hex);
       console.log('Color seleccionado:', color.hex);
      

      
   
        }
       
      };
    
    const handleCloseEstacion = () => {
      
        setLocalModal(false)
    }

    const handleAgregarEstacion = () => {
        

            let objeto = estaciones
             
            objeto.push({
                estacion: estacion,
                latitud: latitud,
                longitud: longitud
            })
            
            setEstaciones(objeto)
      
            handleCloseEstacion()
        
    }

    const handleUploadPath = (text) => {

        let path = [];
       

        var lines = text.split('\n');
            for(var i = 0;i < lines.length;i++){
                //code here using lines[i] which will give you each line
                var coord = lines[i].split



                var coords = lines[i].split(',');

               

                let lng = coords[0];

                let lat = coords[1];
                path.push({                  
                    latitud: parseFloat(lat),
                    longitud: parseFloat(lng)
                })
                

            }
            setRecorrido(path);
            console.log(recorrido);




    }

    const handleUpdateEstacion = () => {

        console.log("estaciones=" + estaciones);
        let objeto = estaciones

        if(edit)
        {
            let estacionIndex = objeto.findIndex((obj => obj == edit));


            console.log("estacionIndex=" + estacionIndex);

            console.log("estacion=" + estacion);

            console.log("latitud=" + latitud);

            console.log("longitud=" + longitud);

            
            objeto[estacionIndex].estacion = estacion;

            objeto[estacionIndex].latitud = parseFloat(latitud);
     
            objeto[estacionIndex].longitud = parseFloat(longitud);

           
        }

        else{

          
             
            objeto.push({
                estacion: estacion,
                latitud: latitud,
                longitud: longitud
            })
            
        }

    

        setEstaciones(objeto)

        console.log("estaciones=" + estaciones);

        handleCloseEstacion()

        
    }



    
    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                        <TextField required error={errorExternalId} helperText={errorLegendExternalId} onKeyPress={(e) => handleExternalId(e)} sx={{m: 2, width: 400}} label='External ID' value={ externalId } onChange={ (e) => setExternalId(e.target.value) } />
                        <TextField required error={errorLabel} helperText={errorLegendLabel} onKeyPress={handleLabel} sx={{m: 2, width: 400}} label='Label' value={ label } onChange={ (e) => setLabel(e.target.value) } />
                        <TextField required error={errorLongitudVuelta} helperText={errorLegendLongitudVuelta} onKeyPress={handleLongitudVuelta} sx={{m: 2, width: 400}} label='Longitud de Vuelta' type='number' value={ longitudVuelta } onChange={ (e) => {setLongitudVuelta(e.target.value)} } />
                        <Box  sx={{  display: 'inline-flex', ml:0 , mt:1,mb:1}}> Color </Box> 
                        <Box  sx={{ ml:2,mb:1}} >
                       
                        <InputColor
                         
                        initialValue={options.strokeColor}
                        onChange={handleColorSelect}
                        placement="left"
                        />
                        </Box>
                        <FormControl>
                            <FormGroup>
                                <FormLabel>Estaciones</FormLabel>
                                <List   sx={{width:400, maxHeight: 150, overflow: 'auto'}}>
                                    {estaciones.map((item, index) => (
                                        <ListItem
                                        key={index}
                                        secondaryAction={
                                          <Box>
                                            <IconButton edge="end" aria-label="edit" onClick={() => {setLocalModal(!localModal); setEstacion(item.estacion); setLatitud(item.latitud); setLongitud(item.longitud); setEdit(item)}}>
                                                <EditIcon />
                                            </IconButton>

                                            <IconButton edge="end" aria-label="delete" onClick={() => handleRemoveEstacion(item)}>
                                                <DeleteIcon />
                                            </IconButton>
                                          </Box>
                                        }
                                      >
                                          {item.estacion} 
                                         
                                      </ListItem>
                                    ))}
                                </List>
                                <IconButton onClick={() => {setLocalModal(!localModal); setEdit(null)}}>
                                    <AddIcon />
                                </IconButton>
                            </FormGroup>
                        </FormControl>

                        <TextareaAutosize
                            aria-label="empty textarea"
                            placeholder="Puntos de recorrido"
                            defaultValue={recorridoText}
                            style={{ width: 400, height: 200 }}
                            onChange={(event) => {
                               handleUploadPath(event.target.value)}}
                            />
                    
                    
                    
                    
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleUpdateRutas()}>
                            {button}
                        </Button>
                        <Button onClick={() => closeModal()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>

                <Modal open={localModal}>
                    <Card style={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: 500,
                        bgcolor: 'background.paper',
                        boxShadow: 24,
                        p: 4
                      }
                    }>
                        <CardHeader component="div"  title='Agregar Estacion' />
                        <CardContent>                            
                            <TextField value={ estacion } required onChange={(e) => setEstacion(e.target.value)} type='string'  sx={{m: 1, width: 420}} label='Estacion' />
                            <TextField value={ latitud }  required onChange={(e) => setLatitud(e.target.value)} type='string' sx={{m: 1, width: 200}} label='Latitud' />
                            <TextField value={ longitud } required onChange={(e) => setLongitud(e.target.value)} type='string'  sx={{m: 1, width: 200}} label='Longitud' />
                        </CardContent>
                        <CardActions>
                            <Button onClick={() => handleUpdateEstacion()} >
                                Aceptar
                            </Button>
                            <Button onClick={() => handleCloseEstacion()}>
                                Cancelar
                            </Button>
                        </CardActions>
                    </Card>
                </Modal>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}