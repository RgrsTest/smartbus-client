import { Button, Card, CardActions, CardContent, CardHeader, Modal } from "@mui/material";
import React from "react";
import { Loading } from "./Loading";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function ErrorModal({ status, setStatus }) {
    return (
        <Modal open={status} style={style}>
            <Card>
                <CardHeader title='Error inesperado' />
                <CardContent>
                    Ha ocurrido un error, intentelo mas tarde.
                </CardContent>
                <CardActions>
                    <Button onClick={() => setStatus(false)}>
                        Aceptar
                    </Button>
                </CardActions>
            </Card>
        </Modal>
    )
}

export { ErrorModal }