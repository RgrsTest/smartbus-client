import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import RouteIcon from '@mui/icons-material/Route';
import { Avatar, Box, IconButton, List, ListItem, ListItemAvatar, ListItemText, TextField, Button,Paper } from "@mui/material";
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import { useRutas } from "./useRutas";
import { Loading } from "../components/Loading";
import { DeleteModal } from "./DeleteModal";

const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Rutas(){
  const [ change, setChange ] = React.useState(false)
  const { rutas, loading, updateRutas, deleteRutas, getRutas, remplazarRutas } = useRutas()
  const [ modal, setModal ] = React.useState(false)
  const [ search, setSearch ] = React.useState('')
  const [ localRutas, setLocalRutas ] = React.useState([])
  const [ payload, setPayload ] = React.useState(null)
  const [ deleteModal, setDeleteModal ] = React.useState(false)
  
  React.useEffect(() =>{
    
    if(search.length > 0){
      let searching = search.toLocaleLowerCase()
      let valores = rutas.filter((item) => {
        const itemText = item.payload.label.toLowerCase()
        return itemText.includes(searching)
      })
        setLocalRutas(valores)
    }else{
      setLocalRutas(rutas)
    }
  },[search, rutas])

  React.useEffect(getRutas, [change])
  return (
    <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Rutas</Box>
      <Box
      sx={{width:"95%", marginTop:1, marginLeft:4}}
      >
        <Container >
          <TextField sx={{width: '50%', mr: '10%'}}
          placeholder="Escriba para buscar"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          />
          <Button sx={{width: '40%', height: 50}} onClick={() => {setModal(true); setPayload(null)}} variant='contained' >
            Agregar
          </Button>
        
        </Container> 
        <List >
          {localRutas.map((item, index)=>(
            <ListItem
            key={index}
              secondaryAction={
                  <Box>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setModal(true); setPayload(item)}}>
                        <EditIcon />
                    </IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={() => {setDeleteModal(true); setPayload(item)}} >
                        <DeleteIcon />
                    </IconButton>
                  </Box>
              }
            >
              <ListItemAvatar>
                <Avatar>
                  <RouteIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.payload.label}
                secondary={item.payload.external_id}
              />
            </ListItem>
          ))}
        </List>
        <DeleteModal status={ deleteModal } setStatus={ setDeleteModal } success={ change } setSucess={ setChange } item={ payload } action={ deleteRutas } loading={loading} />
        <Loading status={loading}></Loading>
        <Formulario status={modal} loading={loading} success={change} setSuccess={setChange} setStatus={setModal} action={updateRutas} item={payload}></Formulario>
      </Box> </Paper>
  )
}

export {Rutas}