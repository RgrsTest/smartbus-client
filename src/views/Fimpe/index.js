import React from "react";
import {Autocomplete,Box,Paper,TextField, Table, TableBody, TableCell, TableContainer, TableHead, TableRow,Typography,IconButton } from "@mui/material";
import { useFimpe } from "./useFimpe";
import { Loading } from "../components/Loading";
import { CustomRow } from "../components/CustomRow";
import { DatePickerRow } from "../components/DatePickerRow";
import { useDownloadExcel,downloadExcel } from 'react-export-table-to-excel';
import Pagination from '@mui/material/Pagination';

import { useValidaciones } from "../Validaciones/useValidaciones";
import { useRecargas } from "../Recargas/useRecargas";
import { useVentas } from "../Ventas/useVentas";


import { RiFileExcel2Fill } from "react-icons/ri";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import XMLViewer from 'react-xml-viewer';
import { useVehiculos } from "../Vehiculos/useVehiculos";



function convertTransactionToXML(transaction)
{

}


function Fimpe(){

  const [ fromDate, setFromDate] = React.useState(new Date())
  const [ toDate, setToDate] = React.useState(new Date())

  const [ startDate, setStartDate] = React.useState(new Date())
  const [ finalDate, setFinalDate] = React.useState(new Date())

  const { vehiculos, getVehiculos } = useVehiculos()
  const [ vehiculo, setVehiculo ] = React.useState(null)

  let [page, setPage]  = React.useState(1);
  const [ xml, setXML] = React.useState([])

  const {validaciones} = useValidaciones(fromDate,toDate)
  const {recargas} = useRecargas(fromDate,toDate)
  const {ventas } = useVentas(fromDate,toDate)


  const [ docType, setDocType ] = React.useState(null)


  const fileType = ['Debito','Recarga','Efectivo','Contadores']

  //let xml = '<hello>World</hello>';
  React.useEffect(getVehiculos, [])

  const toXML = (item) => {
     return    "<archivo tipo=\"debito\">"   +  "<debito>" +

       "<C1>" + item.folio_transaccion + "</C1>"  +
       "<C2>" + item.fecha_hora + "</C2>"  +
       "<C3>" + item.uid + "</C3>"  +
       "<C4>" + item.id_producto + "</C4>"  +
       "<C5>" + item.monto + "</C5>"  +
       "<C6>" + item.saldo_inicial + "</C6>"  +
       "<C7>" + item.saldo_final + "</C7>"  +
       "<C8>" + item.id_dispositivo + "</C8>"  +
       "<C9>" + item.id_sam + "</C9>"  + 
       "<C10>" + item.consecutivo_sam + "</C10>"  + 
       "<C11>" + item.consecutivo_aplicacion + "</C11>"  + 
       "<C12>" + item.latitud + "</C12>"  + 
       "<C13>" + item.longitud + "</C13>"  + 
       "<C14>" + item.tipo_debito + "</C14>"  +
        "</debito>" + "</archivo>" ;
   
    
  }

  const toXML_recarga = (item) => {
    return    "<archivo tipo=\"recarga\">"   +  "<recarga>" +

     
      "<C1>" + item.uid + "</C1>"  +
      "<C2>" + item.id_producto + "</C2>"  +
      "<C3>" + item.monto + "</C3>"  +
      "<C4>" + item.saldo_inicial + "</C4>"  +
      "<C5>" + item.saldo_final + "</C5>"  +
       "</recarga>" + "</archivo>" ;
  
   
 }

 const toXML_efectivo = (item) => {
  return    "<archivo tipo=\"efectivo\">"   +  "<efectivo>" +

   
    "<C1>" + item.fecha_hora + "</C1>"  +
    "<C2>" + item.monto + "</C2>"  +
    "<C3>" + item.latitud + "</C3>"  +
    "<C4>" + item.longitud + "</C4>"  +
     "</efectivo>" + "</archivo>" ;

 
}

const toXML_contadores = (item) => {
  return    "<archivo tipo=\"contador\">"   +  "<contador>" +

   
    "<C1>" + item.fecha_hora + "</C1>"  +
    "<C2>" + 6 + "</C2>"  +
    "<C3>" + 8 + "</C3>"  +
    "<C4>" + item.latitud + "</C4>"  +
    "<C5>" + item.longitud + "</C5>"  +
     "</contador>" + "</contador>" ;

 
}
 

  const handleDate = () => {
    setFromDate(startDate);
    setToDate(finalDate);

    console.log("choose: " + docType);

    setXML([])

    let vehiculoId = vehiculos.find((element) => element.payload.label === vehiculo);



    if(docType == 'Debito')
    {


        validaciones.forEach(item =>     
          {
          /* if(item.payload.encabezado._relaciones.vehiculo.payload.label === vehiculo)
            {*/
              console.log(item.payload.encabezado)
              //setXML([...xml,toXML(item.payload.encabezado)])

              let newXML = toXML(item.payload.encabezado)
              setXML(xml => [...xml, newXML])
          // }
          }
        
          )


          console.log(xml)
    }

    else if(docType == 'Recarga')
    {
      recargas.forEach(item =>     
        {
        /* if(item.payload.encabezado._relaciones.vehiculo.payload.label === vehiculo)
          {*/
            console.log(item.payload.encabezado)
            //setXML([...xml,toXML(item.payload.encabezado)])

            let newXML = toXML_recarga(item.payload.encabezado)
            setXML(xml => [...xml, newXML])
        // }
        }
      
        )


        console.log(xml)

    }

    else if(docType == 'Efectivo')
    {
      ventas.forEach(item =>     
        {
        /* if(item.payload.encabezado._relaciones.vehiculo.payload.label === vehiculo)
          {*/
            console.log(item.payload.encabezado)
            //setXML([...xml,toXML(item.payload.encabezado)])

            let newXML = toXML_efectivo(item.payload.encabezado)
            setXML(xml => [...xml, newXML])
        // }
        }
      
        )
    }

    else if(docType == 'Contadores')
    {
      validaciones.forEach(item =>     
        {
        /* if(item.payload.encabezado._relaciones.vehiculo.payload.label === vehiculo)
          {*/
            console.log(item.payload.encabezado)
            //setXML([...xml,toXML(item.payload.encabezado)])

            let newXML = toXML_contadores(item.payload.encabezado)
            setXML(xml => [...xml, newXML])
        // }
        }
      
        )
    }

    
  }

  const handleChange = (e, p) => {
    setPage(p);
   
  };
  

  return (
    <Paper elevation={0}  sx={{width:"95%", marginTop:10, marginLeft:4}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>FIMPE</Box>
    
        <TableBody>
        <TableCell>
        <Autocomplete 
        sx={{width: 200, marginRight: 2 }}
        options={vehiculos.map((item) => item.payload.label)}
        value={vehiculo}
        onChange={(e, newValue) => setVehiculo(newValue)}
        renderInput={(params) => <TextField {...params} label='Vehiculo' /> }
        />

        <Autocomplete 
        sx={{width: 200, marginRight: 2, marginTop: 2 }}
        options={fileType}
        onChange={(e, newValue) => setDocType(newValue)}
        renderInput={(params) => <TextField {...params} label='Tipo' /> }
        />  
       </TableCell>
       <TableCell  sx={{ mx: 'auto', width: 1000 }}>
        <DatePickerRow startDate = {startDate} 
         setStartDate = {setStartDate}  
         finalDate={finalDate} 
         setFinalDate = {setFinalDate}  
         handleDate = {handleDate} 
         >


        </DatePickerRow>
        </TableCell>
        </TableBody>

        <TableBody>
         

          <Pagination
            sx={{marginTop: 2 }}
            count={xml.length}
            size="large"
            page={page}
            variant="outlined"
            shape="rounded"
            onChange={handleChange}
          />
        </TableBody>


        <div>
        <XMLViewer xml={xml[page]} />
         </div>








    </Paper>
  )
}

export {Fimpe}