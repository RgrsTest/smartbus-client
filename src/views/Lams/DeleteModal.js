import { Box, Button, Card, CardActions, CardContent, CardHeader, Modal, Typography } from "@mui/material";
import React from "react";
import { Loading } from "../components/Loading";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function DeleteModal( { status, setStatus, success, setSucess, action, loading,  item=null}) {

    const hanldeDeleteDispositivos = async () => {
        const response = await action(item.id)
        if( response ){
            setSucess(!success)
            setStatus(false)
        }
    }

    return (
        <Modal open={ status } >
            <Box style={ style }>
                <Loading status={loading}></Loading>
                <Card>
                    <CardHeader title="Eliminar Accion" />
                    <CardContent>
                        <Typography>
                            ¿Esta seguro que desea eliminar la accion en { item && item.payload.uid }?
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => hanldeDeleteDispositivos()} >
                            Si
                        </Button>
                        <Button onClick={() => setStatus(false)}>
                            No
                        </Button>
                    </CardActions>
                </Card>
            </Box>
        </Modal>
    )
}

export { DeleteModal }