import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardHeader, InputBase, Modal, TextField } from "@mui/material";
import React from "react";
import { Loading } from "../components/Loading";
import dayjs from 'dayjs';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function Formulario( { status, setStatus, loading, success, setSuccess, action , item} ){
    const [ uid, setUid ] = React.useState('');
    const [ errorUid, setErrorUid ] = React.useState(false);
    const [ consecutivo, setConsecutivo ] = React.useState('');
    const [ errorConsecutivo, setErrorConsecutivo ] = React.useState(false);
    const [ accion, setAccion ] = React.useState(false);
    const [ errorAccion, setErrorAccion ] = React.useState(false);
    const [ errorLegendUid, setErrorLegendUid ] = React.useState('');
    const [ errorLegendConsecutivo, setErrorLegendConsecutivo ] = React.useState('');
    const [ errorLegendAccion, setErrorLegendAccion ] = React.useState('');
    const [ title, setTitle ] = React.useState('Crear Nueva Accion en LAM');
    const [ button, setButton ] = React.useState('Agregar');






    const handlePayload = () =>{
        if (item){
            setButton('Guardar')
            setTitle('Editar Accion')

            item.payload.uid && setUid(item.payload.uid)
            item.payload.codigo_accion && setAccion(item.payload.codigo_accion)
            item.payload.numero_accion_aplicada && setConsecutivo(item.payload.numero_accion_aplicada)
        }
        else{
            setButton('Agregar')
            setTitle('Crear Nueva Accion en LAM')
            setUid('')
            setAccion('')
            setConsecutivo('')
        }
    }

    React.useEffect(handlePayload, [item,status])


    
    const closeModal = () => {
     
        setUid('');
        setErrorUid(false);
        setConsecutivo('');
        setErrorConsecutivo(false);
        setAccion('');
        setErrorAccion(false);
        setErrorLegendUid('');
        setErrorLegendConsecutivo('');
        setErrorLegendAccion('');
        setStatus(false)
    }

    const handleUid = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && uid.length >= 16 ){
            event.preventDefault()
        }
        else {
            if ( errorUid ) {
                setErrorLegendUid('');
            }
        }
    }

    const handleUpdateLams = async () => {
        let ok = true
        if ( !uid ) {
            setErrorUid(true)
            setErrorLegendUid('Este campo es obligatorio')
            ok = false
        }
        else if ( uid.length < 14 ) {
            setUid(true)
            setUid('External ID debe ser un numero hexadecimal de 16 digitos')
            ok = false
        }
        if ( !consecutivo ) {
            setErrorConsecutivo(true)
            setErrorConsecutivo('Este campo es obligatorio')
            ok = false
        }
        if ( !accion ) {
            setErrorAccion(true)
            setErrorLegendAccion('Este campo es obligatorio')
            ok = false
        }
        if (ok) {
            //setStatus(false)
            let send = null
        
            if (item) {
                send =await action({ uid: uid.toLowerCase(), numero_accion_aplicada: parseInt(consecutivo), fecha_accion_aplicada:"", sam:"",codigo_accion: parseInt(accion) , fecha_accion_registro: dayjs().format('YYYY-MM-DD HH:mm:ss'), estatus:0}, item.id)
            }
            else {
                send =  await action({ uid: uid.toLowerCase(), numero_accion_aplicada: parseInt(consecutivo), codigo_accion: parseInt(accion), fecha_accion_aplicada:"", sam:"",fecha_accion_registro:  dayjs().format('YYYY-MM-DD HH:mm:ss'), estatus: 0})
            }

            if (send){
                closeModal();
                setSuccess(!success);
            }
        }
    }

    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                        <TextField required error={errorUid} helperText={errorLegendUid} onKeyPress={(e) => handleUid(e)} sx={{m: 2, width: 400}} label='UID' value={ uid } onChange={ (e) => setUid(e.target.value) } />
                        <TextField type='number' required error={errorAccion} helperText={errorAccion} sx={{m: 2, width: 400}} label='Accion' value={ accion } onChange={ (e) => {setAccion(e.target.value)} } />
                        <TextField type='number' required error={errorAccion} helperText={errorLegendConsecutivo}  sx={{m: 2, width: 400}} label='Numero Accion Aplicada' value={ consecutivo } onChange={ (e) => setConsecutivo(e.target.value) } />
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleUpdateLams()}>
                            {button}
                        </Button>
                        <Button onClick={() => closeModal()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}