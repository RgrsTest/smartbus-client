import React from 'react';
import { Context } from '../../Context';
import { Catalogos } from '../../Api';

function useLams(){
    const { jwt } = React.useContext(Context);
    const [ lams, setLams ] = React.useState([]);
    const [ error, setError ] = React.useState(false);
    const [ loading, setLoading ] = React.useState(false);
    const lamsCatalogos = Catalogos(jwt);
    var payload = {
        schema: 'lam'
    };

    async function getLams(){
        setLoading(true);
        try{
            const response = await lamsCatalogos.consultarCatalogo(payload);
            if (response.status === 'success'){
                setLams(response.data);
                setLoading(false);
            }
        }
        catch {
            setError(true);
        }
        setLoading(false);
    }

    async function updateLams(data, id=null){
       

        setLoading(true)
        try{
          if (id) payload.id = id
          payload.payload = data
          const response = await lamsCatalogos.actualizarCatalogo(payload)
          if (response.status === 'success'){
            setLoading(false)
            return true
          }
        }
        catch (e){
          setError(true)  
        }
        setLoading(false)
        return false
    }

    async function deleteLams(id){
        setLoading(true)
        try{
          payload.id = id
          const response = await lamsCatalogos.eliminarCatalogo(payload)
          if (response.status === 'success'){
            setLoading(false)
            return true
          }
        }
        catch (e){
          setError(true)
        }
        setLoading(false)
        return false
      }

    return {
        lams,
        loading,
        error,
        getLams,
        updateLams,
        deleteLams
    }
}

export {useLams}