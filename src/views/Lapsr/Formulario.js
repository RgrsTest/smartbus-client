import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardHeader, InputBase, Modal, TextField } from "@mui/material";
import React from "react";
import { Loading } from "../components/Loading";
import dayjs from 'dayjs';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4
  };

function Formulario( { status, setStatus, loading, success, setSuccess, action, item } ){
    const [ uid, setUid ] = React.useState('');
    const [ errorUid, setErrorUid ] = React.useState(false);
    const [ numeroAccionAplicada, setNumeroAccionAplicada ] = React.useState('');
    const [ errorNumeroAccionAplicada, setErrorNumeroAccionAplicada ] = React.useState(false);
    const [ monto, setMonto ] = React.useState(false);
    const [ errorMonto, setErrorMonto ] = React.useState(false);
    const [ errorLegendUid, setErrorLegendUid ] = React.useState('');
    const [ errorLegendNumeroAccionAplicada, setErrorLegendNumeroAccionAplicada ] = React.useState('');
    const [ errorLegendMonto, setErrorLegendMonto ] = React.useState('');
    const [ title, setTitle ] = React.useState('Crear nueva ruta');
    const [ button, setButton ] = React.useState('Agregar');

    const [ idProducto, setIdProducto ] = React.useState(false);

    
    const handlePayload = () =>{
        if (item){
            setButton('Guardar')
            setTitle('Editar Recarga Remota')

            item.payload.uid && setUid(item.payload.uid)
            item.payload.monto && setMonto(item.payload.monto)
            item.payload.numeroAccionAplicada && setNumeroAccionAplicada(item.payload.numeroAccionAplicada)
            item.payload.idProducto && setIdProducto(item.payload.idProducto)
        }
        else{
            setButton('Agregar')
            setTitle('Crear Nueva Recarga Remota')
            setUid('')
            setMonto('')
            setIdProducto('')
            setNumeroAccionAplicada('')
        }
    }

    React.useEffect(handlePayload, [item,status])


    const closeModal = () => {
        setUid('');
        setErrorUid(false);
        setNumeroAccionAplicada('');
        setErrorNumeroAccionAplicada(false);
        setMonto('');
        setErrorMonto(false);
        setErrorLegendUid('');
        setErrorLegendNumeroAccionAplicada('');
        setErrorLegendMonto('');
        setStatus(false);
    }

    const handleUid = (event) => {
        if ((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)){
            event.preventDefault()
        }
        else if (!((event.charCode < 48 || event.charCode > 57) &&  (event.charCode < 65  || event.charCode > 70) && (event.charCode < 97  || event.charCode > 102)) && uid.length >= 14 ){
            event.preventDefault()
        }
        else {
            if ( errorUid ) {
                setErrorLegendUid('');
            }
        }
    }

    const handleUpdateLams = async () => {
        let ok = true
        if ( !uid ) {
            setErrorUid(true)
            setErrorLegendUid('Este campo es obligatorio')
            ok = false
        }
        else if ( uid.length < 14 ) {
            setUid(true)
            setUid('External ID debe ser un numero hexadecimal de 16 digitos')
            ok = false
        }
        if ( !numeroAccionAplicada ) {
            setErrorNumeroAccionAplicada(true)
            setErrorNumeroAccionAplicada('Este campo es obligatorio')
            ok = false
        }
        if ( !monto ) {
            setErrorMonto(true)
            setErrorLegendMonto('Este campo es obligatorio')
            ok = false
        }
        if (ok) {
            //setStatus(false)
            let send = null
           

            if (item) {
                send =  await action({ uid: uid.toLowerCase(), 
                    numeroAccionAplicada: parseInt(numeroAccionAplicada),
                    fecha_accion_aplicada:"", 
                    sam:"", 
                    idProducto: idProducto, 
                    monto: parseFloat(monto),
                    fecha_accion_registro: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                    estatus:0}, item.id)
            }
            else {
                send =  await action({ uid: uid.toLowerCase(), 
                    numeroAccionAplicada: parseInt(numeroAccionAplicada),
                    fecha_accion_aplicada:"",
                    sam:"", 
                    idProducto: idProducto,
                    monto: parseFloat(monto),
                    fecha_accion_registro: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                    estatus:0})
            }

            if (send){
                closeModal();
                setSuccess(!success);
            }
        }
    }

    return (
        <Modal open={status}>
            <Box style={style}>
                <Card>
                    <CardHeader component='div' title={title} />
                    <CardContent>
                        <TextField required error={errorUid} helperText={errorLegendUid} onKeyPress={(e) => handleUid(e)} sx={{m: 2, width: 400}} label='UID' value={ uid } onChange={ (e) => setUid(e.target.value) } />
                        <TextField type='number' required error={errorNumeroAccionAplicada} helperText={errorLegendNumeroAccionAplicada}  sx={{m: 2, width: 400}} label='Numero Accion Aplicada' value={ numeroAccionAplicada } onChange={ (e) => setNumeroAccionAplicada(e.target.value) } />
                        <TextField type='number' required error={errorMonto} helperText={errorLegendMonto} sx={{m: 2, width: 400}} label='Monto' value={ monto } onChange={ (e) => {setMonto(e.target.value)} } />
                        <TextField  sx={{m: 2, width: 400}} label='ID Producto' value={ idProducto } onChange={ (e) => setIdProducto(e.target.value) } />
                    
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => handleUpdateLams()}>
                            {button}
                        </Button>
                        <Button onClick={() => closeModal()}>
                            Cerrar
                        </Button>
                    </CardActions>
                </Card>
                <Loading status={loading}></Loading>
            </Box>
        </Modal>
    )
}

export {Formulario}