import React from 'react';
import { Context } from '../../Context';
import { Catalogos } from '../../Api';

function useLaprs(){
    const { jwt } = React.useContext(Context);
    const [ laprs, setLaprs ] = React.useState([]);
    const [ error, setError ] = React.useState(false);
    const [ loading, setLoading ] = React.useState(false);
    const laprsCatalogos = Catalogos(jwt);
    var payload = {
        schema: 'lapr'
    };

    async function getLaprs(){
        setLoading(true);
        try{
            const response = await laprsCatalogos.consultarCatalogo(payload);
            if (response.status === 'success'){
                setLaprs(response.data);
                setLoading(false);
            }
        }
        catch {
            setError(true);
        }
        setLoading(false);



        
    }

    async function updateLaprs(data, id=null){        

        setLoading(true)
        try{
          if (id) payload.id = id
          payload.payload = data
          const response = await laprsCatalogos.actualizarCatalogo(payload)
          if (response.status === 'success'){
            setLoading(false)
            return true
          }
        }
        catch (e){
          setError(true)  
        }
        setLoading(false)
        return false
    }

    async function deleteLaprs(id){
        setLoading(true)
        try{
          payload.id = id
          const response = await laprsCatalogos.eliminarCatalogo(payload)
          if (response.status === 'success'){
            setLoading(false)
            return true
          }
        }
        catch (e){
          setError(true)
        }
        setLoading(false)
        return false
      }

    return {
        laprs,
        loading,
        error,
        getLaprs,
        updateLaprs,
        deleteLaprs
    }
}

export {useLaprs}