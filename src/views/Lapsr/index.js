import React from "react";
import { Avatar, Box, Button, Fab, IconButton, List, ListItem, ListItemAvatar, ListItemText, Modal, TextField,Dialog,Paper, DialogTitle, DialogContent, DialogContentText, DialogActions } from "@mui/material";
import LaprsIcon from '@mui/icons-material/List';
import { Formulario } from "./Formulario";
import { styled } from "@mui/system";
import { Loading } from "../components/Loading";
import { useLaprs } from "./useLapsr";
import { DeleteModal } from "./DeleteModal";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useVersiones } from "../components/useVersiones";
import dayjs from 'dayjs';

const Container = styled('div')({
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'start',
  display: 'flex'
})

function Laprs(){
  const [ modal, setModal ] = React.useState(false)
  const [ change, setChange ] = React.useState(false)
  const { laprs, loading, getLaprs, updateLaprs,deleteLaprs } = useLaprs();
  const [ search, setSearch ] = React.useState('')
  const [ localLaprs, setLocalLaprs ] = React.useState([])
  const [ payload, setPayload ] = React.useState(null)
  const [ deleteModal, setDeleteModal ] = React.useState(false)

  const { versiones, loadingVersiones, getVersiones, updateVersiones, deleteVersiones } = useVersiones("LAPR")
  const [openConfirmUpdateDialog, setOpenConfirmUpdateDialog] = React.useState(false);
  const [lastUpdate, setLastUpdate] = React.useState(null);


  React.useEffect(() =>{
   
    if(versiones[versiones.length - 1] != undefined)
    {
      setLastUpdate(versiones[versiones.length - 1].payload.ultima_actualizacion);
    }

  },[versiones])

  const handleOpenConfirmUpdate = () => {
    setOpenConfirmUpdateDialog(true);
  };

  const handleCloseConfirmUpdate = () => {
    setOpenConfirmUpdateDialog(false);
  };
  

  const handleConfirmUpdate = async () => {
    let send = null
    console.log("versiones")
    console.log(versiones)
    if (versiones[versiones.length - 1] != undefined){
      console.log("not undefined")
        send = await updateVersiones({catalogo:"LAPR", ultima_actualizacion: dayjs().format('YYYY-MM-DD HH:mm:ss')}, versiones[versiones.length - 1].id)
    }
    else{
        send = await updateVersiones({catalogo:"LAPR", ultima_actualizacion: dayjs().format('YYYY-MM-DD HH:mm:ss')})
    }

    if (send){
      setLastUpdate(dayjs().format('YYYY-MM-DD HH:mm:ss'));
      getVersiones();
    }

    handleCloseConfirmUpdate();
  };







  React.useEffect(() =>{
    
    if(search.length > 0){
        let searching = search.toLocaleLowerCase()
        let valores = laprs.filter((item) => {
            const itemText = item.payload.label.toLowerCase()
            return itemText.includes(searching)
        })
        setLocalLaprs(valores)
    }else{
        setLocalLaprs(laprs)
    }
  },[search, laprs])



  React.useEffect( getLaprs, [change])


  return (
    <Paper elevation={0}  sx={{width:"95%", marginTop:10, marginLeft:4}}>
    <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:0, fontSize: 35}}>Listas de Recargas Remotas (LAP_R)</Box>

    <Dialog
        open={openConfirmUpdateDialog}
        onClose={handleCloseConfirmUpdate}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Esta seguro en actualizar todos los dispositivos?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Se actualizara la lista de acciones sobre los medios de pago
            en todos los dispositivos.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmUpdate}>Cancelar</Button>
          <Button onClick={handleConfirmUpdate} autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>


      <Box
      sx={{flexGrow: 1}}
      >
        <Container >
        <TextField sx={{width: '30%', mr: '2%'}}
          placeholder="Escriba para buscar"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          />
          
          <Button sx={{width: '8%', height: 50,mr: '2%'}} onClick={() => {setModal(true); setPayload(null)}} variant='contained' >
            Agregar
          </Button>

          <Button  sx={{width: '18%', height: 50,mr: '20%'}} onClick={handleOpenConfirmUpdate} variant='contained' >
            Actualizar Dispositivos
          </Button>

          <Box sx={{display: 'flex', marginBottom:0, marginLeft:0, fontSize: 20}}>Ultima actualizacion:{lastUpdate}   </Box>
        
        </Container> 
        <List >
          {localLaprs.map((item, index) => (
            
            <ListItem
              key={index}
              secondaryAction={
                <Box>
                  <IconButton edge="end" aria-label="delete" onClick={() => {setModal(true); setPayload(item)}}>
                      <EditIcon />
                  </IconButton>
                  <IconButton edge="end" aria-label="delete" onClick={() => {setDeleteModal(true); setPayload(item)}} >
                      <DeleteIcon />
                  </IconButton>
                </Box>
            }
            >
            <ListItemAvatar>
                <Avatar   style={{   backgroundColor:  item.payload.estatus == 1 ? 'green' :'orange'}}>
                  <LaprsIcon />
                </Avatar>
              </ListItemAvatar>

              <ListItemText sx={{maxWidth:'10%', marginRight:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold', fontStyle: 'italic' }}> UID: {item.payload.uid}</span><br />
           
                
                  </ListItemText>


                  <ListItemText sx={{maxWidth:'12%', marginLeft:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold' }}>Codigo Producto: </span>{item.payload.idProducto}<br />
                  </ListItemText>


                  <ListItemText sx={{maxWidth:'13%', marginLeft:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold'}}> Numero Accion Producto: </span>{item.payload.numeroAccionAplicada}
                  </ListItemText>

                  <ListItemText sx={{maxWidth:'6%', marginLeft:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold'}}>Monto: </span>{item.payload.monto}
                  </ListItemText>


                  <ListItemText sx={{maxWidth:'10%', marginLeft:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold'}}> Fecha Hora Registro: </span> {item.payload.fecha_accion_registro}<br />
                  </ListItemText>



                  <ListItemText sx={{maxWidth:'12%', marginLeft:'15px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold'}}>SAM: </span> {item.payload.sam}<br />
                  </ListItemText>



                  <ListItemText sx={{maxWidth:'11%', marginLeft:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold'}}>Fecha Accion Aplicada: </span> {item.payload.fecha_accion_aplicada}<br />
                  </ListItemText>


                  <ListItemText sx={{maxWidth:'10%', marginLeft:'10px', display:'flex'}}>
                  <span style={{ fontWeight: 'bold'}}>Estatus:  </span>  <span style={{ fontWeight: 'bold', color: item.payload.estatus == 1  ? 'green' :'orange'}}> {  item.payload.estatus == 1  ? 'Aplicada' :'Sin Aplicar'}</span>
                  </ListItemText>

            </ListItem>
            ))
          }
        </List>
        <Loading status={loading} />
        <DeleteModal status={ deleteModal } setStatus={ setDeleteModal } success={ change } setSucess={ setChange } item={ payload } action={ deleteLaprs } loading={loading} />
        <Formulario status={modal} setStatus={setModal} success={change} setSuccess={setChange} action={updateLaprs} loading={loading} item={payload} />
      </Box> </Paper>
  )
}

export {Laprs}