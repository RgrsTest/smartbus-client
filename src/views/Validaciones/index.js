import React from "react";
import { Paper, Table, TableBody,TextField, Box,TableCell, TableContainer, TableHead, TableRow,Stack,CardHeader ,Container,IconButton } from "@mui/material";
import { useDownloadExcel,downloadExcel } from 'react-export-table-to-excel';
import { useValidaciones } from "./useValidaciones";
import { Loading } from "../components/Loading";
import { CustomRow } from "../components/CustomRow";
import { createTheme, ThemeProvider, Typography } from "@mui/material";
import { DatePickerRow } from "../components/DatePickerRow";
import { DatePicker, LocalizationProvider,TimePicker} from "@mui/lab";
import DateAdapter from '@mui/lab/AdapterDateFns'
import { SlRefresh } from "react-icons/sl"
import { RiFileExcel2Fill ,RiSearchLine } from "react-icons/ri";


const columns = [
  { id: 'filtro', label:'Vehiculo', minWidth: 180 ,  align: 'left',marginL:9},
  { 
    id: 'validaciones', 
    label: 'Validaciones', 
    minWidth: 180 ,
    align: 'flex-end',
    marginL:6,
    marginR:0,
    format: (value) => value.toLocaleString('es-MX')
  },
  { 
    id: 'validaciones_recaudo', 
    label: 'Validaciones Recaudo', 
    minWidth: 180 ,
    align: 'right',
    marginL:10,
    marginR:0,
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },
  {
    id: 'bpd',
    label: 'BPD',
    minWidth: '10%',
    align: 'right',
    marginL:8,
    minWidth: 170,
    format: (value) => value.toLocaleString('es-MX')
  },
  {
    id: 'total',
    label: 'Total Movimientos',
    minWidth:'10%',
    align: 'right',
    marginL:9,
    minWidth: 180 ,
    format: (value) => value.toLocaleString('es-MX')
  }
]

const detailsColumns = [
  { id : 'fecha_hora_servidor', label: 'Fecha Hora Servidor'},
  { id: 'fecha_hora', label: 'Fecha Hora Equipo'},
  { id: 'uid', label: 'Tarjeta' },
  { id: 'consecutivo_aplicacion', label:'Consecutivo Tarjeta'},
  { id: 'id_producto', label: 'Producto', format: (id) => productoNameByID(id) },
  { id: 'perfil', label: 'Perfil' , format: (id) => perfilNameByID(id)},
  { id: 'saldo_inicial', label:'Saldo Inicial'},
  { id: 'saldo_final', label:'Saldo Final'},
  { 
    id: 'monto', 
    label: 'Monto',
    format: (value) => value.toLocaleString('es-MX', {style: 'currency', currency: 'MXN'})
  },
  {id:'id_sam',label:'ID SAM'},
  {id:'consecutivo_sam',label:'Consecutivo SAM'},
  {id:'latitud',label:'Ubicacion'},
  {id:'longitud'},
  {id:'subidas',label:'Subidas'},
  {id:'bajadas',label:'Bajadas'}
  
  

]

function uniqueValuesArray(data){
  let result = []
  data.forEach((item) => {
    if (result.findIndex((item) => item) === -1 ){
      result.push(item)}
  })
  return result
}

function arraySum(data){
  let result = 0
  data.forEach((item) =>{
    result += item
  })
  return result
}

function createData(data, filter){
  let result = []
  if(data.length > 0){
    const valores = data.map( (item) => item.payload )
    
    let filtro = uniqueValuesArray(valores.map((item) => item.encabezado._relaciones[filter].payload.label))
    
    filtro.forEach( (value) => {
      var validaciones = valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value && (item.encabezado.id_producto === "4331" || item.encabezado.id_producto === "4d31") ))
      var bpd = valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value && (item.encabezado.id_producto === '4231' || item.encabezado.id_producto === '4232')))
      var validaciones_recaudo = arraySum(validaciones.map((item) => item.encabezado.monto))
      var bpd_recaudo = arraySum(bpd.map((item) => item.encabezado.monto))
      

      let localValue = {
        filtro: value,
        validaciones: validaciones.length,
        validaciones_recaudo: validaciones_recaudo,
        bpd: bpd.length,
        bpd_recaudo: arraySum(bpd.map((item) => item.encabezado.monto)),
        total: validaciones.length + bpd.length,
        total_recaudo: validaciones_recaudo + bpd_recaudo,
        details: valores.filter((item) => (item.encabezado._relaciones[filter].payload.label === value))
      }
      result.push(localValue)
    })
  }
  return result
}

var productosObj = {
 
};

var perfilesObj = {
 
};

function getProductos(data)
{
   
  if(data.length > 0){
 
    
    data.forEach((item) => {

        let key = item.payload.id_producto
        let value = item.payload.producto_nombre

        productosObj[key] = value;
    })
    }
}


function getPerfiles(data)
{
   
  if(data.length > 0){
 
    
    data.forEach((item) => {

        let key = item.payload.codigo_perfil
        let value = item.payload.usuario_tipo

        perfilesObj[key] = value;
    })

   
    }
  
  
}


function productoNameByID(identifier)
{
  console.log(identifier)
  return productosObj[identifier]
}

function perfilNameByID(identifier)
{
  
  console.log(identifier)
  return perfilesObj[identifier]
}


function Validaciones(){


  const [ fromDate, setFromDate] = React.useState(new Date())
  const [ toDate, setToDate] = React.useState(new Date())

  const [ startDate, setStartDate] = React.useState(new Date())
  const [ finalDate, setFinalDate] = React.useState(new Date())

  const [ docExcel, setDocExcel] = React.useState(null)


  

  //const header = [];

 

  /**
   * @description:
   *  also accepts an array of objects; the method (downloadExcel) will take
   *  as order of each column, the order that each property of the object brings with it.
   *  the method(downloadExcel) will only take the value of each property.
   */
  const excel = [];

  const {loading, validaciones, productos,perfiles } = useValidaciones(fromDate,toDate)



  getProductos(productos)
  getPerfiles(perfiles)

 const handleDate = () => {
    setFromDate(startDate);
    setToDate(finalDate);
  }

  const header = [];
  header.push("Unidad");

  detailsColumns.forEach((item) => {


    header.push(item.label);


   })

   console.log("header");
   console.log(header)

function handleDownloadExcel() {
  downloadExcel({
    fileName: "ReporteValidacionTarjetas",
    sheet: "Transacciones",
    tablePayload: {
      header,

      body: excel,
    },
  });
}



  return (
    <Paper elevation={0}  sx={{width:"100%", marginTop:10, marginLeft:0}}>
     
     
     <Box sx={{display: 'flex', fontWeight: 'bold', marginBottom:5, marginLeft:4, fontSize: 35}}>Validacion de Tarjetas</Box>
      
     
     <Box>

      <TableCell sx={{ marginLeft:0,marginTop:10,width: '100vw'}}>
     

      <LocalizationProvider dateAdapter={DateAdapter}>
        <DatePicker label='Fecha Inicial' value={startDate} 
        inputFormat="dd/MM/yyyy"
        onChange={(newValue) => setStartDate(newValue)}
        renderInput={(params) => <TextField variant="standard" 
        sx={{width: 220,marginLeft:3,justifyContent: 'left'}} 
        error={false} {...params} />} />

     </LocalizationProvider>

     <LocalizationProvider dateAdapter={DateAdapter}>
        <DatePicker label='Fecha Final'  value={finalDate} 
        inputFormat="dd/MM/yyyy"
        onChange={(newValue) => setFinalDate(newValue)}
        renderInput={(params) => <TextField variant="standard" 
        sx={{width: 220,marginLeft:4}} 
        error={false} {...params} />} />

     </LocalizationProvider>

     
      <IconButton   sx={{marginLeft:7, marginTop:1.6, ":hover": {
          bgcolor: "#F2F4F4",
          color: "#1661c9"
        }}}  variant="solid" onClick={handleDate}>
                  <RiSearchLine />
                                        
      </IconButton>

      <IconButton  sx={{marginLeft:3, marginTop:1.5 , ":hover": {
          bgcolor: "#F2F4F4",
          color: "#478C21"
        }}}  variant="solid"  onClick={handleDownloadExcel}>
          <RiFileExcel2Fill />
                                                          
      </IconButton>


      </TableCell>
    
      </Box>
      <Box sx={{display: 'flex' ,justifyContent:  'flex-start'}}>
                {columns.map((column) => (
                    < Box
                    key={column.id}
                    sx={{ marginLeft:column.marginL, minWidth:column.minWidth}}
                    
                    >
                      {column.label}
                    </Box>
                  ))}
                            
       </Box>                 


    <TableBody>
    {createData(validaciones, 'vehiculo').map(( row, index ) => (
      <CustomRow key={index} row={row} fields={columns} detailFields={detailsColumns} excel={excel} ></CustomRow>
    ))}
  </TableBody>
 

 

      
      
      <Loading status={loading}></Loading>
    </Paper>
  )
}

export {Validaciones}

/*
<DatePickerRow startDate = {startDate} 
setStartDate = {setStartDate}  
finalDate={finalDate} 
setFinalDate = {setFinalDate}  
handleDate = {handleDate} 
handleDownloadExcel = {handleDownloadExcel}


>
   {columns.map((column) => (
       <TableCell
       key={column.id}
      
    

      
       >
         {column.label}
       </TableCell>
     ))}

</DatePickerRow>*/


  /*
<TableContainer>
<Table>

<TableHead>    
<Typography sx={{marginLeft:5}} className="title-validaciones" variant="h4" component='div'>Validacion de Tarjetas</Typography>     
  

  
</TableHead>

<TableBody>
<LocalizationProvider dateAdapter={DateAdapter}>
    <DatePicker label='Seleccione Fecha' 
     renderInput={(params) => <TextField variant="standard" 
     sx={{width: 220,marginTop:1}} 
     error={false} {...params} />} />

  </LocalizationProvider>
  </TableBody>


  <TableBody>

<TableRow>
      <TableCell />
      {columns.map((column) => (
        <TableCell
        key={column.id}
        align={column.align}
        style={{ minWidth: column.minWidth }}
        >
          {column.label}
        </TableCell>
      ))}
</TableRow>

</TableBody>

  <TableBody>
    {createData(validaciones, 'vehiculo').map(( row, index ) => (
      <CustomRow key={index} row={row} fields={columns} detailFields={detailsColumns} excel={excel} ></CustomRow>
    ))}
  </TableBody>
</Table>
</TableContainer>*/