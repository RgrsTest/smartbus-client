import './App.css';
//import { Layout } from './views/Layout';
import {Main} from './views/Main'

function App() {
  return (
    <div className="App">
      <Main></Main>
    </div>
  );
}

export default App;
