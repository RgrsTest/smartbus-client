import smartbusRequest from "../config";


function Catalogos(jwt){
    const request = smartbusRequest(jwt)

    async function actualizarCatalogo(payload ){
            const response =  await request.post('/catalogos:actualizar', payload)
            return response.data
    }
    
    async function consultarCatalogo(payload ) {
        const response = await request.post('/catalogos:consultar', payload)
        return  response.data

    }
    
    async function eliminarCatalogo(payload){
        const response = await request.post('/catalogos:eliminar', payload)
        return  response.data
    }

    async function remplazarCatalogo(payload){
        const response = await request.post('/catalogos:remplazar', payload)
        return  response.data
    }

    return {
        actualizarCatalogo,
        consultarCatalogo,
        eliminarCatalogo,
        remplazarCatalogo
    }
}

export { Catalogos }