import axios from 'axios'

function smartbusRequest(jwt = null){

    const config = {
        baseURL: 'https://newton.klayware.com/smartbus/',
        validateStatus: function(status){return (status >= 200 && status <300) || status === 422  },
        headers:  jwt?{'Authorization': 'Bearer ' + jwt} : {}
    }
    const request = axios.create(config)
    return request
}

export default smartbusRequest