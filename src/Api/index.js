import { authenticate } from "./Auth";
import { Catalogos } from "./Catalogos";
import { Documentos } from "./Documentos";
import { Tracking } from './Tracking';
import { Dashboard } from "./Dashboard";
import { Sesion } from "./Sesion";

export {
    authenticate,
    Catalogos,
    Documentos,
    Tracking,
    Dashboard,
    Sesion
}