import smartbusRequest from "../config";


function Dashboard(jwt){
    const request = smartbusRequest(jwt)

    async function getDashboard(payload ){
            const response =  await request.post('/catalogos:dashboard', payload)
            return response.data
    }
    
    return {
        getDashboard
    }
}

export { Dashboard }