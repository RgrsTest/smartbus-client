import smartbusRequest from "../config";

function Documentos(jwt){
    const request = smartbusRequest(jwt)

    async function consultar(payload){
        const response = await request.post('/documentos:consultar', payload)
        return response.data
    }

    return {
        consultar
    }
}

export {Documentos}