import smartbusRequest from "../config";
import React from "react";

function Tracking(jwt){
    const request = smartbusRequest(jwt)
    
    async function getInicialStatus(){
        const starting = await request.post('/documentos:ubicacion')
        
        return starting.data
    }

    
    function initSocket(setter, initValue){
        const Pusher = window.Pusher;

        var pusher = new Pusher('4e153c947ec3bbfa8484', {
            cluster: 'us2'
            });

        //Pusher.logToConsole = true;
        
        var channel = pusher.subscribe('smartbus');
        channel.bind('Smartbus\\Events\\UpdateLocationEvent', function(data) {
            setter(data, initValue)
        });
    }
    return {
        getInicialStatus,
        initSocket
    }
}

export {Tracking}