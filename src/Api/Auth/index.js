import smartbustRequest from '../config'

const request = smartbustRequest()


async function authenticate( username, password ){
    const apikey =  await obtenerApikey(username, password)
    if (apikey.status === "fail") throw apikey

    const session = await iniciarSesion(username, apikey.data.apikey)
    if (session.status ==="fail") throw session
    return session.data.jwt
    
}

async function iniciarSesion(username, apikey ){
    try{
        const response =  await request.post('/sesion:iniciar', {correo: username, apikey: apikey})
        return response.data
    }
    catch(e){
        throw e
    }
}

async function obtenerApikey(username, password ) {
    const response = await request.post('/usuarios:obtener_apikey', {correo: username, contrasena: password})
    return  response.data
}

export {authenticate}