import smartbusRequest from "../config";

function Sesion(jwt){
    const request = smartbusRequest(jwt)
    
    async function cerrarSesion(){
        const response = await request.post('/sesion:cerrar')
        return response.data
    }
    async function consultar(){
        const response = await request.post('/sesion:consultar')
        return response.data
    }

    return {
        cerrarSesion,
        consultar
    }
}

export {Sesion}