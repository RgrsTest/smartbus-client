import React from 'react';
import { Sesion } from '../Api';
import { useLocalStorage } from './useLocalStorage';

const Context = React.createContext()


function Provider(props){
    const [ auth, setAuth ] = React.useState(null)
    const [ jwt, setJwt ] = React.useState('')
    const [ loading, setLoading ] = React.useState(false)
    const { storage, saveLocalStorage } = useLocalStorage('SMARTBUS_V1', {})
    const [ drawer, setDrawer ] = React.useState(false)
    const [ sesion, setSesion ] = React.useState(null)
    
    const hanldeJWT = async () => {
        try{
            let localJWT = storage.jwt
            if (localJWT){
                const sesion =  Sesion(localJWT)
                try{
                    await sesion.consultar()
                    setSesion(sesion)
                    setJwt(localJWT)
                    setAuth(true)
                }
                catch(e) {
                    console.log(e)
                }
            }
        }
        catch (e){

        }
    }

    const handleSetJWT = (jwt) => {
        setJwt(jwt)
        if (jwt === ''){
            sesion.cerrarSesion()
            setSesion(null)
        }
        setSesion(Sesion(jwt))
        let newStorage = storage
        newStorage.jwt = jwt
        saveLocalStorage(newStorage)
    }
    React.useEffect( () => {
        try{
            hanldeJWT()
        }
        catch (e) {

        }
    }, [storage])


    return(
        <Context.Provider value={{
            auth,
            sesion,
            setAuth,
            handleSetJWT,
            jwt,
            setJwt,
            loading,
            setLoading,
            storage,
            saveLocalStorage,
            drawer,
            setDrawer
        }}>
            { props.children }
        </Context.Provider>
    )
}

export { Context, Provider }