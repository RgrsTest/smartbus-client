import React from 'react';

function useLocalStorage(itemName, initialValue) {
    const [ storage, setStorage ] = React.useState(initialValue)

    React.useEffect(() => {
        const localStorageItem = localStorage.getItem(itemName)
        if ( localStorageItem ) {
            setStorage(JSON.parse(localStorageItem))
        }
        else {
            localStorage.setItem(itemName, JSON.stringify(initialValue))
            setStorage(initialValue)
        }
    },[])

    function saveLocalStorage(newItem) {
        try {
            const strItem = JSON.stringify(newItem)
            localStorage.setItem(itemName, strItem)
            setStorage(newItem)
        }
        catch (e) {
            console.log(e);
        }
    }

    return {
        storage,
        saveLocalStorage
    }

}

export { useLocalStorage }